<?php
    $root = realpath($_SERVER["DOCUMENT_ROOT"]);

    function get($name) {
        if (isset($_GET[$name])) {
            return ($_GET[$name]);
        } else {
            return (null);
        }
    }

    // CONFIG
        require("$root/config.php");

    // LOCALES
        if(isset($_COOKIE["language"])) {
            switch ($_COOKIE["language"]) {
                case "fr_FR":
                    require("$root/locales/fr_FR.php");
                    break;
                case "en_US":
                    require("$root/locales/en_US.php");
                    break;
                default:
                    require("$root/locales/fr_FR.php");
                    break;
            }
        } else {
            require("$root/locales/fr_FR.php");
        }

    // MODELS
        require("$root/models/Database.php");
        require("$root/models/User.php");
        require("$root/models/Country.php");
        require("$root/models/Province.php");
        require("$root/models/Platform.php");
        require("$root/models/Region.php");
        require("$root/models/Game.php");
        require("$root/models/Item.php");
        require("$root/models/Match.php");
        require("$root/models/Notification.php");

    // VIEWS
        require("$root/views/View.php");
        require("$root/views/LightBox.php");
        require("$root/views/TextView.php");
        require("$root/views/Card.php");
        require("$root/views/BreadCard.php");
        require("$root/views/Navbar.php");
        require("$root/views/LanguageSelect.php");
        require("$root/views/Marquee.php");
        require("$root/views/Carousel.php");

        // Sign
            require("$root/views/sign/SignUpForm.php");
            require("$root/views/sign/SignInForm.php");
            require("$root/views/sign/SignOutForm.php");

        // Country
            require("$root/views/country/CountrySelector.php");

        // Province
            require("$root/views/province/ProvinceSelector.php");

        // User
            require("$root/views/user/UserProfile.php");
            require("$root/views/user/UserEditForm.php");
            require("$root/views/user/UserLockForm.php");

        // Platform
            require("$root/views/platform/PlatformAddForm.php");
            require("$root/views/platform/PlatformListTable.php");
            require("$root/views/platform/PlatformEditForm.php");
            require("$root/views/platform/PlatformDeleteForm.php");
            require("$root/views/platform/PlatformSelector.php");

        // Region
            require("$root/views/region/RegionAddForm.php");
            require("$root/views/region/RegionListTable.php");
            require("$root/views/region/RegionEditForm.php");
            require("$root/views/region/RegionDeleteForm.php");
            require("$root/views/region/RegionSelector.php");

        // Game
            require("$root/views/game/GameAddForm.php");
            require("$root/views/game/GameListTable.php");
            require("$root/views/game/GameEditForm.php");
            require("$root/views/game/GameDeleteForm.php");
            require("$root/views/game/GameSelector.php");

        // Item
            require("$root/views/item/ItemAddForm.php");
            require("$root/views/item/ItemDeleteForm.php");
            require("$root/views/item/ItemListTable.php");
            require("$root/views/item/ItemEditForm.php");

        // Match
            require("$root/views/match/MatchAcceptForm.php");
            require("$root/views/match/MatchVoteForm.php");
            require("$root/views/match/MatchListTable.php");
            require("$root/views/match/MatchInfosTable.php");
            require("$root/views/match/MatchRefusForm.php");
            require("$root/views/match/MatchCancelForm.php");

        // Admin
            require("$root/views/admin/AdminUsersTable.php");
            //require("$root/views/tableUsers/UpdateUserForm.php");
            require("$root/views/admin/AdminItemsTable.php");

        //Notification
            require("$root/views/notification/NotificationListTable.php");
            require("$root/views/notification/NotificationAddForm.php");
  
    // CONTROLLERS
        require("$root/controllers/Router.php");
        require("$root/controllers/Controller.php");
        require("$root/controllers/HomeController.php");
        require("$root/controllers/DatabaseController.php");

        // Sign
            require("$root/controllers/sign/SignController.php");

        // Country
            require("$root/controllers/country/country-selector/CountrySelectorController.php");

        // Province
            require("$root/controllers/province/province-selector/ProvinceSelectorController.php");

        // User
            require("$root/controllers/user/UserController.php");
            require("$root/controllers/user/UserProfileController.php");
            require("$root/controllers/user/user-edit/UserEditFormController.php");
            require("$root/controllers/user/user-lock/UserLockFormController.php");

        // Platform
            require("$root/controllers/platform/PlatformController.php");
            require("$root/controllers/platform/platform-delete/PlatformDeleteFormController.php");
            require("$root/controllers/platform/platform-selector/PlatformSelectorController.php");

        // Region
            require("$root/controllers/region/RegionController.php");
            require("$root/controllers/region/region-add/RegionAddFormController.php");
            require("$root/controllers/region/region-edit/RegionEditFormController.php");
            require("$root/controllers/region/region-delete/RegionDeleteFormController.php");
            require("$root/controllers/region/region-selector/RegionSelectorController.php");

        // Game
            require("$root/controllers/game/GameController.php");
            require("$root/controllers/game/game-add/GameAddFormController.php");
            require("$root/controllers/game/game-edit/GameEditFormController.php");
            require("$root/controllers/game/game-delete/GameDeleteFormController.php");
            require("$root/controllers/game/game-selector/GameSelectorController.php");

        // Item
            require("$root/controllers/item/ItemController.php");
            require("$root/controllers/item/item-add/ItemAddFormController.php");
            require("$root/controllers/item/item-delete/ItemDeleteFormController.php");
            require("$root/controllers/item/item-edit/ItemEditFormController.php");

        // Match
            require("$root/controllers/match/MatchController.php");
            require("$root/controllers/match/MatchInfosTableController.php");
            require("$root/controllers/match/match-accept/MatchAcceptFormController.php");
            require("$root/controllers/match/match-refus/MatchRefusFormController.php");
            require("$root/controllers/match/match-vote/MatchVoteFormController.php");
            require("$root/controllers/match/match-cancel/MatchCancelFormController.php");

        //Admin
            require("$root/controllers/admin/AdminController.php");

        //Notification
            require("$root/controllers/notification/NotificationController.php");
?>
