<?php
    class ProvinceSelector extends View {
        private $countrySelectorController;
        private $provinces;
        private $province_id;

        function __construct($countrySelectorController, $provinces = null, $province_id = null) {
            $this->countrySelectorController = $countrySelectorController;
            $this->provinces = $provinces;
            $this->province_id = $province_id;
        }

        function render() {
            $this->countrySelectorController->render();
?>
            <script src="/controllers/country/country-selector/country_selector_updater.js"></script>

            <div class='form-group'>
                <label for="province_id"><?= $GLOBALS['locale']['province'] ?></label>
                <select id='province_select' class='form-control' name='province_id'>
                    <option disabled selected><?= $GLOBALS['locale']['select_option'] ?></option>
<?php
                    foreach ($this->provinces as $province) {
                        $id = $province->getId();
                        $name = $province->getProvince();
                        
                        if(!is_null($this->province_id)) { 
                            if($id == $this->province_id) {
                                echo("<option value='$id' selected>$name</option>");
                            } else {
                                echo("<option value='$id'>$name</option>");
                            }
                        } else {
                            echo("<option value='$id'>$name</option>");
                        }
                    }
?>
                </select>
            </div>
<?php 
        }
    }
?>