<?php
    class Carousel extends View {
        private $slides;
        private $captions;

        function __construct($slides, $captions) {
            $this->slides = $slides;
            $this->captions = $captions;
        }

        function render() {
?>
            <div id="carousel" class="carousel slide" data-ride="carousel">
                

                <div class="carousel-inner">
<?php
                    $titles = array_keys($this->captions);
                    $captions = $this->captions;

                    $i = 0;
                    $first = true;

                    foreach ($this->slides as $title => $image) {
                        if ($first) {
                            $first = false;
?>
                            <div class="carousel-item active">
                                <img class="d-block w-100" src="<?= $image ?>" alt="<?= $title ?>">
                                <div class="carousel-caption d-none d-md-block">
                                    <h5><?= $titles[$i] ?></h5>
                                    <p><?= $captions[$titles[$i]] ?></p>
                                </div>
                            </div>
<?php
                        } else {
?>
                            <div class="carousel-item">
                                <img class="d-block w-100" src="<?= $image ?>" alt="<?= $title ?>">
                                <div class="carousel-caption d-none d-md-block">
                                    <h5><?= $titles[$i] ?></h5>
                                    <p><?= $captions[$titles[$i]] ?></p>
                                </div>
                            </div>
<?php
                        }

                        $i++;
                    }
?>
                </div>
                <a class="carousel-control-prev" href="#carousel" role="button" data-slide="prev">
                    <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                    <span class="sr-only">Previous</span>
                </a>
                <a class="carousel-control-next" href="#carousel" role="button" data-slide="next">
                    <span class="carousel-control-next-icon" aria-hidden="true"></span>
                    <span class="sr-only">Next</span>
                </a>
            </div>
<?php
        }
    }
?>