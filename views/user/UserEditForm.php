<?php
    class UserEditForm extends View {
        private $provinceSelectorController;
        private $user;

        function __construct($provinceSelectorController, $user) {
            $this->provinceSelectorController = $provinceSelectorController;
            $this->user = $user;
        }

        function render() {
            $user = $this->user;

            $id = $user->getId();
            $firstname = $user->getFirstname();
            $lastname =  $user->getLastname();
            $username =  $user->getUsername();
            $email =  $user->getEmail();
?>
            <form method="post" action="/controllers/user/user-edit/UserEditFormAction.php">
                <input type="hidden" name="id" value="<?php echo($id); ?>" class="form-control"/>
                <div class="form-group">
                    <label for="username">
                        <?=$GLOBALS["locale"]["username"] ?>
                    </label>
                    <input type="text" name="username" class="form-control" required value="<?php echo($username); ?>" />
                </div>

                <div class="row">
                    <div class="col-md-6 form-group">
                        <label for="firstname">
                            <?=$GLOBALS["locale"]["firstname"] ?>
                        </label>
                        <input type="text" name="firstname" class="form-control" required value="<?php echo($firstname); ?>" />
                    </div>

                    <div class="col-md-6 form-group">
                        <label for="lastname">
                            <?=$GLOBALS["locale"]["lastname"] ?>
                        </label>
                        <input type="text" name="lastname" class="form-control" required value="<?php echo($lastname); ?>" />
                    </div>
                </div>

                <div class="form-group">
                    <label for="email">
                        <?=$GLOBALS["locale"]["email"] ?>
                    </label>
                    <input type="email" name="email" class="form-control" required value="<?php echo($email); ?>" />
                </div>
<?php
                $this->provinceSelectorController->render();
?>
                
                <a class="btn btn-danger" href="index.php?controller=user&action=profile&id=<?= $id  ?>"><?= $GLOBALS["locale"]["return"] ?></a>
                <div class='float-right'>
                    <input class='btn btn-success' type='submit' value="<?=$GLOBALS["locale"]["edit"] ?>" />
                    <a class='btn btn-danger' href="/index.php?controller=user&action=lock"><?=$GLOBALS["locale"]["lock"] ?></a>
                </div>
            </form>
<?php
        }
    }
?>
