<?php
    class UserProfile extends View {
        private $id;
        private $avatar;
        private $username;
        private $firstname;
        private $lastname;
        private $email;
        private $positive_ranking;
        private $negative_ranking;

        function __construct($user) {
            $this->id = $user->getId();
            $this->avatar = $user->getAvatar();
            $this->username = $user->getUsername();
            $this->firstname = $user->getFirstname();
            $this->lastname = $user->getLastname();
            $this->email = $user->getEmail();
            $this->positive_ranking = $user->getPositiveRanking();
            $this->negative_ranking = $user->getNegativeRanking();
        }

        function render() {
?>
                    <div class="text-center">
                        <img class="rounded m-3 w-25 img-fluid" src="<?= $this->avatar ?>" />
                    </div>
<?php
                    $session_user_id = $_SESSION["user_id"];
                    $session_user = getUserById($session_user_id);
                    
?>                
                    <div class="row">
                        <div class="d-flex justify-content-center align-items-center col-md-2 alert-danger rounded-pill">
                            <h2><?= $this->negative_ranking ?></h2>
                        </div>

                        <div class="col-md-8 text-center">
                            <h1><?= $this->username ?></h1>
                            <!-- <h2><?= $this->firstname ?> <?= $this->lastname ?></h2> --> 
<?php        
                            if ($session_user->isAdministrator() || $session_user_id == $this->id) {
?>
                                <a class="m-3 btn btn-primary" href="/index.php?controller=user&action=edit&id=<?= $this->id ?>"><?= $GLOBALS["locale"]["edit"] ?></a>
<?php
                            }
?>        
                        </div>

                        <div class="d-flex justify-content-center align-items-center col-md-2 alert-success rounded-pill">
                            <h2><?= $this->positive_ranking ?></h2>
                        </div>
                    </div>

                    <div class='card m-5'>
                    <div class='card-header'>Informations</div>
                    <div class='card-body'>
                        <div class="form-group">
                            <label for="username">
                                <?=$GLOBALS["locale"]["username"] ?>
                            </label>
                            <input type="text" name="username" class="form-control" required value="<?php echo($this->username); ?>" readonly />
                        </div>

                        <div class="row">
                            <div class="col-md-6 form-group">
                                <label for="firstname">
                                    <?=$GLOBALS["locale"]["firstname"] ?>
                                </label>
                                <input type="text" name="firstname" class="form-control" required value="<?php echo($this->firstname); ?>" readonly />
                            </div>

                            <div class="col-md-6 form-group">
                                <label for="lastname">
                                    <?=$GLOBALS["locale"]["lastname"] ?>
                                </label>
                                <input type="text" name="lastname" class="form-control" required value="<?php echo($this->lastname); ?>" readonly />
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="email">
                                <?=$GLOBALS["locale"]["email"] ?>
                            </label>
                            <input type="email" name="email" class="form-control" required value="<?php echo($this->email); ?>" readonly />
                        </div>
                    </div>
                    </div>
<?php
        }
    }
?>