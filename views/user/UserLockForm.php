<?php
    class UserLockForm extends View {
		private $id;  
		private $action;
      
		function __construct($id, $action) {
			$this->id = $id;
			$this->action = $action;
		}

        function render() {
			$action = $this->action;
	?>
			<form method="post" action="/controllers/user/user-lock/UserLockFormAction.php">
				<div class="mx-auto" style="width: 100%;">
					<?php
						if($action=="lock") {
							echo($GLOBALS["locale"]["user_lock_question"]);
						} else {
							echo($GLOBALS["locale"]["user_unlock_question"]);
						}
					?>
				</div>

                <br>

                <div class="mx-auto">
                    <a  class="btn btn-danger" href='index.php?controller=user&action=profile&id=<?= $this->id ?>'><?= $GLOBALS["locale"]["return"] ?></a>
                    <?php
						if($action == "lock") {
							echo("<button class=\"btn btn-success\" type=\"submit\" name=\"id\" value=".$this->id.">".$GLOBALS["locale"]["lock"]."</button>");
						} else {
							echo("<button class=\"btn btn-success\" type=\"submit\" name=\"id\" value=".$this->id.">".$GLOBALS["locale"]["unlock"]."</button>");
						}
                    ?>
                </div>
            </form>
<?php
        }
    }
?>
