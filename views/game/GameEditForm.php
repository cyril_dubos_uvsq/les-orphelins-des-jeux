<?php
    class GameEditForm extends View {
        private $regionSelectorController;

        private $id;
        private $title;
        private $year;
        private $region_id;
        private $description;
        private $language;

        private $platforms;
        private $regions;

        private $platform_id;

        function __construct($regionSelectorController, $game) {
            $this->regionSelectorController = $regionSelectorController;

            $this->id = $game->getId();
            $this->title = $game->getTitle();
            $this->year = $game->getYear();
            $this->region_id = $game->getRegionId();
            $this->description = $game->getDescription();
            $this->language = $game->getLanguage();
        }

        function render() {
            echo(" 
                <script src='/views/game/scripts/select_change.js' type='text/javascript' > </script>
                <form action='/controllers/game/game-edit/GameEditFormAction.php' method='post'>
                    <div class='form-group'>
                        <label for='id'>{$GLOBALS["locale"]["id"]}</label> 
                        <input class='form-control' name='id' type='text' value='$this->id' readonly/>
                    </div>

                    <div class='form-group'>
                        <label for='title'>{$GLOBALS["locale"]["title"]}</label> 
                        <input class='form-control' name='title' type='text' value='$this->title'/>
                    </div>

                    <div class='form-group'>
                        <label for='year'>{$GLOBALS["locale"]["year"]}</label> 
                        <input class='form-control' name='year' type='number' value='$this->year'/>
                    </div>
            ");
            
            $this->regionSelectorController->render();

            echo("
                    <div class='form-group'>
                        <label for='description'>{$GLOBALS["locale"]["description"]}</label>
                        <textarea class='form-control' name='description'>$this->description</textarea>
                    </div>

                    <div class='form-group'>
                        <label for='language'>{$GLOBALS["locale"]["language"]}</label> 
                        <input class='form-control' name='language' type='text' value='$this->language'/>
                    </div>

                    <a class='btn btn-danger' href='index.php?controller=region&id=$this->region_id'>{$GLOBALS["locale"]["return"]}</a>
                    <input class='btn btn-success float-right' type='submit' value='{$GLOBALS["locale"]["edit"]}' />
                </form>
            ");
        }
    }
?>