<?php
    class GameSelector extends View {
        private $regionSelectorController;
        private $games;
        private $game_id;

        function __construct($regionSelectorController, $games = null, $game_id = null) {
            $this->regionSelectorController = $regionSelectorController;
            $this->games = $games;
            $this->game_id = $game_id;
        }

        function render() {
            $this->regionSelectorController->render();
?>
            <script>
                function readMoreButton() {
                    console.log("buttonnnn");

                    select = document.getElementById("game_select");
                    select_id = select.value

                    link = "/index.php?controller=game&id=" + select_id;
                    window.open(link, '_blank');
                }
             </script>

            <div id="game-selector" class='form-group'>
                <label for="game_id"><?= $GLOBALS["locale"]["game"] ?></label>
                
                <div class="input-group">
                    <select id="game_select" class='form-control' name='game_id'>
                        <option disabled selected><?= $GLOBALS["locale"]["select_option"] ?></option>
<?php      
                        foreach ($this->games as $game) {
                            $id = $game->getId();
                            $title = $game->getTitle();
                            
                            if(!is_null($this->game_id)) { 
                                if($id == $this->game_id) {
                                    echo("<option value='$id' selected>$title</option>");
                                } else {
                                    echo("<option value='$id'>$title</option>");
                                }
                            } else {
                                echo("<option value='$id'>$title</option>");
                            }
                        }
?>
                    </select>
                    <div class="input-group-append">
                        <a class="btn btn-outline-secondary" onclick="readMoreButton()"><?= $GLOBALS["locale"]["read_more"] ?></a>
                    </div>
                </div>
            </div>
<?php 
        }
    }
?>