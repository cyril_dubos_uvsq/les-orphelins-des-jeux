<?php
    $plat = intval($_GET['plat']);

    $root = realpath($_SERVER["DOCUMENT_ROOT"]);
    require("$root\models\Region.php");
    require("$root\models\Database.php");
    $regions = getRegionsByPlatformId($plat);

    foreach ($regions as $region) {
        $id = $region->getId();
        $name = $region->getName();
        echo("$id/=/$name;");
    }
?>