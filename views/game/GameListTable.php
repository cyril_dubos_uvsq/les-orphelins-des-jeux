<?php
    class GameListTable extends View {
        private $games;

        function __construct($games) {
            $this->games = $games;
        }

        function render() {
            echo(" 
                <table class='table'>
                    <tr>
                        <th>{$GLOBALS["locale"]["id"]}</th>
                        <th>{$GLOBALS["locale"]["title"]}</th>
                        <th>{$GLOBALS["locale"]["year"]}</th>
                        <th>{$GLOBALS["locale"]["language"]}</th>
                        <th></th>
                    </tr>
            ");

            if(isset($this->games)) { 
                foreach ($this->games as $game) {
                    $id = $game->getId();
                    $title = $game->getTitle();
                    $year = $game->getYear();
                    $region_id = $game->getRegionId();
                    $description = $game->getDescription();
                    $language = $game->getLanguage();

                    echo("
                        <tr>
                            <td>$id</td>
                            <td>$title</td>
                            <td>$year</td>
                            <td>$language</td>
                            <td>
                                <a class='btn btn-primary' href='/index.php?controller=game&id=" . $id . "'>{$GLOBALS["locale"]["view"]}</a>
                    ");

                    if (isset($_SESSION["user_id"])) {
                        $user_id = $_SESSION["user_id"];
                        $user = getUserById($user_id);
        
                        if ($user->isAdministrator()) {
                            echo("
                            <a class='btn btn-secondary' href='/index.php?controller=game&id=" . $id . "&action=edit'>{$GLOBALS["locale"]["edit"]}</a>
                            <a class='btn btn-danger' href='/index.php?controller=game&id=" . $id . "&action=delete'>{$GLOBALS["locale"]["delete"]}</a>
                            ");
                        }
                    }
    
                    echo("
                            </td>
                        </tr>
                    ");
                }
            }

            echo("</table>");
        }
    }
?>