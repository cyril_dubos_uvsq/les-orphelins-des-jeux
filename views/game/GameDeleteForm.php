<?php
    class GameDeleteForm extends View {
        private $game;
        private $platform_id;
        private $region_id;

        function __construct($game, $platform_id, $region_id) {
            $this->game = $game;
            $this->platform_id = $platform_id;
            $this->region_id = $region_id;
        }

        function render() {
            $game = $this->game;

            $id = $game->getId();
            $title = $game->getTitle();
            $year = $game->getYear();
            $platform_id = $this->platform_id;
            $region_id = $this->region_id;
            $description = $game->getDescription();
            $language = $game->getLanguage();
?>
            <form action='/controllers/game/game-delete/GameDeleteFormAction.php' method='post'>
                    <div class='form-group'>
                        <label for='id'><?= $GLOBALS["locale"]["id"] ?></label> 
                        <?php echo("<input class='form-control' name='id' type='text' value='$id' readonly />"); ?>
                    </div>

                    <div class='form-group'>
                        <label for='Ddescription'><?= $GLOBALS["locale"]["title"] ?></label> 
                        <?php echo("<input class='form-control' name='title' type='text' value='$title' readonly />"); ?>
                    </div>

                    <div class='form-group'>
                        <label for='year'><?= $GLOBALS["locale"]["year"] ?></label> 
                        <?php echo("<input class='form-control' name='year' type='text' value='$year' readonly />"); ?>
                    </div>

                    <div class='form-group'>
                        <label for='platform_id'><?= $GLOBALS["locale"]["platform"] ?></label> 
                        <?php echo("<input class='form-control' name='platform_id' type='text' value='$platform_id' readonly />"); ?>
                    </div>

                    <div class='form-group'>
                        <label for='region_id'><?= $GLOBALS["locale"]["region"] ?></label> 
                        <?php echo("<input class='form-control' name='region_id' type='text' value='$region_id' readonly />"); ?>
                    </div>

                    <div class='form-group'>
                        <label for='description'><?= $GLOBALS["locale"]["description"] ?></label>
                        <?php echo("<input class='form-control' name='description' type='text' value='$description' readonly />"); ?>
                    </div>

                    <div class='form-group'>
                        <label for='language'><?= $GLOBALS["locale"]["language"] ?></label> 
                        <?php echo("<input class='form-control' name='language' type='text' value='$language' readonly />"); ?>
                    </div>

                <div class="mx-auto">
                    <?php echo("<a class='btn btn-danger' href='index.php?controller=region&id=$region_id'>{$GLOBALS["locale"]["return"]}</a>"); ?>
                    <input class="btn btn-success float-right" type="submit" value="<?= $GLOBALS["locale"]["delete"] ?>" />
                </div>
            </form>
<?php
        }
    }
?>