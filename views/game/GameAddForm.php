<?php
    class GameAddForm extends View {
        private $regionSelectorController;

        function __construct($regionSelectorController) {
            $this->regionSelectorController = $regionSelectorController;
        }

        function render() {
            echo(" 
                <script src='/views/game/scripts/select_change.js' type='text/javascript' > </script>
                <form action='/controllers/game/game-add/GameAddFormAction.php' method='post'>
                    <div class='form-group'>
                        <label for='title'>{$GLOBALS["locale"]["title"]}</label> 
                        <input class='form-control' name='title' type='text' />
                    </div>

                    <div class='form-group'>
                        <label for='year'>{$GLOBALS["locale"]["year"]}</label> 
                        <input class='form-control' name='year' type='number' />
                    </div>
            ");

            $this->regionSelectorController->render();

            echo("
                    <div class='form-group'>
                        <label for='description'>{$GLOBALS["locale"]["description"]}</label>
                        <textarea class='form-control' name='description'></textarea>
                    </div>

                    <div class='form-group'>
                        <label for='language'>{$GLOBALS["locale"]["language"]}</label>
                        <input class='form-control' name='language' type='text' />
                    </div>

                    <input class='btn btn-primary' type='submit' value='{$GLOBALS["locale"]["add"]}' />
                </form>
            ");
        }
    }
?>