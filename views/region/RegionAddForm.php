<?php
    class RegionAddForm extends View {
        private $platformSelectorController;

        function __construct($platformSelectorController) {
            $this->platformSelectorController = $platformSelectorController;
        }

        function render() {
?>
                <form action='/controllers/region/region-add/RegionAddFormAction.php' method='post'>
                    <div class='form-group'>
                        <label for='name'><?= $GLOBALS['locale']['name'] ?></label> 
                        <input class='form-control' name='name' type='text' />
                    </div>
<?php
                    $this->platformSelectorController->render();
?>
                    <div class='form-group'>
                        <label for='description'><?= $GLOBALS['locale']['description'] ?></label>
                        <textarea class='form-control' name='description'></textarea>
                    </div>

                    <input class='btn btn-primary' type='submit' value='<?= $GLOBALS['locale']['add'] ?>' />
                </form>
<?php
        }
    }
?>