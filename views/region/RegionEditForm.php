<?php
    class RegionEditForm extends View {
        private $platformSelectorController;

        private $id;
        private $name;
        private $platform_id;
        private $description;

        function __construct($platformSelectorController, $region) {
            $this->platformSelectorController = $platformSelectorController;
            
            $this->id = $region->getId();
            $this->name = $region->getName();
            $this->platform_id = $region->getPlatformId();
            $this->description = $region->getDescription();
        }

        function render() {
            echo("
                <form action='controllers/region/region-edit/RegionEditFormAction.php' method='post'>
                    <div class='form-group'>
                        <label for='id'>{$GLOBALS['locale']['id']}</label> 
                        <input class='form-control' name='id' type='text' value='$this->id' readonly/>
                    </div>
                
                    <div class='form-group'>
                        <label for='name'>{$GLOBALS['locale']['name']}</label> 
                        <input class='form-control' name='name' type='text' value='$this->name'/>
                    </div>
            ");
            
            $this->platformSelectorController->render();

            echo("
                    <div class='form-group'>
                        <label for='description'>{$GLOBALS['locale']['description']}</label>
                        <textarea class='form-control' name='description'>$this->description</textarea>
                    </div>

                    <a class='btn btn-danger' href='index.php?controller=platform&id=$this->platform_id'>{$GLOBALS["locale"]["return"]}</a>
                    <input class='btn btn-success float-right' type='submit' value='{$GLOBALS['locale']['edit']}' />
                </form>
            ");
        }
    }
?>