<?php
    class RegionSelector extends View {
        private $platformSelectorController;
        private $regions;
        private $region_id;

        function __construct($platformSelectorController, $regions = null, $region_id = null) {
            $this->platformSelectorController = $platformSelectorController;
            $this->regions = $regions;
            $this->region_id = $region_id;
        }

        function render() {
            $this->platformSelectorController->render();
?>
            <script src="/controllers/region/region-selector/region_selector_updater.js"></script>

            <div class='form-group'>
                <label for="region_id"><?= $GLOBALS['locale']['region'] ?></label>
                <select id='region_select' class='form-control' name='region_id'>
                    <option disabled selected><?= $GLOBALS['locale']['select_option'] ?></option>
<?php
                    foreach ($this->regions as $region) {
                        $id = $region->getId();
                        $name = $region->getName();
                        
                        if(!is_null($this->region_id)) { 
                            if($id == $this->region_id) {
                                echo("<option value='$id' selected>$name</option>");
                            } else {
                                echo("<option value='$id'>$name</option>");
                            }
                        } else {
                            echo("<option value='$id'>$name</option>");
                        }
                    }
?>
                </select>
            </div>
<?php 
        }
    }
?>