<?php
    class RegionListTable extends View {
        private $regions;

        function __construct($regions) {
            $this->regions = $regions;
        }

        function render() {
            echo(" 
                <table class='table'>
                    <tr>
                        <th>{$GLOBALS['locale']['id']}</th>
                        <th>{$GLOBALS['locale']['name']}</th>
                        <th></th>
                    </tr>
            ");

            if(isset($this->regions)) { 
                foreach ($this->regions as $region) {
                    $id = $region->getId();
                    $name = $region->getName();
                    $platform_id = $region->getPlatformId();
                    $description = $region->getDescription();

                    echo("
                        <tr>
                            <td>$id</td>
                            <td>$name</td>
                            <td>
                                <a class='btn btn-primary' href='/index.php?controller=region&id=" . $id . "'>{$GLOBALS["locale"]["view"]}</a>

                    ");

                    if (isset($_SESSION["user_id"])) {
                        $user_id = $_SESSION["user_id"];
                        $user = getUserById($user_id);
        
                        if ($user->isAdministrator()) {
                            echo("
                            <a class='btn btn-secondary' href='/index.php?controller=region&id=" . $id . "&action=edit'>{$GLOBALS["locale"]["edit"]}</a>
                            <a class='btn btn-danger' href='/index.php?controller=region&id=" . $id . "&action=delete'>{$GLOBALS["locale"]["delete"]}</a>
                            ");
                        }
                    }
    
                    echo("
                            </td>
                        </tr>
                    ");
                }
            }

            echo("</table>");
        }
    }
?>