<?php
    class RegionDeleteForm extends View {
        private $region;

        function __construct($region) {
            $this->region = $region;
        }

        function render() {
            $region = $this->region;

            $id = $region->getId();
            $name = $region->getName();
            $platform_id = $region->getPlatformId();
            $description = $region->getDescription();
?>
            <form action='/controllers/region/region-delete/RegionDeleteFormAction.php' method='post'>
                    <div class='form-group'>
                        <label for='id'><?= $GLOBALS['locale']['id'] ?></label> 
                        <?php echo("<input class='form-control' name='id' type='text' value='$id' readonly />"); ?>
                    </div>

                    <div class='form-group'>
                        <label for='name'><?= $GLOBALS['locale']['name'] ?></label> 
                        <?php echo("<input class='form-control' name='name' type='text' value='$name' readonly />"); ?>
                    </div>

                    <div class='form-group'>
                        <label for='platform_id'><?= $GLOBALS['locale']['platform'] ?></label> 
                        <?php echo("<input class='form-control' name='platform_id' type='text' value='$platform_id' readonly />"); ?>
                    </div>

                    <div class='form-group'>
                        <label for='description'><?= $GLOBALS['locale']['description'] ?></label>
                        <?php echo("<input class='form-control' name='description' type='text' value='$description' readonly />"); ?>
                    </div>

                <div class="mx-auto">
                    <?php echo("<a class='btn btn-danger' href='index.php?controller=platform&id=$platform_id'>{$GLOBALS['locale']['return']}</a>"); ?>
                    <input class='btn btn-success float-right' type='submit' value='<?= $GLOBALS['locale']['delete'] ?>' /> 
                </div>
            </form>
<?php
        }
    }
?>