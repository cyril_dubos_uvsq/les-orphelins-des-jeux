<?php
    class MatchCancelForm extends View {
        private $id_match;

        function __construct($id_match, $table) {
            $this->id_match = $id_match;
            $this->table = $table;
        }

        function render() {
        	?>
        			<form method="post" action="/controllers/match/match-cancel/MatchCancelFormAction.php">
        				<div class="mx-auto" style="width: 100%;">
                            <?= $GLOBALS["locale"]["$this->table"] ?>
                            :
                            <?= $GLOBALS["locale"]["match_cancel_question"] ?>
        				</div>

                        <br>

                        <div class="mx-auto">
                            <a  class="btn btn-danger" href='index.php?controller=matchs'><?= $GLOBALS["locale"]["return"] ?></a>
                            <?php
        						echo("<button class=\"btn btn-success\" type=\"submit\" name=\"id\" value=\"$this->id_match!$this->table\">{$GLOBALS['locale']['cancel']}</button>");
                            ?>
                        </div>
                    </form>
        <?php
                }
            }
        ?>
