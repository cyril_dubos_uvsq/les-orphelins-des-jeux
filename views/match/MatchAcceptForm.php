<?php
    class MatchAcceptForm extends View {
        private $id_match;
        private $table;

        function __construct($id_match, $table) {
            $this->id_match = $id_match;
            $this->table = $table;
        }

        function render() {
        	?>
        			<form method="post" action="/controllers/match/match-accept/MatchAcceptFormAction.php">
        				<div class="mx-auto" style="width: 100%;">
                            <?= $GLOBALS["locale"]["$this->table"] ?>
                            :
                            <?= $GLOBALS["locale"]["match_accept_question"] ?>
        				</div>

                        <br>

                        <div class="mx-auto">
                            <a  class="btn btn-danger" href='index.php?controller=matchs'><?= $GLOBALS["locale"]["return"] ?></a>
                            <?php
        						echo("<button class=\"btn btn-success\" type=\"submit\" name=\"id\" value=\"$this->id_match!$this->table\">{$GLOBALS['locale']['accept']}</button>");
                            ?>
                        </div>
                    </form>
        <?php
                }
            }
        ?>
