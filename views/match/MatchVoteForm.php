<?php
    class MatchVoteForm extends View {
        private $id_match;
        private $table;

        function __construct($id_match, $table) {
            $this->id_match = $id_match;
            $this->table = $table;
        }

        function render() {
        	?>
        			<form method="post" action="/controllers/match/match-vote/MatchVoteFormAction.php">
        				<div class="mx-auto" style="width: 100%;">
                            <?= $GLOBALS["locale"]["$this->table"] ?>
                            :
                            <?= $GLOBALS["locale"]["match_vote_question"] ?>
        				</div>

                        <br>

                        <div class="mx-auto">
                            <?php
        						echo("<button class=\"btn btn-success\" type=\"submit\" name=\"id\" value=\"$this->id_match!$this->table!pos\">Positive</button>");
        						echo(" <button class=\"btn btn-danger\" type=\"submit\" name=\"id\" value=\"$this->id_match!$this->table!neg\">Negative</button>");
                            ?>
                            <a  class="btn btn-secondary" href='index.php?controller=matchs'><?= $GLOBALS["locale"]["return"] ?></a>
                        </div>
                    </form>
        <?php
                }
            }
        ?>
