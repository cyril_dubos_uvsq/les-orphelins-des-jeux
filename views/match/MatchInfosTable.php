<?php
    class MatchInfosTable extends View {
        private $table;
        private $match;
        private $item;

        function __construct($table,$match,$item) {
            $this->table = $table;
            $this->match = $match;
            $this->item = $item;
        }

        function render() {
            $item = $this->item;

            $id = $item->getId();
            $game = getGameById($item->getGameId());
            $region = getRegionById($game->getRegionId());
            $platform = getPlatformById($region->getPlatformId());
            $type = null;
            switch($item->getTypeId()){
                case(1):
                    $type = "Notice";
                    break;
                case(2):
                    $type = "Boite";
                    break;
                case(3):
                    $type = "Cartouche";
                    break;
            }
            $stat_id = null;
            switch($item->getStatId()){
                case(1):
                    $stat_id = "Neuf";
                    break;
                case(2):
                    $stat_id = "Très bon";
                    break;
                case(3):
                    $stat_id = "Bon";
                    break;
                case(4):
                    $stat_id = "Moyen";
                    break;
                case(5):
                    $stat_id = "Mauvais";
                    break;
            }
            $createdAt = $item->getCreatedAt();
            $table_display = ($this->table == "searched_items") ? "Item recherché :" : "Item possédé :" ;
?>
            <form action='/controllers/match/MatchInfosTableAction.php' method='post'>

                    <div class='form-group'>
                        <?php
                        echo("<label for='table'>$table_display</label>");
                        echo("<input class='form-control' name='table' type='hidden' value='$this->table' readonly />");
                        ?>
                    </div>

                    <div class='form-group'>
                        <label for='id'>ID</label> 
                        <?php echo("<input class='form-control' name='id' type='text' value='$id' readonly />"); ?>
                    </div>

                    <div class='form-group'>
                        <label for='game'>Game</label> 
                        <?php echo("<input class='form-control' name='game' type='text' value='".$game->getTitle()." - ".$platform->getName()." - ".$region->getName()."' readonly />"); ?>
                    </div>

                    <div class='form-group'>
                        <label for='type'>Type</label> 
                        <?php echo("<input class='form-control' name='type' type='text' value='$type - $stat_id' readonly />"); ?>
                    </div>

                    <div class='form-group'>
                        <label for='created'>Created at</label> 
                        <?php echo("<input class='form-control' name='created' type='text' value='$createdAt' readonly />"); ?>
                    </div>

                <div class="mx-auto">
                    <?php echo("<a class='btn btn-secondary' href='index.php?controller=matchs'>Go back</a>"); ?>
                    
                </div>
            </form>
<?php
            if("owned_items" == $this->table){
                $matchs = getMatchesByOwnedItemId($id);
            }
            else if("searched_items" == $this->table){
                $matchs = getMatchesBySearchedItemId($id);
            }


            
            echo("
                <br />Matches list:<br />
                <table class='table'>
                    <tr>
                        <th>ID</th>
                        <th>State</th>
                        <th>User</th>
                        <th>Mail</th>
                        <th>Positive Ranking</th>
                        <th>Negative Ranking</th>
                        <th>Created at</th>
                        <th>Status</th>
                        <th>Action</th>
                    </tr>
            ");

            foreach($matchs as $match){
                if("owned_items" == $this->table){
                    $item_id = $match->getSearchedItemId();
                    $item = getItemById("searched_items",$item_id);
                    $user = getUserById($item->getUserId());
                    $stat_id = $item->getStatId();
                }
                else if("searched_items" == $this->table){
                    $item_id = $match->getOwnedItemId();
                    $item = getItemById("owned_items",$item_id);
                    $user = getUserById($item->getUserId());
                    $stat_id = $item->getStatId();
                }

                $stat_id = null;
                switch($item->getStatId()){
                    case(1):
                        $stat_id = "Neuf";
                        break;
                    case(2):
                        $stat_id = "Très bon";
                        break;
                    case(3):
                        $stat_id = "Bon";
                        break;
                    case(4):
                        $stat_id = "Moyen";
                        break;
                    case(5):
                        $stat_id = "Mauvais";
                        break;
                }
                
                $id = $match->getId();
                $username= $user->getUsername();
                $mail = $user->getEmail();
                if($match->getSearcherState()==0 || $match->getOwnerState()==0)
                    $mail = str_repeat("*", strlen($mail)) ;
                $posRank = $user->getPositiveRanking();
                $negRank = $user->getNegativeRanking();
                $createdAt = $match->getCreatedAt();
                $status = ($match->getStatus() == 1) ? "enable" : "disabled";
                
                echo("
                <tr>
                    <td>$id</td>
                    <td>$stat_id</td>
                    <td>$username</td>
                    <td>$mail</td>
                    <td>$posRank</td>
                    <td>$negRank</td>
                    <td>$createdAt</td>
                    <td>$status</td>
                    <td>");
                    
                if($match->getSearcherState()==1 && $match->getSearcherState()==$match->getOwnerState())
                    echo "
                    <a class='btn btn-primary' href='index.php?controller=matchs&action=vote&id=$id&table=$this->table'>Vote</a>
                    <a class='btn btn-danger' href='index.php?controller=matchs&action=cancel&id=$id&table=$this->table'>Annuler</a>";

                else if(!(($this->table == "searched_items" && $match->getSearcherState()==1) || ($this->table == "owned_items" && $match->getOwnerState()==1)))
                    echo "
                    <a class='btn btn-success' href='index.php?controller=matchs&action=accept&id=$id&table=$this->table'>Accepter</a>
                    <a class='btn btn-danger' href='index.php?controller=matchs&action=refus&id=$id&table=$this->table'>Refuser</a>";
                else echo "En attente";
                echo ("</td>");
            }
        }
    }
?>