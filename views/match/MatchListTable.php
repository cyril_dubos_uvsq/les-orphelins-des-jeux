<?php
    class MatchListTable extends View {
        private $table;
        private $matchs;

        function __construct($table,$matchs) {
            $this->table= $table;
            $this->matchs = $matchs;
        }

        function render() {
            echo(" 
                <table class='table'>
                    <tr>
                        <th>ID</th>
                        <th>Game</th>
                        <th>Region</th>
                        <th>Platform</th>
                        <th>Type</th>
                        <th>State</th>
                        <th>Created at</th>
                        <th>Status</th>
                        <th>Action</th>
                    </tr>
            ");

            if(isset($this->matchs)) { 
                foreach ($this->matchs as $match) {
                    if("owned_items" == $this->table){
                        $item = getItemById($this->table, $match->getOwnedItemId());
                    }
                    else if("searched_items" == $this->table){
                        $item = getItemById($this->table, $match->getSearchedItemId());
                    }
                    
                    $id = $item->getId();
                    $game = getGameById($item->getGameId());
                    $region = getRegionById($game->getRegionId());
                    $platform = getPlatformById($region->getPlatformId());
                    $type = null;
                    switch($item->getTypeId()){
                        case(1):
                            $type = "Notice";
                            break;
                        case(2):
                            $type = "Boite";
                            break;
                        case(3):
                            $type = "Cartouche";
                            break;
                    }
                    $stat_id = null;
                    switch($item->getStatId()){
                        case(1):
                            $stat_id = "Neuf";
                            break;
                        case(2):
                            $stat_id = "Très bon";
                            break;
                        case(3):
                            $stat_id = "Bon";
                            break;
                        case(4):
                            $stat_id = "Moyen";
                            break;
                        case(5):
                            $stat_id = "Mauvais";
                            break;
                    }
                    $createdAt = $item->getCreatedAt();
                    $status = ($item->getStatus() == 1) ? "enable" : "disabled";

                    echo("
                        <tr>
                            <td>$id</td>
                            <td>".$game->getTitle()."</td>
                            <td>".$region->getName()."</td>
                            <td>".$platform->getName()."</td>
                            <td>$type</td>
                            <td>$stat_id</td>
                            <td>$createdAt</td>
                            <td>$status</td>
                            <td>
                    ");

                    if (isset($_SESSION["user_id"])) {
                        $user_id = $_SESSION["user_id"];
    
                            echo("
                            <a class='btn btn-primary' href='/index.php?controller=matchs&id=" . $match->getId() . "&action=infos&table=". $this->table ."'>Infos</a>
                            ");
                        
                    }
    
                    echo("
                            </td>
                        </tr>
                    ");
                }
            }

            echo("</table>");
        }
    }
?>