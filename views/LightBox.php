<?php
    class LightBox extends View {
        private $id_item;

        function __construct($id_item) {
            $this->id_item = $id_item;
        }

        function render()
          {
          $item = getItemById("owned_items",$this->id_item);
          if($item != null)
            {
            $id_user = $item->getUserId();
            $root = realpath($_SERVER["DOCUMENT_ROOT"]);
            $compteur = 1;
            $test = false;
            echo ("<td>");
            while ($compteur < 4)
              {
                if (file_exists("$root/models/images/items/$id_user/$this->id_item/$compteur.png") and $test==false)
                  {
                  $test = true;
                  echo ("<a class='btn btn-primary' href='models/images/items/$id_user/$this->id_item/$compteur.png' data-lightbox='gallery$this->id_item' data-title='Photo n°$compteur'>Photos</a>");
                  }
                  else if (file_exists("$root/models/images/items/$id_user/$this->id_item/$compteur.png")) {
                    echo ("<a href='models/images/items/$id_user/$this->id_item/$compteur.png' data-lightbox='gallery$this->id_item' data-title='Photo n°$compteur'/>");
                  }
                $compteur = $compteur + 1;
              }
              if ($test == false) {
                echo ("<button class='btn btn-primary' disabled>Photos</button>");
              }
          echo ("</td>");
          }
        }
      }
?>
