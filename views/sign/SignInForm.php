<?php
    class SignInForm extends View {
        function render() {
?>
            <form method="post" action="/controllers/sign/SignInFormAction.php">
                <div class="row">
                    <div class="col-md-6 form-group">
                        <label class="control-label" for="username"><?= $GLOBALS["locale"]["username"] ?></label>
                        <input class="form-control" type="text" name="username" required pattern="{4,20}"/>
                    </div>
                    <div class="col-md-6 form-group">
                        <label class="control-label" for="password"><?= $GLOBALS["locale"]["password"] ?></label>
                        <input class="form-control" type="password" name="password" required pattern="{8,20}"/>
                    </div>
                </div>
                
                <button class= "btn btn-primary" type="submit"><?= $GLOBALS["locale"]["sign_in"] ?></button>
            </form>
<?php
        }
    }
?>