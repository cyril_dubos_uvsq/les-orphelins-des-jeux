<?php
    class SignUpForm extends View {
        function render() {
?>
            <form method="post" action="/controllers/sign/SignUpFormAction.php" enctype="multipart/form-data">
                <div class="form-group">
                    <label for="username">
                        <?= $GLOBALS["locale"]["username"] ?>
                    </label>
                    <input type="text" name="username" class="form-control" required />
                </div>

                <div class="row">
                    <div class="col-md-6 form-group">
                        <label for="password">
                            <?= $GLOBALS["locale"]["password"] ?>
                        </label>
                        <input type="text" name="password" class="form-control" required />
                    </div>

                    <div class="col-md-6 form-group">
                        <label for="c_password">
                            <?= $GLOBALS["locale"]["confirm_password"] ?>
                        </label>
                        <input type="text" name="c_password" class="form-control" required />
                    </div>
                </div>

                <hr />

                <div class="row">
                    <div class="col-md-6 form-group">
                        <label for="firstname">
                            <?= $GLOBALS["locale"]["firstname"] ?>
                        </label>
                        <input type="text" name="firstname" class="form-control" required />
                    </div>

                    <div class="col-md-6 form-group">
                        <label for="lastname">
                            <?= $GLOBALS["locale"]["lastname"] ?>
                        </label>
                        <input type="text" name="lastname" class="form-control" required />
                    </div>
                </div>

                <div class="form-group">
                    <label for="email">
                        <?= $GLOBALS["locale"]["email"] ?>
                    </label>
                    <input type="email" name="email" class="form-control" required />
                </div>

                <hr />
<?php
                $provinceSelectorController = new ProvinceSelectorController();
                $provinceSelectorController->render();
?>

                <div class="form-group">
                    <label for="avatar"><?= $GLOBALS["locale"]["avatar"] ?></label>
                    <input type="file" name ="avatar" class="form-control-file">
                </div>

                <hr />

                <div class="form-group">
                    <div class="form-check">
                        <input class="form-check-input" type="checkbox" id="conditions" required />
                        <label class="form-check-label" for="conditions">
                            <?= $GLOBALS["locale"]["conditions_check"] ?>
                        </label>

                            <a href="" id="rgpd-button" data-toggle="modal" data-target="#rgpd-modal"><?= $GLOBALS["locale"]["view"] ?></a>

                            <div id="rgpd-modal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                <div class="modal-dialog" role="document">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <h5 class="modal-title" id="exampleModalLabel">RGPD</h5>
                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                <span aria-hidden="true">&times;</span>
                                            </button>
                                        </div>

                                        <div class="modal-body">
                                            <p>Les informations personnelles collectées dans le cadre de votre activité chez Les Orphelins Des Jeux sont enregistrées dans des fichiers informatisés.
                                            <p>Les Orphelins Des Jeux ne traite ou n'utilise ces données que dans la mesure où cela est nécessaire (assurer les communications entre les utilisateurs).
                                            <p>Les informations personnelles sont conservées pendant une durée qui ne saurait excéder 2 ans.
                                            <p>Une durée de conservation plus longue est autorisée ou imposée en vertu d'une obligation légale ou règlementaire.
                                            <p>Pendant cette période, Les Orphelins Des Jeux met en œuvre tous moyens pour assurer la confidentialité et la sécurité des données personnelles, de manière à empêcher leur endommagement, effacement ou accès par des tiers non autorisés.
                                            <p>L'accès aux données personnelles est strictement limité aux membres des Orphelins Des Jeux souhaitant vous contacter afin de faire un échange si vous êtes d’accord.
                                            <p>Concernant l'admnistrateur, il utilisera les données personnelles des utilisateurs uniquement dans le but d'assurer le bon fonctionnement des Orphelins Des Jeux.
                                            <p>En dehors des cas énoncés ci-dessus, Les Orphelins des Jeux s'engage à ne pas céder ni donner accès à des tiers, les données sans le consentement préalable des intéressés, à moins d'y être contraints en raison d'un motif légitime (obligation légale, lutte contre la fraude ou l'abus, exercice des droits de la défense, etc.).
                                            <br>
                                            <p>Toute information complémentaire peut être obtenue auprès à l’adresse suivante: <a href="mailto: contact@lesorphelinsdesjeux.com">contact@lesorphelinsdesjeux.com</a></p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                    </div>
                </div>

                <div class="form-group">
                    <input class='btn btn-primary' type='submit' value="<?= $GLOBALS["locale"]["sign_up"] ?>" />
                </div>
            </form>
<?php
        }
    }
?>
