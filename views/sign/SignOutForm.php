<?php
    class SignOutForm extends View {
        function render() {
?>
            <form method="post" action="/controllers/sign/SignOutFormAction.php">
                <div class="mx-auto" style="width: 100%;">
                    <?= $GLOBALS["locale"]["sign_out_question"] ?>
                </div>

                <br>

                <div class="mx-auto">
                    <button class="btn btn-danger" name="submit" type="submit" value="0"><?= $GLOBALS["locale"]["return"] ?></button>
                    <button class="btn btn-success" name="submit" type="submit" value="1"><?= $GLOBALS["locale"]["sign_out"] ?></button>
                </div>
            </form>
<?php
        }
    }
?>
