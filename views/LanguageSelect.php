<?php
    class LanguageSelect extends View {
        function render() { 
?>
            <form class="form-inline">
                <script src="/controllers/language/LanguageSelectScript.js"></script>

                <select class='form-control' style="height: 42px; margin-left: 4px;" id="language" name='language' onchange="languageSelect()">
<?php
                    $root = realpath($_SERVER["DOCUMENT_ROOT"]);
                    $files = scandir("$root/locales");

                    foreach ($files as $file) {
                        if($file != "." && $file != "..") {
                            $language = pathinfo($file, PATHINFO_FILENAME);

                            if(isset($_COOKIE["language"])) {
                                if($_COOKIE["language"] == $language) {
                                    echo("
                                        <option value='$language' selected>{$GLOBALS['locale'][$language]}</option>
                                    ");
                                } else {
                                    echo("
                                        <option value='$language'>{$GLOBALS['locale'][$language]}</option>
                                    ");
                                }
                            } else {
                                if ($language == "fr_FR") {
                                    echo("
                                        <option value='$language' selected>{$GLOBALS['locale'][$language]}</option>
                                    ");
                                } else {
                                    echo("
                                        <option value='$language'>{$GLOBALS['locale'][$language]}</option>
                                    ");
                                }
                            }
                        }
                    }
?>
                </select>
            </form>
<?php 
        }
    }
?>