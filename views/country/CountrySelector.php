<?php
    class CountrySelector extends View {
        private $countries;
        private $country_id;

        function __construct($countries = null, $country_id = null) {
            $this->countries = $countries;
            $this->country_id = $country_id;
        }

        function render() {
?>
            <script src="/controllers/country/country-selector/country_selector_updater.js"></script>

            <div class='form-group'>
                <label for="country_id"><?= $GLOBALS["locale"]["country"] ?></label>
                <select id='country_select' class='form-control' name='country_id'>
                    <option disabled selected><?= $GLOBALS["locale"]["select_option"] ?></option>
<?php
                    foreach ($this->countries as $country) {
                        $id = $country->getId();
                        $name = $country->getCountry();
                        
                        if(!is_null($this->country_id)) { 
                            if($id == $this->country_id) {
                                echo("<option value='$id' selected>$name</option>");
                            } else {
                                echo("<option value='$id'>$name</option>");
                            }
                        } else {
                            echo("<option value='$id'>$name</option>");
                        }
                    }
?>
                </select>
            </div>
<?php 
        }
    }
?>