<?php
    class Card extends View {
        private $title;
        private $element;

        function __construct($title, $element) {
            $this->title = $title;
            $this->element = $element;
        }

        function render() {
            echo("
                <div class='card m-5'>
                    <div class='card-header'>$this->title</div>
                    <div class='card-body'>
            ");

                        $this->element->render();

            echo("
                    </div>
                </div>
            ");
        }
    }
?>