<?php
    class Navbar extends View {
        function render() {
            $root = realpath($_SERVER["DOCUMENT_ROOT"]);
?>
            <nav class="sticky-top navbar navbar-light bg-light">
                <a class="navbar-brand" href="index.php"><?= $GLOBALS["locale"]["website_title"] ?></a>
                <ul class="nav">
                    <li class="nav-item">
                        <a class="nav-link" href="index.php"><?= $GLOBALS["locale"]["home"] ?></a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="index.php?controller=database"><?= $GLOBALS["locale"]["database"] ?></a>
                    </li>
<?php
                    session_start();

                    if (isset($_SESSION["user_id"])) {
                        $user_id = $_SESSION["user_id"];
                        $user = getUserById($user_id);
                        $username = $user->getUsername();
                        $avatar = $user->getAvatar();
                        $notifications = getNotificationsOfToday();
                        $nb_notifications = sizeof($notifications);

                        //NavbarAdmin
                        if ($user->isAdministrator()) {
                           echo("
                            <li class='nav-item'>
                                <a class='nav-link text-danger' href='index.php?controller=admin&action=users'>Users</a>
                            </li>
                            <li class='nav-item'>
                                <a class='nav-link text-danger' href='index.php?controller=admin&action=items'>Items</a>
                            </li>
                            <li class='nav-item'>
                                <a class='nav-link text-danger' href='index.php?controller=notification'>Notification</a>
                            </li>
                            ");
                        } else {
?>
                            <link rel="stylesheet" type="text/css" href="views/Navbar_style.css">
                            <li class="nav-item">
                                <a class="nav-link" href="index.php?controller=items"><?= $GLOBALS["locale"]["my_items"] ?></a>
                            </li>

                            <li class="nav-item">
                                <a class="nav-link" href="index.php?controller=matchs"><?= $GLOBALS["locale"]["my_matches"] ?></a>
                            </li>
                            <li class="nav-item">
                                <a tabindex="0"  type="button" id="notification-button" class="nav-link btn btn-success text-light"
                                    data-container="body" data-toggle="popover" data-placement="bottom" 
                                    data-content="
                                        <?php
                                            foreach ($notifications as $notification) {
                                                $date = $notification->getDate();
                                                $content = $notification->getContent();
                                        ?>
                                            <div class='p-2'>
                                                <b class='d-block'><?= $date ?></b>
                                                <p class='d-block'><?= $content ?></p>
                                            </div>
                                        <?php
                                            }
                                        ?>
                                    "
                                >
                                    Notifications <span class="badge badge-light"><?= $nb_notifications ?></span>
                                </a>
                            </li>
<?php
                        }
?>
                        <li class="nav-item">
                            <a class="nav-link btn btn-outline-primary" style="margin-left: 4px;" href="index.php?controller=sign&action=out"><?= $GLOBALS["locale"]["sign_out"] ?></a>
                        </li>

                        <li class="nav-item">
                            <a class="nav-link btn btn-primary" style="margin-left: 4px;" href="index.php?controller=user&action=profile"><img class="rounded-circle" width="16" height="16" src="<?= $avatar ?>" /> <?= $username ?></a>
                        </li>
<?php
                    } else {
?>
                        <li class="nav-item">
                            <a class="nav-link btn btn-outline-primary" style="margin-left: 4px;" href="index.php?controller=sign&action=in"><?= $GLOBALS["locale"]["sign_in"] ?></a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link btn btn-primary" style="margin-left: 4px;" href="index.php?controller=sign&action=up"><?= $GLOBALS["locale"]["sign_up"] ?></a>
                        </li>
<?php
                    }

                    $languageSelect = new LanguageSelect();
                    $languageSelect->render();
?>
                </ul>
            </nav>
<?php
        }
    }
?>
