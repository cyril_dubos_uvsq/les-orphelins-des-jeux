<?php
    class PlatformAddForm extends View {
        function render() {
?>
            <form action='/controllers/platform/platform-add/PlatformAddFormAction.php' method='post'>
                <div class='form-group'>
                    <label for='name'><?= $GLOBALS['locale']['name'] ?></label> 
                    <input class='form-control' name='name' type='text' />
                </div>

                <div class='form-group'>
                    <label for='description'><?= $GLOBALS['locale']['description'] ?></label>
                    <textarea class='form-control' name='description'></textarea>
                </div>
<?php
                echo("<input class='btn btn-primary' type='submit' value='{$GLOBALS['locale']['add']}' />");
?>
            </form>
<?php
        }
    }
?>