<?php
    class PlatformListTable extends View {
        private $platforms;

        function __construct($platforms) {
            $this->platforms = $platforms;
        }

        function render() {
            echo(" 
                <table class='table'>
                    <tr>
                        <th>{$GLOBALS['locale']['id']}</th>
                        <th>{$GLOBALS['locale']['name']}</th>
                        <th></th>
                    </tr>
            ");

            foreach ($this->platforms as $platform) {
                $id = $platform->getId();
                $name = $platform->getName();
                $description = $platform->getDescription();

                echo("
                    <tr>
                        <td>$id</td>
                        <td>$name</td>
                        <td>
                            <a class='btn btn-primary' href='/index.php?controller=platform&id=" . $id . "'>{$GLOBALS["locale"]["view"]}</a>
                ");

                if (isset($_SESSION["user_id"])) {
                    $user_id = $_SESSION["user_id"];
                    $user = getUserById($user_id);
    
                    if ($user->isAdministrator()) {
                        echo("
                            <a class='btn btn-secondary' href='/index.php?controller=platform&id=" . $id . "&action=edit'>{$GLOBALS["locale"]["edit"]}</a>
                            <a class='btn btn-danger' href='/index.php?controller=platform&id=" . $id . "&action=delete'>{$GLOBALS["locale"]["delete"]}</a>
                        ");
                    }
                }

                echo("
                        </td>
                    </tr>
                ");
            }

            echo("</table>");
        }
    }
?>