<?php
    class PlatformDeleteForm extends View {
        private $platform;

        function __construct($platform) {
            $this->platform = $platform;
        }

        function render() {
            $platform = $this->platform;

            $id = $platform->getId();
            $name = $platform->getName();
            $description = $platform->getDescription();
?>
            <form action='/controllers/platform/platform-delete/PlatformDeleteFormAction.php' method='post'>
                    <div class='form-group'>
                        <label for='id'><?= $GLOBALS['locale']['id'] ?></label> 
                        <?php echo("<input class='form-control' name='id' type='text' value='$id' readonly />"); ?>
                    </div>

                    <div class='form-group'>
                        <label for='name'><?= $GLOBALS['locale']['name'] ?></label> 
                        <?php echo("<input class='form-control' name='name' type='text' value='$name' readonly />"); ?>
                    </div>

                    <div class='form-group'>
                        <label for='description'><?= $GLOBALS['locale']['description'] ?></label>
                        <?php echo("<input class='form-control' name='description' type='text' value='$description' readonly />"); ?>
                    </div>

                <div class="mx-auto">
                    <?php echo("<a class='btn btn-danger' href='index.php?controller=database'>{$GLOBALS['locale']['return']}</a>"); ?>
                    <input class='btn btn-success float-right' type='submit' value='<?= $GLOBALS['locale']['delete'] ?>' />
                </div>
            </form>
<?php
        }
    }
?>