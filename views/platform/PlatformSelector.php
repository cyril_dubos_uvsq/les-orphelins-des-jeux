<?php
    class PlatformSelector extends View {
        private $platforms;
        private $platform_id;

        function __construct($platforms, $platform_id = null) {
            $this->platforms = $platforms;
            $this->platform_id = $platform_id;
        }

        function render() {
?>
            <script src="/controllers/platform/platform-selector/platform_selector_updater.js"></script>
            
            <div id="platform-selector" class='form-group'>
                <label for="platform_id"><?= $GLOBALS['locale']['platform'] ?></label>
                <select id="platform_select" class='form-control' name='platform_id'>
                    <option disabled selected><?= $GLOBALS['locale']['select_option'] ?></option>
<?php      
                    foreach ($this->platforms as $platform) {
                        $id = $platform->getId();
                        $name = $platform->getName();
                        
                        if(!is_null($this->platform_id)) { 
                            if($id == $this->platform_id) {
                                echo("<option value='$id' selected>$name</option>");
                            } else {
                                echo("<option value='$id'>$name</option>");
                            }
                        } else {
                            echo("<option value='$id'>$name</option>");
                        }
                    }
?>
                </select>
            </div>
<?php 
        }
    }
?>