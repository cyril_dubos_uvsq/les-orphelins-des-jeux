<?php
    class PlatformEditForm extends View {
        private $id;
        private $name;
        private $description;

        function __construct($platform) {
            $this->id = $platform->getId();
            $this->name = $platform->getName();
            $this->description = $platform->getDescription();
        }

        function render() {
            echo(" 
                <form action='/controllers/platform/platform-edit/PlatformEditFormAction.php' method='post'>
                    <div class='form-group'>
                        <label for='id'>{$GLOBALS['locale']['id']}</label> 
                        <input class='form-control' name='id' type='text' value='$this->id' readonly/>
                    </div>
                
                    <div class='form-group'>
                        <label for='name'>{$GLOBALS['locale']['name']}</label> 
                        <input class='form-control' name='name' type='text' value='$this->name'/>
                    </div>

                    <div class='form-group'>
                        <label for='description'>{$GLOBALS['locale']['description']}</label>
                        <textarea class='form-control' name='description'>$this->description</textarea>
                    </div>

                    <a class='btn btn-danger' href='index.php?controller=database'>{$GLOBALS["locale"]["return"]}</a>
                    <input class='btn btn-success float-right' type='submit' value='{$GLOBALS['locale']['edit']}' />
                </form>
            ");
        }
    }
?>