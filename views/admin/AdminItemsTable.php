<?php
	class AdminItemsTable extends View {
    	private $item_searched;
    	private $item_owned;
    	private $user;

		function __construct($item_searched,$item_owned) {
			$this->item_searched = $item_searched;
			$this->item_owned = $item_owned;
			$this->user = get("user");
		}


		function render() {
			$root = realpath($_SERVER["DOCUMENT_ROOT"]);
?>
			<div class='form-group'>
                <label for='search'><?= $GLOBALS['locale']['search'] ?></label>
                <input class='form-control' id="search" name='search' type='text' value="<?= $this->user ?>" />
			</div>

			<form method='post' action='/controllers/item/ItemLockFormAction.php'>
				<table class="table" id="items_table" >
					<tr id='header'>
						<th>ID</th>
						<th><?= $GLOBALS['locale']['username'] ?></th>
						<th><?= $GLOBALS['locale']['game_name'] ?></th>
						<th><?= $GLOBALS['locale']['game_type'] ?></th>
						<th><?= $GLOBALS['locale']['date_item'] ?></th>
						<th>Status</th>
						<th><?= $GLOBALS['locale']['pictures'] ?></th>
						<th>Actions</th>
					</tr>
<?php
					foreach ($this->item_owned as $item) {
						$id_item = $item->getId();
						$id_user = $item->getUserId();

						$user=getUserById($id_user);
						$username = $user->getUsername();

						$game_id = $item->getGameId();
						$game = getGameById($game_id);
						$game_name = $game->getTitle();
						$type_id = $item->getTypeId();

						switch($type_id){
						  case(1):
							  $type = "Notice";
							  break;
						  case(2):
							  $type = "Boite";
							  break;
						  case(3):
							  $type = "Cartouche";
							  break;
						}

						$creation_date = $item->getCreatedAt();
						$status = $item->getStatus();

						if($status == 0) {
							$status = $GLOBALS['locale']['disabled'];
						} else {
							$status = $GLOBALS['locale']['active'];
						}
?>
						<tr>
							<td><?= $id_item ?></td>
							<td><?= $username ?></td>
							<td><?= $game_name ?></td>
							<td><?= $type ?></td>
							<td><?= $creation_date ?></td>
							<td><?= $status ?></td>
<?php
							$lightBox = new LightBox($id_item);
							$lightBox->render();

							if($status == $GLOBALS['locale']['active'] ) {
								echo("<td><button class='btn btn-danger' type=submit value=$id_item name=\"Bloquer\">{$GLOBALS['locale']['lock']}</button></td>");
							} else {
								echo("<td><button class='btn btn-danger' type=submit value=$id_item name=\"Debloquer\">{$GLOBALS['locale']['unlock']}</button></td>");
							}
?>
						</tr>
<?php
					}
?>
				</table>
			</from>

			<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
			<script src="/controllers/admin/AdminItemsTableSearch.js"></script>
<?php
		}
	}
?>
