<?php
    class AdminUsersTable extends View {
    	private $users;

		function __construct($users) {
			$this->users = $users;
		}


		function render() {
			$root = realpath($_SERVER["DOCUMENT_ROOT"]);
?>
			<div class='form-group'>
                <label for='search'><?= $GLOBALS['locale']['search'] ?></label> 
                <input class='form-control' id="search" name='search' type='text' />
			</div>

			<!--
			<div class='form-group'>
            	<button class='btn btn-primary' type="submit"><?= $GLOBALS['locale']['research'] ?></button>
            </div>
			-->
				<table class='table' id='users_table'>
            		<tr id='header'>
						<th><?= $GLOBALS['locale']['avatar'] ?></th>
                        <th>ID</th>
						<th><?= $GLOBALS['locale']['username'] ?></th>
						<th><?= $GLOBALS['locale']['firstname'] ?></th>
						<th><?= $GLOBALS['locale']['lastname'] ?></th>
						<th><?= $GLOBALS['locale']['email'] ?></th>
						<th><?= $GLOBALS['locale']['country'] ?></th>
						<th>Status</th>
						<th>Actions</th>
            		</tr>
<?php
					foreach ($this->users as $user) {
						$id = $user->getId();
						$firstname = $user->getFirstname();
						$lastname = $user->getLastname();
						$username = $user->getUsername();
						$mail = $user->getEmail();
						$province_id = $user->getProvinceId();
						$country = getCountryById($province_id);
						if ($country) {
							$country_name = $country->getCountry();
						} 
						$status = $user->getStatus();
						$avatar = $user->getAvatar();

						if($status == 0) {
							$status = "Banni(e)";
						} else {
							$status ="Actif(ve)";
						}
?>
		  				<tr>
							<td>
								<img src='<?= $avatar ?>'  class="img-fluid img-circle"/>
							</td>
							<td><?= $id ?></td>
							<td><?= $username ?></td>
							<td><?= $firstname ?></td>
							<td><?= $lastname ?></td>
							<td><?= $mail ?></td>
							<td><?= $country_name ?></td>
							<td><?= $status ?></td>
							<td>
								<a class='btn btn-primary' href='index.php?controller=admin&action=items&user=<?= $username ?>'><?= $GLOBALS["locale"]["items"] ?></a>
								<a class='btn btn-secondary' href='index.php?controller=user&action=edit&id=<?= $id ?>'><?= $GLOBALS["locale"]["edit"] ?></a>
<?php
								if($status=="Actif(ve)"){
									echo("<a class='btn btn-danger' href='index.php?controller=user&action=lock&id=$id'>Bloquer</a>");
								}
								else {
									echo("<a class='btn btn-danger' href='index.php?controller=user&action=lock&id=$id'>Debloquer</a>");
								}
?>							</td>
						</tr>
<?php
					}
?>
				</table>

			<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
			<script src="/controllers/admin/AdminUsersTableSearch.js"></script>
<?php
		}
    }
?>
