<?php
    class ItemListTable extends View {
        private $table;
        private $items;

        function __construct($table,$items) {
            $this->table= $table;
            $this->items = $items;
        }

        function render() {
            echo("
                <table class='table'>
                    <tr>
                        <th>{$GLOBALS['locale']['title']}</th>
                        <th>{$GLOBALS['locale']['region']}</th>
                        <th>{$GLOBALS['locale']['platform']}</th>
                        <th>{$GLOBALS['locale']['type']}</th>
                        <th>{$GLOBALS['locale']['stat']}</th>
                        <th>{$GLOBALS['locale']['created_at']}</th>
                        <th>{$GLOBALS['locale']['status']}</th>
                        ");
                      if($this->table == "owned_items") {
                        echo ("<th>{$GLOBALS['locale']['pictures']}</th>");
                      }
                      echo ("
                        <th>Actions</th>
                    </tr>
            ");

            if(isset($this->items)) {
                foreach ($this->items as $item) {
                    $id_item = $item->getId();
                    $game = getGameById($item->getGameId());
                    $region = getRegionById($game->getRegionId());
                    $platform = getPlatformById($region->getPlatformId());
                    $type = null;
                    switch($item->getTypeId()){
                      case(1):
                          $type = "Notice";
                          break;
                      case(2):
                          $type = "Boite";
                          break;
                      case(3):
                          $type = "Cartouche";
                          break;

                    }
                    $stat = null;
                    switch($item->getStatId()){
                      case(1):
                          $stat = "Neuf";
                          break;
                      case(2):
                          $stat = "Très bon";
                          break;
                      case(3):
                          $stat = "Bon";
                          break;
                      case(4):
                          $stat = "Moyen";
                          break;
                      case(5):
                          $stat = "Mauvais";
                          break;
                    }
                    $createdAt = $item->getCreatedAt();
                    $status = ($item->getStatus() == 1) ? "enable" : "disabled";

                    echo("
                        <tr>
                            <td>".$game->getTitle()."</td>
                            <td>".$region->getName()."</td>
                            <td>".$platform->getName()."</td>
                            <td>$type</td>
                            <td>$stat</td>
                            <td>$createdAt</td>
                            <td>$status</td>
                    ");

                    if (isset($_SESSION["user_id"])) {
                        $user_id = $_SESSION["user_id"];
                        $user = getUserById($user_id);
                        //if ($user->isAdministrator()) {
                        $lightBox = new LightBox($id_item);
                        $lightBox->render();
                        //}
                    }

                    echo("
                        <td><a class='btn btn-secondary' href='index.php?controller=items&action=edit&id=$id_item&table=$this->table'>{$GLOBALS['locale']['edit']}</a>
                        <a class='btn btn-danger' href='index.php?controller=items&action=delete&id=$id_item&table=$this->table'>{$GLOBALS['locale']['delete']}</a></td>
                        </tr>
                    ");
                }
            }

            echo("</table>");
        }
    }
?>
