<?php
    class ItemAddForm extends View {
        private $platforms;
        private $regions;
        private $games;
        private $platform_id;
        private $region_id;
        private $game_id;

        function __construct($platforms, $regions, $games, $platform_id, $region_id, $game_id) {
            $this->platforms = $platforms;
            $this->regions = $regions;
            $this->games = $games;
            $this->platform_id = $platform_id;
            $this->region_id = $region_id;
            $this->game_id = $game_id;
        }

        function render() {
            echo("
                <script src='/views/item/scripts/select_change.js' type='text/javascript' > </script>
                <form action='/controllers/item/item-add/ItemAddFormAction.php' method='post'  enctype='multipart/form-data'>
                    <div class='form-group'>
                        <div class='row'>
                            <div class='col-md-6'>
                            <div class='text-center'>
                                <input name='table' id=\"owned\" value='owned_items' type='radio' checked onclick=\"pictures()\"/> {$GLOBALS['locale']['i_own']}
                            </div>

                            </div>
                            <div class='col-md-6'>
                                <div class='text-center'>
                                    <input name='table' id=\"searched\" value='searched_items' type='radio' onclick=\"pictures()\"/> {$GLOBALS['locale']['i_search']}
                                </div>
                            </div>
                        </div>
                    </div>
            ");

            $gameSelectorController = new GameSelectorController($this->game_id);
            $gameSelectorController->render();

            echo("
                    <div class='form-group'>
                        <label for='type_id'>{$GLOBALS['locale']['type']}</label>
                        <select class='form-control' name='type_id'>
                            <option disabled >Sélectionnez une option</option>
                            <option value='1' selected>Notice</option>
                            <option value='2'>Boite</option>
                            <option value='3'>Cartouche</option>
                        </select>
                    </div>

                    <div class='form-group'>
                        <label for='stat_id'>{$GLOBALS['locale']['state']}</label>
                        <select class='form-control' name='stat_id'>
                            <option disabled >Sélectionnez une option</option>
                            <option value='1' selected>Neuf</option>
                            <option value='2'>Très bon</option>
                            <option value='3'>Bon</option>
                            <option value='4'>Moyen</option>
                            <option value='5'>Mauvais</option>
                        </select>
                    </div>


                    <script>

                    function pictures() {
                        if (document.getElementById(\"owned\").checked == true) {
                          document.getElementById('photos').innerHTML='<label for=\"type_id\">{$GLOBALS['locale']['pictures']}</label><input class=\"form-control-file\" id=\"images_item\" type=\"file\" name=\"images_item[]\" multiple accept=\"image/x-png,image/jpeg\"/>';

                        }
                        else if (document.getElementById(\"searched\").checked == true) {
                            document.getElementById('photos').innerHTML='';
                        }
                        else {
                            // DO NOTHING
                            }
                        }


                    </script>

                    <div class='form-group' id='photos'/>
                        <label for='type_id'>{$GLOBALS['locale']['pictures']}</label>
                        <input class='form-control-file' id='images_item' type='file' name='images_item[]' multiple accept=\"image/x-png,image/jpeg\"/>
                    </div>
                    <input class='btn btn-primary' type='submit' value='{$GLOBALS['locale']['add']}' />
                </form>
            ");
        }
    }
?>
