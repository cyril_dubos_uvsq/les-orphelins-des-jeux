<?php
    class ItemEditForm extends View {
        private $item;
        private $table;

        function __construct($item,$table) {
            $this->item = $item;
            $this->table = $table;
        }

        function render() {
            $item = $this->item;
            $table = $this->table;
            $root = realpath($_SERVER["DOCUMENT_ROOT"]);

            $id_user = $item->getUserId();
            $id_item = $item->getId();
            $game = getGameById($item->getGameId());
            $title = $game->getTitle();
            $region_id = $game->getRegionId();
            $region = getRegionById($region_id);
            $region_name = $region->getName();
            $date = $item->getCreatedAt();
            $stat =$item->getStatId();
            $stat_name = null;
            switch($stat){
              case(1):
                  $stat_name = "Neuf";
                  break;
              case(2):
                  $stat_name = "Très bon";
                  break;
              case(3):
                  $stat_name = "Bon";
                  break;
              case(4):
                  $stat_name = "Moyen";
                  break;
              case(5):
                  $stat_name = "Mauvais";
                  break;
            }
?>
            <form method="post" action="/controllers/item/item-edit/ItemEditFormAction.php" enctype="multipart/form-data">
                <input type="hidden" name="id" value="<?php echo($id_item); ?>" class="form-control"/>
                <div class="form-group">
                    <label for="title"><?=$GLOBALS["locale"]["title"] ?></label>
                    <input class='form-control' name="title" readonly value="<?= $title ?>" />
                </div>

                <div class="form-group">
                    <label for="region"><?=$GLOBALS["locale"]["region"] ?></label>
                    <input class='form-control' name="region" readonly value="<?= $region_name ?>" />
                </div>

                <div class="form-group">
                    <label for="date"><?=$GLOBALS["locale"]["created_at"] ?></label>
                    <input class='form-control' name="created_at" readonly value="<?= $date ?>" />
                </div>

                <div class="form-group">
                    <label for="stat"><?=$GLOBALS["locale"]["stat"] ?></label>
                    <select class='form-control' name='stat_id'>
                            <?php
                            echo ("<option disabled selected value='$stat' >$stat_name</option>");
                            ?>
                              <option value='1'>Neuf</option>
                              <option value='2'>Très bon</option>
                              <option value='3'>Bon</option>
                              <option value='4'>Moyen</option>
                              <option value='5'>Mauvais</option>
                    </select>
                </div>
                
                <div class="form-group mb-5">
                    <a class="btn btn-danger" href="index.php?controller=items"><?= $GLOBALS["locale"]["return"] ?></a>
                    <button class="btn btn-success float-right" type="submit" name='stat' value='<?php echo ("$table!$id_item");?>' >Modifier</button>
                </div>

                <?php
                    if($table == "owned_items") {
                ?>
                        <?php
                            $i = 1;
                            while ($i < 4) {
                                if(file_exists("$root/models/images/items/$id_user/$id_item/$i.png")) {
                        ?>
                                    <hr>
                                    <div class="row">
                                        <div class="col-md-4">
                                            <img class="img-fluid" src="models/images/items/<?= $id_user ?>/<?= $id_item ?>/<?= $i ?>.png" />
                                        </div>
                                        
                                        <div class="col-md-8">
                                            
                                            <div class='form-group'>
                                                <div class='input-group input-file' name='Fichier1'>
                                                    <input id='image_edit<?= $i ?>' name='image_edit<?= $i ?>' type='file' accept="image/x-png,image/jpeg">
                                                </div>
                                            </div>

                                            <button class="btn btn-secondary" type="submit" name='edit' value='<?= $table ?>!<?= $id_item ?>!<?= $i ?>' >Modifier</button>
                                            <button class="btn btn-danger" type="submit" name='delete' value='<?= $table ?>!<?= $id_item ?>!<?= $i ?>' >Supprimer</button>
                                        </div>
                                    </div>
                        <?php
                                } else {
                        ?>
                                    <hr>
                                    <div class='form-group'>
                                        <div class='input-group input-file' name='Fichier1'>
                                            <input id='image_add<?= $i ?>' name='image_add<?= $i ?>' type='file'>
                                            <button class="btn btn-primary" type="submit" name='add' value='<?= $table ?>!<?= $id_item ?>!<?= $i ?>' >Ajouter</button>
                                        </div>
                                    </div>
                        <?php
                                }
                                  
                                $i = $i+1;
                            }
                        ?>
            </form> 
<?php
            } 
        }
    }
?>
