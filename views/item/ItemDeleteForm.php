<?php
    class ItemDeleteForm extends View {
        private $id_item;
        private $table;

        function __construct($id_item, $table) {
            $this->id_item = $id_item;
            $this->table = $table;
        }

        function render() {
        	?>
        			<form method="post" action="/controllers/item/item-delete/ItemDeleteFormAction.php">
        				<div class="mx-auto" style="width: 100%;">
                            <?= $GLOBALS["locale"]["$this->table"] ?>
                            :
                            <?= $GLOBALS["locale"]["item_delete_question"] ?>
        				</div>

                        <br>

                        <div class="mx-auto">
                            <a  class="btn btn-danger" href='index.php?controller=items'><?= $GLOBALS["locale"]["return"] ?></a>
                            <?php
        						echo("<button class=\"btn btn-success\" type=\"submit\" name=\"id\" value=\"$this->id_item!$this->table\">{$GLOBALS['locale']['delete']}</button>");
                            ?>
                        </div>
                    </form>
        <?php
                }
            }
        ?>
