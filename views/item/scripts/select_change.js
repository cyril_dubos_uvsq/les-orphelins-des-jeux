   
window.onload = initPage;

function initPage(){	
	document.getElementById("platform_id").onchange = displayRegions;
	document.getElementById("region_id").onchange = displayGames;
}

function callback_region(){
	if(xhr.readyState == 4 && xhr.status == 200){
		txt = xhr.responseText;
		txt = txt.split(";");
		
		div = document.getElementById("region_id");
		
		while(div.childNodes.length != 0){
			div.removeChild(div.childNodes[0]);
		}

		elt = document.createElement("option");
		elt.disabled = true;

		if(txt[0] == ""){
			elt.selected = true;
			elt.appendChild(document.createTextNode("No option available"));
		} else{
			elt.selected = false;			
			elt.appendChild(document.createTextNode("Select an option"));
		}
		div.appendChild(elt);

		for(t in txt){
			if(txt[t] != ""){
				str = txt[t].split("/=/");
				elt = document.createElement("option");
				value = parseInt(str[0])
				text = document.createTextNode(str[1]);
				
				elt.value = value;
				elt.appendChild(text);
				div.appendChild(elt);
			}
		}
	
		displayGames();
	}
}

function callback_games(){
	if(xhr.readyState == 4 && xhr.status == 200){
		txt = xhr.responseText;
		txt = txt.split(";");
		
		div = document.getElementById("game_id");

		while(div.childNodes.length != 0){
			div.removeChild(div.childNodes[0]);
		}
		
		elt = document.createElement("option");
		elt.disabled = true;

		if(txt[0] == ""){
			elt.selected = true;
			elt.appendChild(document.createTextNode("No option available"));
		} else{
			elt.selected = false;			
			elt.appendChild(document.createTextNode("Select an option"));
		}
		div.appendChild(elt);
		
		for(t in txt){
			if(txt[t] != ""){
				str = txt[t].split("/=/");
				elt = document.createElement("option");
				value = parseInt(str[0])
				text = document.createTextNode(str[1]);
				
				elt.value = value;
				elt.appendChild(text);
				div.appendChild(elt);
			}
		}
	}
}

function displayGames(){
	clearContent(document.getElementById("game_id"));
	
	e = document.getElementById("platform_id");
	value1 = e.options[e.selectedIndex].value;
	e = document.getElementById("region_id");
	value2 = e.options[e.selectedIndex].value;

  	xhr = new XMLHttpRequest();
	xhr.onreadystatechange = callback_games;
	xhr.open("GET","/views/item/scripts/select_games.php?plat="+value1+"&reg="+value2,true);
	xhr.send(null)
}

function displayRegions (){
	clearContent(document.getElementById("region_id"));
	
	e = document.getElementById("platform_id");
	value = e.options[e.selectedIndex].value;

  	xhr = new XMLHttpRequest();
	xhr.onreadystatechange = callback_region;
	xhr.open("GET","/views/item/scripts/select_regions.php?plat="+value,true);
	xhr.send(null)
}
 
function clearContent (element){
	while (element.childNodes.length != 0)
		element.removeChild(element.childNodes[0]);
}
    

        