<?php

    class NotificationListTable extends View {
		private $notifications;

		function __construct($notifications) {
			$this->notifications = $notifications;
		}

		function render() {
?>
			<form action='/controllers/notification/NotificationDeleteAction.php' method="post">
				<table class="table" id="notifications_table">
					<tr id='cache'>
						<th>ID</th>
						<th><?= $GLOBALS['locale']['content'] ?></th>
						<th>Date</th>
						<th>Action</th>
					</tr>
<?php
					foreach ($this->notifications as $notification) {
						$id = $notification->getId();
						$content = $notification->getContent();
						$date = $notification->getDate();
?>
						<tr>
							<td><?= $id ?></td>
							<td><?= $content ?></td>
							<td><?= $date ?></td>
							<td>
								<button class="btn btn-danger" type="submit" name='id' value="<?= $id ?>" ><?= $GLOBALS["locale"]["delete"] ?></button>
							</td>
						</tr>
<?php
					}
?>
            	</table>
        	</form>
<?php
        }
    }
?>
