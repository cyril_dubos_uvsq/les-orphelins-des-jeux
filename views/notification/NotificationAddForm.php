<?php
	class NotificationAddForm extends View {
		function render() {
?>
			<form action='/controllers/notification/NotificationAddAction.php' method='post'>
                <div class='form-group'>
                    <label for='content_notification'><?= $GLOBALS['locale']['name'] ?></label> 
                    <textarea name='content_notification' class='form-control' rows='3'></textarea>
                </div>

                <input class='btn btn-primary' type='submit' value='<?= $GLOBALS['locale']['add'] ?>' />
            </form>
<?php
		}
	}
?>
