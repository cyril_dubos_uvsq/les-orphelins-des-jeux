<?php
    class BreadCard extends View {
        private $items;
        private $element;

        function __construct($items, $element) {
            $this->items = $items;
            $this->element = $element;
        }

        function render() {
            echo("
                <div class='card m-5'>
                    <ol class=' m-0 card-header breadcrumb'>
            ");

            $items = array_keys($this->items);

            $last_title = end($items);

            foreach ($this->items as $title => $link) {
                if ($title != $last_title) {
                    echo("<li class='breadcrumb-item'><a href='$link'>$title</a></li>");
                } else {
                    echo("<li class='breadcrumb-item active'>$title </li>");
                }
            }   
            
            echo("
                    </ol>
                    <div class='card-body'>
            ");
                        $this->element->render();
            echo("
                    </div>
                </div>
            ");
        }
    }
?>