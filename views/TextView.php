<?php
    class TextView extends View {
        private $text;

        function __construct($text) {
            $this->text = $text;
        }

        function render() {
            echo($this->text);
        }
    }
?>