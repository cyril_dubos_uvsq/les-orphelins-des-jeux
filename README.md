# Les Orphelins des Jeux #

## Générale

- Retirer les IDs
- Ajout de platforme, régions, jeux
- Gestion des pages et erreur
- Site en HTTPS

## Page d'accueil :

- Bandeaux déroulants d'objets (a finir)
- Description du site / projet / équipe (a faire)

## Page Base de données

- Voir une platforme : OK
- Ajouter une platforme : OK
- Modifier une platforme : OK
- Supprimer une platforme : OK

## Page Platforme

- Voir une région : OK
- Ajouter une région : OK
- Modifier une région : OK
- Supprimer une région : OK

## Page Region

- Voir un jeu : A faire
- Ajouter un jeu : A revoir
- Modifier un jeu : OK
- Supprimer un jeu : OK

## Page Jeux : A faire

## Page Administrateur : Utilisateurs

- Recherche : A revoir
- Table des utilisateurs : A revoir
- Modification d'un utilisateur : OK

## Page Administrateur : Objets

- Recherche : A revoir
- Table des objets : A revoir
- Bloquer / débloquer : A faire

## Page de match (a faire)

## Page de connexion

- Mot de passe oublie

## Page d'inscription

- Vérification par email
- Condition d'utilisation (RGPD)
- Notification (potentiel)