<?php
    $config = array(
        "database_host" => "localhost",
        "database_username" => "root",
        "database_password" => "",
        "database_name" => "retrogaming",

        "item_photo_width" => 720,
        "item_photo_height" => 576,

        "user_avatar_width" => 128,
        "user_avatar_height" => 128 
    )
?>
