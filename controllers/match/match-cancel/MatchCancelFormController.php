<?php
    class MatchCancelFormController extends Controller {
        private $matchCancelForm;

        function __construct($match_id,$table) {
            $this->matchCancelForm = new MatchCancelForm($match_id,$table);
        }

        function render() {
            $this->matchCancelForm->render();
        }
    }
?>