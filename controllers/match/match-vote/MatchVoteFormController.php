<?php
    class MatchVoteFormController extends Controller {
        private $matchVoteForm;

        function __construct($match_id,$table) {
            $this->matchVoteForm = new MatchVoteForm($match_id,$table);
        }

        function render() {
            $this->matchVoteForm->render();
        }
    }
?>