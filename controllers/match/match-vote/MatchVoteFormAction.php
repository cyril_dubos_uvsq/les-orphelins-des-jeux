<?php
    if (!isset($_POST["id"])) {
        header("Location: /index.php?error=language_undefined");
    } else {
        
        $values = explode("!",$_POST["id"]);
        $id = $values[0];
        $table = $values[1];
        $rate = $values[2];
        $note = ($rate == "pos") ? 1 : -1;

        $root = realpath($_SERVER["DOCUMENT_ROOT"]);

        require("$root/models/Database.php");
        require("$root/models/Match.php");

        $match = getMatchById($id);
        echo($id);
        if($table == "searched_items")
            $match_accept = new Match($match->getId(),$match->getOwnedItemId(),$match->getSearchedItemId(),$match->getOwnerState(),$match->getSearcherState(),$match->getOwnerRating(),$note,$match->getCreatedAt(),$match->getStatus());
        else 
            $match_accept = new Match($match->getId(),$match->getOwnedItemId(),$match->getSearchedItemId(),$match->getOwnerState(),$match->getSearcherState(),$note,$match->getSearcherRating(),$match->getCreatedAt(),$match->getStatus());

        if (!$db_result = $match_accept->edit()) {
            header("Location: /index.php?controller=matchs&id=$id&error=match_vote");
        } else {
            if($match_accept->getOwnerRating() != 0 && $match_accept->getSearcherRating() != 0){
                $match_accept->confirm();
            }
            header("Location: /index.php?controller=matchs");
        }
    }
?>
