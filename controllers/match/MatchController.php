<?php
    class MatchController extends Controller {
        function render() { 
            $session_user_id = $_SESSION["user_id"];
            $session_user = getUserById($session_user_id);

            if (!$session_user) {
                header("Location: index.php?error=unauthorized_action");
            }

            echo("<h1 class='m-5'>Mes matchs</h1>");
            if (isset($_GET["error"])) {
                switch (isset($_GET["error"])) {
                    case("match_add_args"):
                        echo("
                            <div class='alert alert-danger m-5' role='alert'>
                                Error: invalid arguments for insert (item_add_args)
                            </div>
                        ");
                        break;
                }
            }

            if (!$this->action) {
                $user_id = $_SESSION["user_id"];
                $user = getUserbyId($user_id);
                $username = $user->getUsername();

                $this->list();
            } else {
                switch($this->action){
                    case("infos"):
                        if(isset($_GET["table"]) && $this->id){
                            $this->table = $_GET["table"];
                            $this->infos();
                        }
                        break;
                        
                    case("accept"):
                        if(isset($_GET["table"]) && $this->id){
                            $this->table = $_GET["table"];
                            $this->accept();
                        }
                        break;
                        
                    case("cancel"):
                        if(isset($_GET["table"]) && $this->id){
                            $this->table = $_GET["table"];
                            $this->cancel();
                        }
                        break;
                    
                    case("vote"):
                        if(isset($_GET["table"]) && $this->id){
                            $this->table = $_GET["table"];
                            $this->vote();
                        }
                        break;
                        
                    case("refus"):
                        if(isset($_GET["table"]) && $this->id){
                            $this->table = $_GET["table"];
                            $this->refus();
                        }
                        break;
                }
            }
        }
        
        function refus() {
            $matchRefusFormController = new MatchRefusFormController($this->id,$this->table);
    
            $card = new Card("Refuse match", $matchRefusFormController);
            $card->render();
        }
        
        function cancel() {
            $matchCancelFormController = new MatchCancelFormController($this->id,$this->table);
    
            $card = new Card("Cancel     match", $matchCancelFormController);
            $card->render();
        }
        
        function accept() {
                $matchAcceptFormController = new MatchAcceptFormController($this->id,$this->table);
        
                $card = new Card("Accept match", $matchAcceptFormController);
                $card->render();
        }
        
        function vote() {
                $matchVoteFormController = new MatchVoteFormController($this->id,$this->table);
        
                $card = new Card("Vote match", $matchVoteFormController);
                $card->render();
        }

        function list() {
            $matchsSrc = getMatchsByUserId("searched",$_SESSION["user_id"], true);
            $matchsOwn = getMatchsByUserId("owned",$_SESSION["user_id"], true);
            
            $matchListTableSrc = new MatchListTable("searched_items",$matchsSrc);
            $matchListTableOwn = new MatchListTable("owned_items",$matchsOwn);
            
            $cardSrc = new Card("Liste des match(s) des objets recherchés", $matchListTableSrc);
            $cardSrc->render();
            
            $cardOwn = new Card("Liste des match(s) des objets possédés", $matchListTableOwn);
            $cardOwn->render();
        }

        function infos() {
            if (isset($_SESSION["user_id"])) {
                $user_id = $_SESSION["user_id"];
                
                $matchInfosTableController = new MatchInfosTableController($this->table,$this->id);
        
                $card = new Card("Matched item", $matchInfosTableController);
                $card->render();
            }
        }
    }
?>