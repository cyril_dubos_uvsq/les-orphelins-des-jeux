<?php
    if (!isset($_POST["id"])) {
        header("Location: /index.php?error=language_undefined");
    } else {
        
        $values = explode("!",$_POST["id"]);
        $id = $values[0];
        $table = $values[1];

        $root = realpath($_SERVER["DOCUMENT_ROOT"]);

        require("$root/models/Database.php");
        require("$root/models/Match.php");

        $match = getMatchById($id);

        if (!$db_result = $match->delete()) {
            header("Location: /index.php?controller=matchs&id=$id&error=match_vote");
        } else {
            header("Location: /index.php?controller=matchs");
        }
    }
?>
