<?php
    class MatchRefusFormController extends Controller {
        private $matchRefusForm;

        function __construct($match_id,$table) {
            $this->matchRefusForm = new MatchRefusForm($match_id,$table);
        }

        function render() {
            $this->matchRefusForm->render();
        }
    }
?>