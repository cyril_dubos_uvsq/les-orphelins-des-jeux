<?php
    class MatchAcceptFormController extends Controller {
        private $matchAcceptForm;

        function __construct($match_id,$table) {
            $this->matchAcceptForm = new MatchAcceptForm($match_id,$table);
        }

        function render() {
            $this->matchAcceptForm->render();
        }
    }
?>