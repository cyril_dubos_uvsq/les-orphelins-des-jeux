<?php
    class MatchInfosTableController extends Controller {
        private $matchInfosForm;

        function __construct($table,$match_id) {
            $match = getMatchById($match_id);

            if($table == "owned_items"){
                $item = getItemById($table,$match->getOwnedItemId());
            }
            else if($table == "searched_items"){
                $item = getItemById($table,$match->getSearchedItemId());
            }

            $this->matchInfosForm = new MatchInfosTable($table,$match,$item);
        }

        function render() {
            $this->matchInfosForm->render();
        }
    }
?>