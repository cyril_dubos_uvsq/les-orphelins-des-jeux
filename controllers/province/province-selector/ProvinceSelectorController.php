<?php
    class ProvinceSelectorController extends Controller {
        private $provinceSelector;

        function __construct($province_id = null) {
                $province = getProvinceById($province_id);

                if ($province) {
                    $country_id = $province->getCountryId();
                    $provinces = getProvinces($country_id);

                    $countrySelectorController = new CountrySelectorController($country_id);
                    $this->provinceSelector = new ProvinceSelector($countrySelectorController, $provinces, $province_id);
                } else {

                    $countrySelectorController = new CountrySelectorController(null);
                    $this->provinceSelector = new ProvinceSelector($countrySelectorController, null, $province_id);
                }            
        }

        function render() {
            $this->provinceSelector->render();
        }
    }
?>