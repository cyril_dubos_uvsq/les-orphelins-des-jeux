<?php
    if (isset($_GET["country_id"])) {
        $country_id = $_GET["country_id"];

        $root = realpath($_SERVER["DOCUMENT_ROOT"]);

        require("$root/models/Database.php");
        require("$root/models/Province.php");

        echo("<option disabled selected>Select an option</option>");

        $provinces = getProvincesByCountryId($country_id);

        foreach ($provinces as $province) {
            $id = $province->getId();
            $name = $province->getProvince();
            
            echo("<option value='$id'>$name</option>");
        }
    }
?>