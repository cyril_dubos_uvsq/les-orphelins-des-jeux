function $_GET(param) {
	var vars = {};
	window.location.href.replace( location.hash, '' ).replace(
		/[?&]+([^=&]+)=?([^&]*)?/gi, // regexp
		function( m, key, value ) { // callback
			vars[key] = value !== undefined ? value : '';
		}
	);
	if ( param ) {
		return vars[param] ? vars[param] : null;
	}
	return vars;
}



var user = $_GET('user');



var header = document.getElementById("header");

$(document).ready(function () {

	//insertion du user en question
	if(user !== null) {
		search_table(user);
  }


	$('#search').keyup(function () {
		search_table($(this).val());
	});

	function search_table(value) {
		$('#items_table tr').each(function () {
			var found = 'false';

			$(this).each(function () {
				if ($(this).text().toLowerCase().indexOf(value.toLowerCase()) >= 0) {
					found = 'true';
				}
			});

			if (found == 'true') {
				$(this).show();
			} else {
				$(this).hide();
			}
		});

		header.setAttribute("style", "display: table-row");
	}
});
