<?php
	class AdminController extends Controller {
		function render() {
			$user = getUserById($this->id);

            $session_user_id = $_SESSION["user_id"];
            $session_user = getUserById($session_user_id);

            if (!$session_user || !$session_user->isAdministrator()) {
                header("Location: index.php?error=unauthorized_action");
            }

			if ($this->action) {
				switch ($this->action) {
					case "users":
						$this->users();
					break;

					case "items":
						$this->items();
					break;

					default:
						header("Location: index.php?error=invalid_action");
						break;
				}
			} else {
				header("Location: index.php?error=undefined_action");
			}
		}

		function users() {
			$users = getUsers();
			$tableAdmin = new AdminUsersTable($users);
			$card = new Card("Administration -  {$GLOBALS["locale"]["users"]}", $tableAdmin);
			$card->render();
		}

		function items() {
			$item_owned = getItemsOwned();
			$item_searched = getItemsSearched();
			$tableObjects = new AdminItemsTable($item_searched,$item_owned);
			$card = new Card("Administration -  {$GLOBALS["locale"]["items"]}", $tableObjects);
			$card->render();
		}
	}
?>
