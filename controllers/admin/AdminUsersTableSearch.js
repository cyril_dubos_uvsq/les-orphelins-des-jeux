




var x = document.getElementById("cache");


var header = document.getElementById("header");

$(document).ready(function () {

	$('#search').keyup(function () {
		search_table($(this).val());
	});

	function search_table(value) {
		$('#users_table tr').each(function () {
			var found = 'false';

			$(this).each(function () {
				if ($(this).text().toLowerCase().indexOf(value.toLowerCase()) >= 0) {
					found = 'true';
				}
			});

			if (found == 'true') {
				$(this).show();
			} else {
				$(this).hide();
			}
		});

		header.setAttribute("style", "display: table-row");
	}
});
