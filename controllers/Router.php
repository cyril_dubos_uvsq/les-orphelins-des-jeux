
<?php
    class Router {
        private $controller;
        private $action;
        private $id;

        function __construct($controller, $action, $id) {
            $this->controller = $controller;
            $this->action = $action;
            $this->id = $id;
        }

        function render() {
            $controller = $this->controller;
            $id = $this->id;
            $action = $this->action;

            if ($controller) {
                switch($controller) {
                    case "sign":
                        $signController = new SignController($action, $id);
                        $signController->render();
                        break;

                    case "user":
                        $userController = new UserController($action, $id);
                        $userController->render();
                        break;

                    case "database":
                        $databaseController = new DatabaseController($action, $id);
                        $databaseController->render();
                        break;

                    case "items":
                        $itemController = new ItemController($action, $id);
                        $itemController->render();
                        break;

                    case "matchs":
                        $matchController = new MatchController($action, $id);
                        $matchController->render();
                        break;

                     case "admin":
                        $adminController = new AdminController($action, $id);
                        $adminController->render();
                        break;

                    case "platform":
                        $platformController = new PlatformController($action, $id);
                        $platformController->render();
                        break;

                    case "region":
                        $regionController = new RegionController($action, $id);
                        $regionController->render();
                        break;

                    case "game":
                        $gameController = new GameController($action, $id);
                        $gameController->render();
                        break;

                    case "admin":
                        $adminController = new AdminController($action, $id);
                        $adminController->render();
                        break;

                    case "tabitems":
                        $tabObjectsController = new TabItemsController($id, $action);
                        $tabObjectsController->render();
                        break;

                    case "notification":
                        $notifications = new NotificationController($id, $action);
                        $notifications->render();
                        break;

                    default:
                        header("Location: index.php?error=invalid_controller");
                        break;
                }
            } else {
                $homeController = new HomeController($action, $id);
                $homeController->render();
            }
        }
    }
?>
