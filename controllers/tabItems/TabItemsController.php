<?php

class TabItemsController extends Controller {

    function render() {

        if ($this->action) {
            switch ($this->action) {
                default:
                    echo("
                        <div class='alert alert-danger m-5' role='alert'>
                            Error: invalid action
                        </div>
                    ");
                    break;
            }
        } else {
            $this->displaytable();
        }

    }

    function displaytable() {
      $item_owned = getItemsOwned();
      $item_searched = getItemsSearched();
      $tableObjects = new TableItemsDisplayForm($item_searched,$item_owned);
      $card = new Card("Table Items", $tableObjects);
      $card->render();
    }

  }



 ?>
