<?php
  $root = realpath($_SERVER["DOCUMENT_ROOT"]);
  require("$root/models/Database.php");
  require("$root/models/User.php");
  require("$root/models/Item.php");

  if (isset($_POST["Bloquer"])) {
    $id_item = $_POST["Bloquer"];
    $lock = 0;
  }
  else if(isset($_POST["Debloquer"])) {
    $id_item = $_POST["Debloquer"];
    $lock = 1;
  }
  else {
    header("Location: /index.php?controller=tabitems&error=id");
  }

$item = getItemById("owned_items",$id_item);
$id_user = $item->getUserId();
$user = getUserById($id_user);
if ($user->getStatus()) {
  lockAnItemByUserId($id_item,$id_user,$lock);
    header("Location: /index.php?controller=tabitems");
}
else {
  header("Location: /index.php?controller=tabitems&error=userlock");
}
?>
