<?php
    class CountrySelectorController extends Controller {
        private $countrySelector;

        function __construct($country_id = null) {
            $countries = getCountries();

            $this->countrySelector = new CountrySelector($countries, $country_id);
        }

        function render() {
            $this->countrySelector->render();
        }
    }
?>