window.addEventListener("load", initialization_countrySelectorUpdate, false);

function initialization_countrySelectorUpdate() {
    country_select = document.getElementById("country_select");
	country_select.addEventListener("change", countrySelectorUpdate, false);
}

function countrySelectorUpdate() {
	clearContent(document.getElementById("province_select"));
	
    country_select = document.getElementById("country_select");
    selected_index = country_select.selectedIndex;
    selected_option = country_select.options[selected_index];
    country_id = selected_option.value;

  	xhr = new XMLHttpRequest();
	xhr.onreadystatechange = callback_countrySelectorUpdate;
	xhr.open("GET","/controllers/province/province-selector/ProvinceSelectorOptions.php?country_id=" + country_id, true);
	xhr.send(null)
}

function callback_countrySelectorUpdate(){
	if(xhr.readyState == 4 && xhr.status == 200){
		response_text = xhr.responseText;
		
		province_select = document.getElementById("province_select");
		province_select.innerHTML = response_text;
	}
}

function clearContent(element){
	while (element.childNodes.length != 0)
		element.removeChild(element.childNodes[0]);
}