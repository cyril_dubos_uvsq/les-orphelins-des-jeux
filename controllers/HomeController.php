
<?php
    class HomeController extends Controller {
        function render() {
            /*
            echo("<h1 class='m-5'>");
            echo($GLOBALS["locale"]["home"]);
            echo("</h1>");
            */

            if (!$this->id) {
                if (!$this->action) {
                    /*
                    if (isset($_SESSION["user_id"])) {
                        $user_id = $_SESSION["user_id"];
                        $user = getUserbyId($user_id);
                        $username = $user->getUsername();

                        echo("<div class='alert alert-success m-5' role='alert'>");
                        echo($GLOBALS["locale"]["hello"] . " " . $username . ". " . $GLOBALS["locale"]["welcome_message"]);
                        echo("</div>");
                    } else {
                        echo("<div class='alert alert-success m-5' role='alert'>");
                        echo($GLOBALS["locale"]["welcome_message"]);
                        echo("</div>");
                    }
                    */
?>
                    <div class="d-flex justify-content-center m-5">
                            <div class="w-50">
<?php
                                /*
                                $items = getLastItemsOwned(-1);

                                echo("<pre>");
                                print_r($items);
                                echo("</pre>");

                                $slides = [];
                                $captions = [];

                                foreach ($items as $item) {
                                    $item_id = $item->getId();
                                    $game_id = $item->getGameId();
                                    $game = getGameById($game_id);
                                    $game_title = $game->getTitle();

                                    $type_id = $item->getTypeId();
                                    switch($type_id){
                                        case(1):
                                            $type = "Notice";
                                            break;
                                        case(2):
                                            $type = "Boite";
                                            break;
                                        case(3):
                                            $type = "Cartouche";
                                            break;
                                      }

                                    $slides[$item_id] = "https://images.unsplash.com/photo-1584354012731-ba34cef5963f";
                                    $captions[$game_title] = $type;
                                }
                                */
                                
                                $slides = [
                                    "1" => "https://rdsimages.cookieless.ca/polopoly_fs/1.6053509!/img/httpImage/image.jpg",
                                    "2" => "https://www.link-tothepast.com/wp-content/uploads/2009/08/znes1.jpg",
                                    "3" => "https://i.ebayimg.com/images/g/rpIAAOSwfjZc94Gz/s-l1600.jpg"
                                ];
                
                                $captions = [
                                    "Bonk's Adventure" => "Cartouche",
                                    "The Legend of Zelda" => "Boite",
                                    "Super Mario 64" => "Notice"
                                ];

                                $carousel = new Carousel($slides, $captions);
                                $carousel->render();
?>
                            </div>
                        </div>
<?php 
                $presentationTitle = "Présetation du site";
                $presentationText = "
                    <p>Bienvenue sur Les Orphelins Des Jeux !</p>
                    
                    <p>Ce site a pour objectif de faciliter les recherches et échanges de pièces orphelines de jeux vidéo (notices, boîtes, ...) entre collectionneurs.
                    Il ne favorise pas les ventes entre particuliers, d'ailleurs aucune indication de montants n'est accepté sur le site. Il permet simplement les mises en contact.
                    Afin d'empêcher le commerce de jeux complets, les pièces et recherches doivent être ajoutée une par une.</p>
                    
                    <p>Renseignez les orphelins que vous possédez, renseignez également les éventuelles recherches de pièces dont vous avez besoin, et des \"matches\" vous seront proposés si vos orphelins sont recherchés, ou si vos recherches aboutissent ! Libre à vous par la suite de contacter par email la personne qui recherche ou propose un item.</p>
                    
                    <p>Tout abus sera sanctionné par une désactivation des utilisateurs. Ce site a demandé du temps pour être développé : prenez-en soin !</p>
                ";

                $presentationView = new TextView($presentationText);
                $presentationCard = new Card($presentationTitle, $presentationView);
                $presentationCard->render();
?>
<?php
                } else {
                    header("Location: /index.php?error=invalid_action");
                }
            } else {
                header("Location: /index.php?error=invalid_id");
            }
        }
    }
?>
