<?php
    class DatabaseController extends Controller {
        function render() {
            echo("<h1 class='m-5'>");
            echo($GLOBALS["locale"]["database"]);
            echo("</h1>");

            if (!$this->id) {
                if (!$this->action) {
                    $this->description();
                    $this->add();
                    $this->list();
                } else {
                    header("Location: /index.php?error=invalid_action");
                }
            } else {
                header("Location: /index.php?error=invalid_id");
            }
        }

        function description() {
            $items = array(
                $GLOBALS["locale"]["database"] => "/index.php?controller=database"
            );

            $textView = new TextView($GLOBALS["locale"]["database_message"]);
            
            $card = new BreadCard($items, $textView);
            $card->render();
        }
        
        function add() {
            if (isset($_SESSION["user_id"])) {
                $user_id = $_SESSION["user_id"];
                $user = getUserById($user_id);

                if ($user->isAdministrator()) {
                    $platformAddForm = new PlatformAddForm();

                    $card = new Card($GLOBALS["locale"]["platform_add"], $platformAddForm);
                    $card->render();
                }
            }
        }

        function list() {
            $platforms = getPlatforms();
            
            $platformListTable = new PlatformListTable($platforms);
            
            $card = new Card($GLOBALS["locale"]["platform_list"], $platformListTable);
            $card->render();
        }
    }
?>