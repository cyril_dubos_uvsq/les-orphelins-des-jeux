<?php
    if (isset($_POST["id"])) {
        $root = realpath($_SERVER["DOCUMENT_ROOT"]);

        require("$root/models/Database.php");
        require("$root/models/Region.php");

        $id = $_POST["id"];

        $region = getRegionById($id);

        $platform_id = $region->getPlatformId();

        if (!$db_result = $region->delete()) {
            header("Location: /index.php?controller=platform&id=$platform_id&error=region_delete_db");
        } else {
            header("Location: /index.php?controller=platform&id=$platform_id");
        }
    } else {
        header("Location: /index.php?controller=platform&id=$platform_id&error=region_delete_args");
    }
?>
