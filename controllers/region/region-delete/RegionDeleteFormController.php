<?php
    class RegionDeleteFormController extends Controller {
        private $regionDeleteForm;

        function __construct($region_id) {
            $region = getRegionById($region_id);

            $this->regionDeleteForm = new RegionDeleteForm($region);
        }

        function render() {
            $this->regionDeleteForm->render();
        }
    }
?>