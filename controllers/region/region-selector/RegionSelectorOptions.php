<?php
    if (isset($_GET["platform_id"])) {
        $platform_id = $_GET["platform_id"];

        $root = realpath($_SERVER["DOCUMENT_ROOT"]);

        require("$root/models/Database.php");
        require("$root/models/Region.php");

        echo("<option disabled selected>Select an option</option>");

        $regions = getRegionsByPlatformId($platform_id);

        foreach ($regions as $region) {
            $id = $region->getId();
            $name = $region->getName();
            
            echo("<option value='$id'>$name</option>");
        }
    }
?>