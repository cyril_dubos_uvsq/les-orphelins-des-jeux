<?php
    class RegionSelectorController extends Controller {
        private $regionSelector;

        function __construct($region_id = null) {
            $plaform_id = null;
            $regions = null;

            if($region_id) {
                $region = getRegionById($region_id);
                if ($region) {
                    $plaform_id = $region->getPlatformId();
                    $regions = getRegionsByPlatformId($plaform_id);
                }
            }



            $platformSelectorController = new PlatformSelectorController($plaform_id);

            $this->regionSelector = new RegionSelector($platformSelectorController, $regions, $region_id);
        }

        function render() {
            $this->regionSelector->render();
        }
    }
?>