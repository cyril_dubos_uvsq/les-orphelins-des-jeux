window.addEventListener("load", initialization_regionSelectorUpdate, false);

function initialization_regionSelectorUpdate() {
    region_select = document.getElementById("region_select");
	region_select.addEventListener("change", regionSelectorUpdate, false);
}

function regionSelectorUpdate() {
	clearContent(document.getElementById("game_select"));
	
    region_select = document.getElementById("region_select");
    selected_index = region_select.selectedIndex;
    selected_option = region_select.options[selected_index];
    region_id = selected_option.value;

  	xhr = new XMLHttpRequest();
	xhr.onreadystatechange = callback_regionSelectorUpdate;
	xhr.open("GET","/controllers/game/game-selector/GameSelectorOptions.php?region_id=" + region_id, true);
	xhr.send(null)
}

function callback_regionSelectorUpdate(){
	if(xhr.readyState == 4 && xhr.status == 200){
		response_text = xhr.responseText;
		
		game_select = document.getElementById("game_select");
		game_select.innerHTML = response_text;
	}
}

function clearContent(element){
	while (element.childNodes.length != 0)
		element.removeChild(element.childNodes[0]);
}