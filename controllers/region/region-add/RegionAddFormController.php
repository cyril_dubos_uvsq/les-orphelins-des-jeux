<?php
    class RegionAddFormController extends Controller {
        private $regionAddForm;

        function __construct($platform_id = null) {
            $platformSelectorController = new PlatformSelectorController($platform_id);

            $this->regionAddForm = new RegionAddForm($platformSelectorController);
        }

        function render() {
            $this->regionAddForm->render();
        }
    }
?>