<?php
    if (isset($_POST["name"]) && isset($_POST["platform_id"]) && isset($_POST["description"])) {
        $root = realpath($_SERVER["DOCUMENT_ROOT"]);

        require("$root/models/Database.php");
        require("$root/models/Region.php");

        $name = $_POST["name"];
        $platform_id = $_POST["platform_id"];
        $description = $_POST["description"];

        $region = new Region(null, $name, $platform_id, $description);

        if (!$db_result = $region->add()) {
            header("Location: /index.php?controller=platform&id=$platform_id&error=region_add_db");
        } else {
            header("Location: /index.php?controller=platform&id=$platform_id");
        }
    } else {
        header("Location: /index.php?controller=platform&id=$platform_id&error=region_add_args");
    }
?>