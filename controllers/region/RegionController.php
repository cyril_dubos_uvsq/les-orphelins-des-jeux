<?php
    class RegionController extends Controller {
        function render() {
            echo("<h1 class='m-5'>");
            echo($GLOBALS["locale"]["region"]);
            echo("</h1>");

            if ($this->id) {
                $region = getRegionById($this->id);
                
                if ($region) {
                    $region_name = $region->getName();

                    $platform_id = $region->getPlatformId();
                    $platform = getPlatformById($platform_id);
                    $platform_name = $platform->getName();

                    //echo("<h2 class='text-muted m-5'><small>$platform_name - $region_name</small></h2>");

                    if ($this->action) {
                        switch ($this->action) {                       
                            case "edit":
                                $this->edit();
                                break;

                            case "delete":
                                $this->delete();
                                break;

                            default:
                                header("Location: /index.php?error=invalid_action");
                                break;
                        }
                    } else {
                        $this->description();
                        $this->add();
                        $this->list();
                    }
                } else {
                    header("Location: /index.php?error=invalid_id");
                }
            } else {
                header("Location: /index.php?error=undefined_id");
            }

            
        }

        function description() {
            $region = getRegionById($this->id);
            $region_name = $region->getName();
            $region_description = $region->getDescription();

            $platform_id = $region->getPlatformId();
            $platform = getPlatformById($platform_id);
            $platform_name = $platform->getName();

            $items = array(
                $GLOBALS["locale"]["database"] => "/index.php?controller=database",
                "$platform_name" => "/index.php?controller=platform&id=$platform_id",
                "$region_name" => "/index.php?controller=region&id=$this->id",
            );

            $textView = new TextView($region_description);
            
            $card = new BreadCard($items, $textView);
            $card->render();
        }

        function add() {
            if (isset($_SESSION["user_id"])) {
                $user_id = $_SESSION["user_id"];
                $user = getUserById($user_id);

                if ($user->isAdministrator()) {
                    $gameAddFormController = new GameAddFormController($this->id);
            
                    $card = new Card($GLOBALS["locale"]["game_add"], $gameAddFormController);
                    $card->render();
                }
            }
        }

        function list() {
            $games = getGamesByRegionId($this->id);
            
            $gameListTable = new GameListTable($games);
            
            $card = new Card($GLOBALS["locale"]["game_list"], $gameListTable);
            $card->render();
        }

        function edit() {
            if (isset($_SESSION["user_id"])) {
                $user_id = $_SESSION["user_id"];
                $user = getUserById($user_id);

                if ($user->isAdministrator()) {
                    $regionEditFormController = new RegionEditFormController($this->id);
            
                    $card = new Card($GLOBALS["locale"]["region_edit"], $regionEditFormController);
                    $card->render();
                } else {
                    header("Location: /index.php?error=unauthorized_action");
                }
            }
        }

        function delete() {
            if (isset($_SESSION["user_id"])) {
                $user_id = $_SESSION["user_id"];
                $user = getUserById($user_id);

                if ($user->isAdministrator()) {
                    $regionDeleteFormController = new RegionDeleteFormController($this->id);
            
                    $card = new Card($GLOBALS["locale"]["region_delete"], $regionDeleteFormController);
                    $card->render();
                } else {
                    header("Location: /index.php?error=unauthorized_action");
                }
            }
        }
    }
?>