<?php
    if (isset($_POST["id"]) && isset($_POST["name"]) && isset($_POST["platform_id"]) && isset($_POST["description"])) {
        $root = realpath($_SERVER["DOCUMENT_ROOT"]);

        require("$root/models/Database.php");
        require("$root/models/Region.php");

        $id = $_POST["id"];
        $name = $_POST["name"];
        $platform_id = $_POST["platform_id"];
        $description = $_POST["description"];

        $region = new Region($id, $name, $platform_id, $description);

        if (!$db_result = $region->edit()) {
            header("Location: /index.php?controller=platform&id=$platform_id&error=region_edit_db");
        } else {
            header("Location: /index.php?controller=platform&id=$platform_id");
        }
    } else {
        header("Location: /index.php?controller=platform&id=$platform_id&error=region_edit_args");
    }
?>

