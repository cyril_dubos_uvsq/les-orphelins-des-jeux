<?php
    class RegionEditFormController extends Controller {
        private $regionEditForm;

        function __construct($region_id) {
            $region = getRegionById($region_id);
            $platform_id = $region->getPlatformId();

            $platformSelectorController = new PlatformSelectorController($platform_id);

            $this->regionEditForm = new RegionEditForm($platformSelectorController, $region);
        }

        function render() {
            $this->regionEditForm->render();
        }
    }
?>