<?php
    class UserLockFormController extends Controller {
        private $userLockForm;

        function __construct($user_id) {
            $user = getUserById($user_id);
            $user_status = $user->getStatus();

            $action = $user_status == 0 ? "unlock" : "lock";

            $this->userLockForm = new UserLockForm($user_id, $action);
        }

        function render() {
            $this->userLockForm->render();
        }
    }
?>