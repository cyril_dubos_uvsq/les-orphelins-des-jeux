<?php
    $root = realpath($_SERVER["DOCUMENT_ROOT"]);

    require("$root/models/Database.php");
    require("$root/models/User.php");
    require("$root/models/Item.php");


    $id = $_POST["id"];
    $user = getUserById($id);

    $user_status = $user->lock();

    lockAllItemsByUserId($id, $user_status);


    session_start();
    $session_user_id = $_SESSION["user_id"];

    if($session_user_id == $id) {
        unset($_SESSION);
        session_destroy();
        header("Location: /index.php");
    } else {
        header("Location: /index.php?controller=admin&action=users");
    }
?>
