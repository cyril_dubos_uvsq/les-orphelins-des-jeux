<?php
    class UserController extends Controller {
        function __construct($action, $id) {
            $this->action = $action;

            if ($id) {
                $this->id = $id;

                if (!$user = getUserById($id)) {
                    header("Location: index.php?error=user_not_found");
                }
            } else if ($_SESSION["user_id"]) {
                $this->id = $_SESSION["user_id"];
            } else {
                header("Location: index.php?error=not_connected");
            }
        }
        
        function render() {
            if ($this->action) {
                switch ($this->action) {
                    case "profile":
                        $this->profile();
                        break;
                    
                    case "edit":
                        $this->edit();
                        break;

                    case "lock":
                        $this->lock();
                        break;

                    default:
                        header("Location: index.php?error=invalid_action");
                        break;
                }
            } else {
                header("Location: index.php?error=undefined_action");
            }
        }

        function profile() {
            $user = getUserById($this->id);

            $session_user_id = $_SESSION["user_id"];
            $session_user = getUserById($session_user_id);

            if ($session_user->isAdministrator() || $this->id == $session_user_id) {
                $userProfileController = new UserProfileController($this->id);
                $userProfileController->render();
            } else {
                header("Location: index.php?error=unauthorized_action");
            }
        }

        function edit() {
            $user = getUserById($this->id);

            $session_user_id = $_SESSION["user_id"];
            $session_user = getUserById($session_user_id);

            if ($session_user->isAdministrator() || $this->id == $session_user_id) {
                $userEditFormController = new UserEditFormController($this->id);
                $card = new Card("Edit user", $userEditFormController);

                $card->render();
            } else {
                header("Location: index.php?error=unauthorized_action");
            }
        }

        function lock() {
            $user = getUserById($this->id);

            $session_user_id = $_SESSION["user_id"];
            $session_user = getUserById($session_user_id);

            if ($session_user->isAdministrator() || $this->id == $session_user_id) {
                $userLockFormController = new UserLockFormController($this->id);
                $card = new Card("Lock user", $userLockFormController);

                $card->render();
            } else {
                header("Location: index.php?error=unauthorized_action");
            }
        }
    }
?>
