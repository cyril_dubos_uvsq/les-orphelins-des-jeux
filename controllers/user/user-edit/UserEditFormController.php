<?php
    class UserEditFormController extends Controller {
        private $userEditForm;

        function __construct($user_id) {
            $user = getUserById($user_id);
            $province_id = $user->getProvinceId();

            $provinceSelectorController = new ProvinceSelectorController($province_id);
            $this->userEditForm = new UserEditForm($provinceSelectorController, $user);
        }

        function render() {
            $this->userEditForm->render();
        }
    }
?>