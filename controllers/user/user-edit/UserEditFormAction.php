<?php
    $root = realpath($_SERVER["DOCUMENT_ROOT"]);

    require("$root/models/Database.php");
    require("$root/models/User.php");

    $id = $_POST['id'];
    $username = htmlspecialchars($_POST['username']);
    $firstname = htmlspecialchars($_POST['firstname']);
    $lastname = htmlspecialchars($_POST['lastname']);
    $email = htmlspecialchars($_POST['email']);
    $province_id = $_POST['province_id'];

    $user = getUserById($id);

    $user->setUsername($username);
    $user->setFirstname($firstname);
    $user->setLastname($lastname);
    $user->setEmail($email);
    $user->setProvinceId($province_id);

    if (!$user->edit()) {
        header("Location: /index.php?controller=user&action=profile&id=$id&error=user_edit");
    } else {
        header("Location: /index.php?controller=user&action=profile&id=$id&success=user_edit");
    }

?>
