<?php
    class UserProfileController extends Controller {
        private $userProfile;
        
        function __construct($user_id) {
            $user = getUserById($user_id);
            
            $this->userProfile = new UserProfile($user);          
        }
        
        function render() {
            $this->userProfile->render();
        }
    }
?>
