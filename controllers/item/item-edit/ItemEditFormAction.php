<?php
$root = realpath($_SERVER["DOCUMENT_ROOT"]);

require("$root/models/Database.php");
require("$root/models/User.php");
require("$root/models/Item.php");
require("$root/config.php");

if (isset($_POST["delete"])) {
  $values = explode("!",$_POST["delete"]);
  $table = $values[0];
  $id_item = $values[1];
  $picture = $values[2];
  if($table == "owned_items") {
    $item = getItemById($table,$id_item);
    $id_user = $item->getUserId();
    unlink("$root/models/images/items/$id_user/$id_item/$picture.png");
    header("Location: /index.php?controller=items&action=edit&id=$id_item&table=$table");
  }
}
else if (isset($_POST["edit"]))
  {
  $values = explode("!",$_POST["edit"]);
  $table = $values[0];
  $id_item = $values[1];
  $picture_name = $values[2];
  $item = getItemById($table,$id_item);
  $user_id = $item->getUserId();
  if (isset($_FILES["image_edit$picture_name"]))
    {
    $picture = $_FILES["image_edit$picture_name"];
    if ($picture['name'] != "")
      {
      if($table == "owned_items")
        {
        $item = getItemById($table,$id_item);
        $id_user = $item->getUserId();
        unlink("$root/models/images/items/$id_user/$id_item/$picture_name.png");

        $image_file = $picture['tmp_name'];

        $image_name = explode( ".",$picture['name']);

        list($width,$height)=getimagesize("$image_file");

        $newwidth=$GLOBALS['config']['item_photo_width'];
        $newheight=$GLOBALS['config']['item_photo_height'];
        $tmp= imagecreatetruecolor($newwidth,$newheight);

        if (strtolower($image_name[1]) == "png")
          {
          $src= imagecreatefrompng("$image_file");
          imagecopyresampled($tmp,$src,0,0,0,0,$newwidth,$newheight,$width,$height);

          imagepng($tmp,"../../../models/images/items/$user_id/$id_item/$picture_name.png" );
          }
        else if (strtolower($image_name[1]) == "jpg")
          {
          $src= imagecreatefromjpeg("$image_file");
          imagecopyresampled($tmp,$src,0,0,0,0,$newwidth,$newheight,$width,$height);

          imagejpeg($tmp,"../../../models/images/items/$user_id/$id_item/$picture_name.png" );
          }
        else
          {
          header("Location: /index.php?controller=items&error=type");
          }

        imagedestroy($src);
        imagedestroy($tmp);
        header("Location: /index.php?controller=items&action=edit&id=$id_item&table=$table");
        }
    }
    else
      {
      header("Location: /index.php?controller=items&action=edit&id=$id_item&table=$table");
      }
  }
  else
    {
    header("Location: /index.php?controller=items&action=edit&id=$id_item&table=$table");
    }
  }
  else if (isset($_POST["add"])) {
    $values = explode("!",$_POST["add"]);
    $table = $values[0];
    $id_item = $values[1];
    $picture_name = $values[2];
    $item = getItemById($table,$id_item);
    $user_id = $item->getUserId();
    if (isset($_FILES["image_add$picture_name"]))
      {
      $picture = $_FILES["image_add$picture_name"];
      if ($picture['name'] != "")
        {
        if($table == "owned_items")
          {
          $item = getItemById($table,$id_item);
          $id_user = $item->getUserId();
          $image_file = $picture['tmp_name'];

          $image_name = explode( ".",$picture['name']);

          list($width,$height)=getimagesize("$image_file");

          $newwidth=$GLOBALS['config']['item_photo_width'];
          $newheight=$GLOBALS['config']['item_photo_height'];
          $tmp= imagecreatetruecolor($newwidth,$newheight);

          if (strtolower($image_name[1]) == "png")
            {
            $src= imagecreatefrompng("$image_file");
            imagecopyresampled($tmp,$src,0,0,0,0,$newwidth,$newheight,$width,$height);

            imagepng($tmp,"../../../models/images/items/$user_id/$id_item/$picture_name.png" );
            }
          else if (strtolower($image_name[1]) == "jpg")
            {
            $src= imagecreatefromjpeg("$image_file");
            imagecopyresampled($tmp,$src,0,0,0,0,$newwidth,$newheight,$width,$height);

            imagejpeg($tmp,"../../../models/images/items/$user_id/$id_item/$picture_name.png" );
            }
          else
            {
            header("Location: /index.php?controller=items&error=type");
            }
          imagedestroy($src);
          imagedestroy($tmp);
          header("Location: /index.php?controller=items&action=edit&id=$id_item&table=$table");
          }
        }
        else
          {
          header("Location: /index.php?controller=items&action=edit&id=$id_item&table=$table");
          }
      }
  }
  else if (isset($_POST["stat"]))
    {
      $values = explode("!",$_POST["stat"]);
      $table = $values[0];
      $id_item = $values[1];
      $item = getItemById($table,$id_item);
      if (isset($_POST["stat_id"]))
      {
        $stat_id = $_POST["stat_id"];
        $item->updateStat($table,$stat_id);
      }
      header("Location: /index.php?controller=items&action=edit&id=$id_item&table=$table");
  }
?>
