<?php
    class ItemEditFormController extends Controller {
        private $itemEditForm;

        function __construct($id_item, $table) {
            $item = getItemById($table,$id_item);

            $this->itemEditForm = new ItemEditForm($item,$table);
        }

        function render() {
            $this->itemEditForm->render();
        }
    }
?>
