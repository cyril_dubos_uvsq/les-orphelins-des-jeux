<?php
  session_start();

 	if (!isset($_POST["table"])) {
        header("Location: /index.php?controller=items&error=table_undefined");
    } else if (!isset($_POST["game_id"])) {
        header("Location: /index.php?controller=items&error=game_id_undefined");
    } else if (!isset($_POST["type_id"])) {
        header("Location: /index.php?controller=items&error=type_id_undefined");
    } else {
 	 	$root = realpath($_SERVER["DOCUMENT_ROOT"]);

 	 	require("$root/models/Database.php");
 	 	require("$root/models/Item.php");
    require("$root/config.php");


    $table = $_POST["table"];
 	 	$user_id = $_SESSION["user_id"];
 	 	$game_id = $_POST["game_id"];
 	 	$type_id = $_POST["type_id"];
    $stat_id = $_POST["stat_id"];
 	 	$status = 1;

        $item = new Item(null, $user_id, $game_id, $type_id,$stat_id, null, $status);

        if (!$db_result = $item->add($table)) {
            header("Location: /index.php?controller=items&error=game_add_db");
        } else {

            if(isset($_FILES["images_item"])) {
                $images_item = $_FILES["images_item"];
                //print_r($images_item);
                $comptImage = 0;
                while (isset($images_item['name'][$comptImage])) {

                  if (!is_dir("../../../models/images/items/$user_id")) {
                    mkdir("../../../models/images/items/$user_id");
                    }

                  $id_item = $item->getId();
                  if (!is_dir("../../../models/images/items/$user_id/$id_item")) {
                    mkdir("../../../models/images/items/$user_id/$id_item");
                    }




                  $image_file = $images_item['tmp_name'][$comptImage];

                  $image_name = explode( ".",$images_item['name'][$comptImage]);
                  $comptImage = $comptImage + 1;

                  list($width,$height)=getimagesize("$image_file");

                  $newwidth=$GLOBALS['config']['item_photo_width'];
                  $newheight=$GLOBALS['config']['item_photo_height'];
                  $tmp= imagecreatetruecolor($newwidth,$newheight);

                  if (strtolower($image_name[1]) == "png") {
                    $src= imagecreatefrompng("$image_file");
                    imagecopyresampled($tmp,$src,0,0,0,0,$newwidth,$newheight,$width,$height);

                    imagepng($tmp,"../../../models/images/items/$user_id/$id_item/$comptImage.png");
                  }
                  else if (strtolower($image_name[1]) == "jpg") {
                    $src= imagecreatefromjpeg("$image_file");
                    imagecopyresampled($tmp,$src,0,0,0,0,$newwidth,$newheight,$width,$height);

                    imagejpeg($tmp,"../../../models/images/items/$user_id/$id_item/$comptImage.png");
                  }
                  else {
                    header("Location: /index.php?controller=items&error=type");
                  }

                  imagedestroy($src);
                  imagedestroy($tmp);
                }
             header("Location: /index.php?controller=items");
            }
            header("Location: /index.php?controller=items");
        }
    }
?>
