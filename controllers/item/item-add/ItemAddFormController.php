<?php
    class ItemAddFormController extends Controller {
        private $itemAddForm;

        function __construct() {
            $platforms = getPlatforms();
            $platform_id = $platforms[0]->getId();

            $regions = getRegionsByPlatformId($platform_id);
            $region_id = $regions[0]->getId();

            $games = getGamesByRegionId($region_id);
            $game_id = $games[0]->getId();
            
            //$this->itemAddForm = new ItemAddForm($platforms, $regions, $games, $platform_id, $region_id, $game_id);
            $this->itemAddForm = new ItemAddForm($platforms, $regions, $games, null, null, null);
        }

        function render() {
            $this->itemAddForm->render();
        }
    }
?>