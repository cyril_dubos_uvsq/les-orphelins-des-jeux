<?php
    class ItemDeleteFormController extends Controller {
      private $itemDeleteForm;

      function __construct($id_item, $table) {
          $item = getItemById($table,$id_item);

          $this->itemDeleteForm = new ItemDeleteForm($id_item,$table);
      }

      function render() {
          $this->itemDeleteForm->render();
      }
    }
?>
