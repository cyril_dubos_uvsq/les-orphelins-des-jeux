<?php
    $root = realpath($_SERVER["DOCUMENT_ROOT"]);
    session_start();
    require("$root/models/Database.php");
    require("$root/models/User.php");
    require("$root/models/Item.php");
    require("$root/models/Match.php");

    $values = explode("!",$_POST["id"]);
    $id_item = $values[0];
    $table = $values[1];
    $item = getItemById($table,$id_item);
    $user_id = $item->getUserId();
    echo $_SESSION["user_id"];
    if ($user_id != $_SESSION["user_id"]) {
      header("Location: /index.php?controller=items");
      return;
    }

    $id_user = $item->getUserId();

    $item->delete($table);

    deleteAllMatchByIdItem($table,$id_item);

    $picture_path ="$root/models/images/items/$id_user/$id_item";
    if (file_exists($picture_path)) {
        supprimer_dossier($picture_path);
    }

    header("Location: /index.php?controller=items");



    function supprimer_dossier($directory, $empty = false) {
        if(substr($directory,-1) == "/") {
            $directory = substr($directory,0,-1);
        }

        if(!file_exists($directory) || !is_dir($directory)) {
            return false;
        } elseif(!is_readable($directory)) {
            return false;
        } else {
            $directoryHandle = opendir($directory);

            while ($contents = readdir($directoryHandle)) {
                if($contents != '.' && $contents != '..') {
                    $path = $directory . "/" . $contents;

                    if(is_dir($path)) {
                        supprimer_dossier($path);
                    } else {
                        unlink($path);
                    }
                }
            }

            closedir($directoryHandle);

            if($empty == false) {
                if(!rmdir($directory)) {
                    return false;
                }
            }

            return true;
        }
    }

?>
