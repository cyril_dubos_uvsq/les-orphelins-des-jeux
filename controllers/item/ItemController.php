<?php
    class ItemController extends Controller {
        function render() {
            echo("<h1 class='m-5'>My items</h1>");

            if (!$this->action) {
                if (!isset($_SESSION["user_id"])) {
                  header("Location: index.php");
                }
                $user_id = $_SESSION["user_id"];
                $user = getUserbyId($user_id);
                $username = $user->getUsername();

                $this->add();
                $this->list();
            } else {
                switch($this->action){
                    case("delete"):
                        $this->delete();
                        break;
                    case("edit"):
                        $this->edit();
                        break;
                }
            }
        }

        function add() {
            if (isset($_SESSION["user_id"])) {
                $user_id = $_SESSION["user_id"];

                //if (!isAdministrator($user_id)) {
                    $itemAddForm = new ItemAddFormController();
                    $card = new Card("Ajouter un objet", $itemAddForm);
                    $card->render();
                //}
            }
        }

        function delete() {
            $id_item = $_GET['id'];
            $table = $_GET['table'];
            $item = getItemById($table,$id_item);
            $user_id = $item->getUserId();
            if ($user_id!=$_SESSION["user_id"]) {
              header("Location: index.php?controller=items");
            }
            if(isset($item)) {
              $id_user_item = $item->getUserId();
              $id_user = $_SESSION['user_id'];
              if($id_user != $id_user_item) {
                header("Location: index.php?controller=items");
              }
              else {
                $itemDeleteFormController = new ItemDeleteFormController($id_item, $table);

                $card = new Card("Supprimer un objet", $itemDeleteFormController);
                $card->render();
              }
            }
          else {
            header("Location: index.php?controller=items");
          }
        }

        function edit() {
            $id_item = $_GET['id'];
            $table = $_GET['table'];
            $item = getItemById($table,$id_item);
            $user_id = $item->getUserId();
            if ($user_id!=$_SESSION["user_id"]) {
              header("Location: index.php?controller=items");
            }
            $itemEditFormController = new ItemEditFormController($id_item, $table);
            $cardOwn = new Card("Modifier un objet", $itemEditFormController);
            $cardOwn->render();
        }

        function list() {

            $itemsSrc = getItemsByUserId("searched_items",$_SESSION["user_id"], true);
            $itemsOwn = getItemsByUserId("owned_items",$_SESSION["user_id"], true);


            $itemListTableSrc = new ItemListTable("searched_items",$itemsSrc);
            $itemListTableOwn = new ItemListTable("owned_items",$itemsOwn);

            $cardSrc = new Card("Liste des objet recherchés", $itemListTableSrc);
            $cardSrc->render();

            $cardOwn = new Card("Liste des objet possédés", $itemListTableOwn);
            $cardOwn->render();
        }
    }
?>
