<?php
    if (isset($_POST["name"]) && isset($_POST["description"])) {
        $root = realpath($_SERVER["DOCUMENT_ROOT"]);

        require("$root/models/Database.php");
        require("$root/models/Platform.php");

        $name = $_POST["name"];
        $description = $_POST["description"];

        $platform = new Platform(null, $name, $description);

        if (!$db_result = $platform->add()) {
            header("Location: /index.php?controller=database&error=platform_add_db");
        } else {
            header("Location: /index.php?controller=database");
        }
    } else {
        header("Location: /index.php?controller=database&error=platform_add_args");
    }
?>
