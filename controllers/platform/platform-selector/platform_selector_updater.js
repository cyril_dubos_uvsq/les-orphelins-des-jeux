window.addEventListener("load", initialization_platformSelectorUpdate, false);

function initialization_platformSelectorUpdate() {
    platform_select = document.getElementById("platform_select");
	platform_select.addEventListener("change", function () {
		platformSelectorUpdate();
		setTimeout(regionSelectorUpdate, 50);
	}, false);
}

function platformSelectorUpdate() {
	clearContent(document.getElementById("region_select"));
	
    platform_select = document.getElementById("platform_select");
    selected_index = platform_select.selectedIndex;
    selected_option = platform_select.options[selected_index];
    platform_id = selected_option.value;

  	xhr = new XMLHttpRequest();
	xhr.onreadystatechange = callback_platformSelectorUpdate;
	xhr.open("GET","/controllers/region/region-selector/RegionSelectorOptions.php?platform_id=" + platform_id, true);
	xhr.send(null)
}

function callback_platformSelectorUpdate(){
	if(xhr.readyState == 4 && xhr.status == 200){
		response_text = xhr.responseText;
		
		region_select = document.getElementById("region_select");
		region_select.innerHTML = response_text;
	}
}

function clearContent(element){
	while (element.childNodes.length != 0)
		element.removeChild(element.childNodes[0]);
}