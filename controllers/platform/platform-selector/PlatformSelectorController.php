<?php
    class PlatformSelectorController extends Controller {
        private $platformSelector;

        function __construct($platform_id = null) {
            $platforms = getPlatforms();

            $this->platformSelector = new PlatformSelector($platforms, $platform_id);
        }

        function render() {
            $this->platformSelector->render();
        }
    }
?>