<?php
    if (isset($_POST["id"]) && isset($_POST["name"]) && isset($_POST["description"])) {
        $root = realpath($_SERVER["DOCUMENT_ROOT"]);

        require("$root/models/Database.php");
        require("$root/models/Platform.php");

        $id = $_POST["id"];
        $name = $_POST["name"];
        $description = $_POST["description"];

        $platform = new Platform($id, $name, $description);
    
        if (!$db_result = $platform->edit()) {
            header("Location: /index.php?controller=database&error=platform_edit_db");
        } else {
            header("Location: /index.php?controller=database");
        }
    } else {
        header("Location: /index.php?controller=database&error=platform_edit_args");
    }
?>
