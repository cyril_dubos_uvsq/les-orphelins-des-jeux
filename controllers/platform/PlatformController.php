<?php
    class PlatformController extends Controller {
        function render() {
            echo("<h1 class='m-5'>");
            echo($GLOBALS["locale"]["platform"]);
            echo("</h1>");

            if ($this->id) {
                $platform = getPlatformById($this->id);

                if ($platform) {
                    $platform_name = $platform->getName();

                    //echo("<h2 class='text-muted m-5'><small>$platform_name</small></h2>");
                    
                    if ($this->action) {
                        switch ($this->action) {                   
                            case "edit":
                                $this->edit();
                                break;

                            case "delete":
                                $this->delete();
                                break;

                            default:
                                header("Location: /index.php?error=invalid_action");
                                break;
                        }
                    } else {
                        $this->description();
                        $this->add();
                        $this->list();
                    }
                } else {
                    header("Location: /index.php?error=invalid_id");
                }
            } else {
                header("Location: /index.php?error=undefined_id");
            }
        }

        function description() {
            $platform = getPlatformById($this->id);
            $platform_name = $platform->getName();
            $platform_description = $platform->getDescription();

            $items = array(
                $GLOBALS["locale"]["database"] => "/index.php?controller=database",
                "$platform_name" => "/index.php?controller=platform&id=$this->id",
            );

            $textView = new TextView($platform_description);
            
            $card = new BreadCard($items, $textView);
            $card->render();
        }

        function add() {
            if (isset($_SESSION["user_id"])) {
                $user_id = $_SESSION["user_id"];
                $user = getUserById($user_id);

                if ($user->isAdministrator()) {
                    $regionAddFormController = new RegionAddFormController($this->id);
            
                    $card = new Card($GLOBALS["locale"]["region_add"], $regionAddFormController);
                    $card->render();
                }
            }
        }

        function list() {
            $regions = getRegionsByPlatformId($this->id);
            
            $regionListTable = new RegionListTable($regions);
            
            $card = new Card($GLOBALS["locale"]["region_list"], $regionListTable);
            $card->render();
        }

        function edit() {
            if (isset($_SESSION["user_id"])) {
                $user_id = $_SESSION["user_id"];
                $user = getUserById($user_id);

                if ($user->isAdministrator()) {
                    $platform = getPlatformById($this->id);

                    $platformEditForm = new PlatformEditForm($platform);
                    
                    $card = new Card($GLOBALS["locale"]["platform_edit"], $platformEditForm);
                    $card->render();
                } else {
                    header("Location: /index.php?error=unauthorized_action");
                }
            }
        }

        function delete() {
            if (isset($_SESSION["user_id"])) {
                $user_id = $_SESSION["user_id"];
                $user = getUserById($user_id);

                if ($user->isAdministrator()) {
                    $platformDeleteFormController = new PlatformDeleteFormController($this->id);
            
                    $card = new Card($GLOBALS["locale"]["platform_delete"], $platformDeleteFormController);
                    $card->render();
                }  else {
                    header("Location: /index.php?error=unauthorized_action");
                }
            }
        }
    }
?>