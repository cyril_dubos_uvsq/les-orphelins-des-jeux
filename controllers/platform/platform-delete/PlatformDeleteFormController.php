<?php
    class PlatformDeleteFormController extends Controller {
        private $platformDeleteForm;

        function __construct($platform_id) {
            $platform = getPlatformById($platform_id);

            $this->platformDeleteForm = new PlatformDeleteForm($platform);
        }

        function render() {
            $this->platformDeleteForm->render();
        }
    }
?>