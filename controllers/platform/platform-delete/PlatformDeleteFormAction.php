<?php
    if (isset($_POST["id"])) {
        $root = realpath($_SERVER["DOCUMENT_ROOT"]);

        require("$root/models/Database.php");
        require("$root/models/Platform.php");

        $id = $_POST["id"];

        $platform = new Platform($id, null, null, null, null, null);

        if (!$db_result = $platform->delete()) {
            header("Location: /index.php?controller=database&error=platform_delete_db");
        } else {
            header("Location: /index.php?controller=database");
        }
    } else {
        header("Location: /index.php?controller=database&error=platform_delete_args");
    }
?>
