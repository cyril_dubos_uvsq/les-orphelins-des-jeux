<?php
    abstract class Controller {
        protected $action;
        protected $id;

        function __construct($action, $id) {
            $this->action = $action;
            $this->id = $id;
        }

        abstract function render();
    }
?>