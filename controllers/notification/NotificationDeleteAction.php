<?php
	$root = realpath($_SERVER["DOCUMENT_ROOT"]);
	
	require("$root/models/Database.php");
	require("$root/models/Notification.php");

	$id = $_POST['id'];
	$notification = getNotificationById($id);
	
	if ($notification->delete()) {
		header("Location: /index.php?controller=notification&success=deleted");
	} else {
		header("Location: /index.php?controller=notification&error=not_deleted");
	}
?>
