<?php
	class NotificationController extends Controller {

		function render() {
			$user = getUserById($this->id);

            $session_user_id = $_SESSION["user_id"];
            $session_user = getUserById($session_user_id);

            if (!$session_user->isAdministrator()) {
                header("Location: index.php?error=unauthorized_action");
            }

			echo $this->action;

			if ($this->action) {
				header("Location: index.php?error=invalid_action");
			} else {
				$this->add();
				$this->list();
			}
		}

		function add() {
			$notifications = new NotificationAddForm();
			$card = new Card("Add notification", $notifications);
			$card->render();
		}

		function List() {
			$notifications = getNotifications();
			$notifications = new NotificationListTable($notifications);
			$card = new Card("List of notification(s)", $notifications);
			$card->render();
		}
	}
?>
