<?php
	$root = realpath($_SERVER["DOCUMENT_ROOT"]);
	
	require("$root/models/Database.php");
	require("$root/models/Notification.php");

	$text = $_POST["content_notification"];

	$notification = new Notification(null,$text,null);


	if ($notification->add()) {
		header("Location: /index.php?controller=notification&success=Notification_added");
	} else {		
		header("Location: /index.php?controller=notification&error=Notification_added");
	}

?>
