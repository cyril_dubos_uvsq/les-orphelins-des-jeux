<?php
    if (!isset($_POST["title"])) {
        header("Location: /index.php?error=title_undefined");
    } else if (!isset($_POST["year"])) {
        header("Location: /index.php?error=year_undefined");
    } else if (!isset($_POST["region_id"])) {
        header("Location: /index.php?error=region_id_undefined");
    } else if (!isset($_POST["description"])) {
        header("Location: /index.php?error=description_undefined");
    } else if (!isset($_POST["language"])) {
        header("Location: /index.php?error=language_undefined");
    } else {
        $title = $_POST["title"];
        $year = $_POST["year"];
        $region_id = $_POST["region_id"];
        $description = $_POST["description"];
        $language = $_POST["language"];

        $root = realpath($_SERVER["DOCUMENT_ROOT"]);

        require("$root/models/Database.php");
        require("$root/models/Game.php");

        $game = new Game(null, $title, $year, $region_id, $description, $language);

        if (!$db_result = $game->add()) {
            header("Location: /index.php?controller=region&id=$region_id&error=game_not_added");
        } else {
            header("Location: /index.php?controller=region&id=$region_id&success=game_added");
        }
    }
?>
