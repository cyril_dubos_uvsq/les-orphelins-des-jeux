<?php
    class GameAddFormController extends Controller {
        private $gameAddForm;

        function __construct($region_id = null) {
            $regionSelectorController = new RegionSelectorController($region_id);
            
            $this->gameAddForm = new GameAddForm($regionSelectorController);
        }

        function render() {
            $this->gameAddForm->render();
        }
    }
?>