<?php
    class GameEditFormController extends Controller {
        private $gameEditForm;

        function __construct($game_id) {
            $game = getGameById($game_id);
            $region_id = $game->getRegionId();

            $regionSelectorController = new RegionSelectorController($region_id);

            $this->gameEditForm = new GameEditForm($regionSelectorController, $game);
        }

        function render() {
            $this->gameEditForm->render();
        }
    }
?>