<script>
    function getRegionSelect(platform_id) {
        xmlhttp = new XMLHttpRequest()

        xmlhttp.onreadystatechange = function () {
            if (this.readyState == 4 && this.status == 200) {
                if(regionSelect = document.querySelector("#game-edit-region-select")) {
                    regionSelect.innerHTML = this.responseText
                }
            }
        };

        xmlhttp.open("GET", "/models/get_region_select.php?platform_id=" + platform_id, true)

        xmlhttp.send()
    }
</script>

<?php
    if (isset($_POST["id"]) && isset($_POST["title"]) && isset($_POST["year"]) && isset($_POST["region_id"]) && isset($_POST["description"]) && isset($_POST["language"])) {
        $root = realpath($_SERVER["DOCUMENT_ROOT"]);

        require("$root/models/Database.php");
        require("$root/models/Game.php");

        $id = $_POST["id"];
        $title = $_POST["title"];
        $year = $_POST["year"];
        $region_id = $_POST["region_id"];
        $description = $_POST["description"];
        $language = $_POST["language"];

        $game = new Game($id, $title, $year, $region_id, $description, $language);

        if (!$db_result = $game->edit()) {
            //echo $id.$title.$year.$region_id.$description;
            header("Location: /index.php?controller=region&id=$region_id&error=game_not_edited");
        } else {
            header("Location: /index.php?controller=region&id=$region_id&succes=game_edited");
        }
    } else {
        header("Location: /index.php?controller=region&id=$region_id&error=game_not_edited");
    }
?>
