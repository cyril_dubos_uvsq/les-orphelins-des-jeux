<?php
    class GameController extends Controller {
        function render() {
            echo("<h1 class='m-5'>");
            echo($GLOBALS["locale"]["game"]);
            echo("</h1>");

            if ($this->id) {
                $game = getGameById($this->id);

                if($game) {
                    if ($this->action) {
                        switch ($this->action) {                        
                            case "edit":
                                $this->edit();
                                break;

                            case "delete":
                                $this->delete();
                                break;
        

                            default:
                                header("Location: /index.php?error=invalid_action");
                                break;
                        }
                    } else {
                        $this->description();
                    }
                } else {
                    header("Location: /index.php?error=invalid_id");
                }
            } else {
                header("Location: /index.php?error=undefined_id");
            }
        }

        function description() {
            $game = getGameById($this->id);
            $game_title = $game->getTitle();
            $game_description = $game->getDescription();

            $region_id = $game->getRegionId();
            $region = getregionById($region_id);
            $region_name = $region->getName();

            $platform_id = $region->getPlatformId();
            $platform = getPlatformById($platform_id);
            $platform_name = $platform->getName();

            $items = array(
                $GLOBALS["locale"]["database"] => "/index.php?controller=database",
                "$platform_name" => "/index.php?controller=platform&id=$platform_id",
                "$region_name" => "/index.php?controller=region&id=$region_id",
                "$game_title" => "/index.php?controller=game&id=$this->id",
            );

            $textView = new TextView($game_description);
            
            $card = new BreadCard($items, $textView);
            $card->render();
        }

        function edit() {
            if (isset($_SESSION["user_id"])) {
                $user_id = $_SESSION["user_id"];
                $user = getUserById($user_id);

                if ($user->isAdministrator()) {
                    $gameEditFormController = new GameEditFormController($this->id);
            
                    $card = new Card($GLOBALS["locale"]["game_edit"], $gameEditFormController);
                    $card->render();
                } else {
                    header("Location: /index.php?error=unauthorized_action");
                }
            }
        }

        function delete() {
            if (isset($_SESSION["user_id"])) {
                $user_id = $_SESSION["user_id"];
                $user = getUserById($user_id);

                if ($user->isAdministrator()) {
                    $gameDeleteFormController = new GameDeleteFormController($this->id);
            
                    $card = new Card($GLOBALS["locale"]["game_delete"], $gameDeleteFormController);
                    $card->render();
                } else {
                    header("Location: /index.php?error=unauthorized_action");
                }
            }
        }
    }
?>