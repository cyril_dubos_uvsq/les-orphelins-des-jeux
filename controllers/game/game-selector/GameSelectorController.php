<?php
    class GameSelectorController extends Controller {
        private $gameSelector;

        function __construct($game_id = null) {    
            $region_id = null;
            $games = null;

            if($game_id) {
                $game = getGameById($game_id);
                if ($game) {
                    $region_id = $game->getRegionId();
                    $games = getGamesByRegionId($region_id);
                }
            }

            $regionSelectorController = new RegionSelectorController($region_id);

            $this->gameSelector = new GameSelector($regionSelectorController, $games, $game_id);
        }

        function render() {
            $this->gameSelector->render();
        }
    }
?>