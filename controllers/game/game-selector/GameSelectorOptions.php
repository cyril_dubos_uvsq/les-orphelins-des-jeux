<?php
    if (isset($_GET["region_id"])) {
        $region_id = $_GET["region_id"];

        $root = realpath($_SERVER["DOCUMENT_ROOT"]);

        require("$root/models/Database.php");
        require("$root/models/Game.php");

        echo("<option disabled selected>Select an option</option>");

        $games = getGamesByRegionId($region_id);

        foreach ($games as $game) {
            $id = $game->getId();
            $title = $game->getTitle();
            
            echo("<option value='$id'>$title</option>");
        }
    }
?>