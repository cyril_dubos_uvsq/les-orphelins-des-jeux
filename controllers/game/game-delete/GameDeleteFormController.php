<?php
    class GameDeleteFormController extends Controller {
        private $gameDeleteForm;

        function __construct($game_id) {
            $game = getGameById($game_id);
            $region_id = $game->getRegionId();
            
            $platform = getPlatformByRegionId($region_id);
            $platform_id = $platform->getId();

            $this->gameDeleteForm = new GameDeleteForm($game, $platform_id, $region_id);
        }

        function render() {
            $this->gameDeleteForm->render();
        }
    }
?>