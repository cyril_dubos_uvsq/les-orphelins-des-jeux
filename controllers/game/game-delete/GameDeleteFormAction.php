<?php
    if (!isset($_POST["id"])) {
        header("Location: /index.php?error=language_undefined");
    } else if (!isset($_POST["region_id"])) {
        header("Location: /index.php?error=region_id_undefined");
    } else {
        $id = $_POST["id"];
        $region_id = $_POST["region_id"];

        $root = realpath($_SERVER["DOCUMENT_ROOT"]);

        require("$root/models/Database.php");
        require("$root/models/Game.php");

        $game = new Game($id, null, null, null, null, null);

        if (!$db_result = $game->delete()) {
            header("Location: /index.php?controller=region&id=$region_id&error=game_not_deleted");
        } else {
            header("Location: /index.php?controller=region&id=$region_id&success=game_deleted");
        }
    }
?>
