<?php
    $root = realpath($_SERVER["DOCUMENT_ROOT"]);

    require("$root/models/Database.php");
    require("$root/models/User.php");

    if (!isset($_POST["username"])) {
        header("Location: /index.php?controller=sign&action=in&error=username_undefined");
    } else if (!isset($_POST["password"])) {
        header("Location: /index.php?controller=sign&action=in&error=password_undefined");
    } else {
        $username = htmlspecialchars($_POST['username']);
        $password = htmlspecialchars($_POST['password']);

        if (!$user = getUserByUsername($username)) {
            header("Location: /index.php?controller=sign&action=in&error=invalid_username_password");
        } else if (!password_verify($password, $user->getPassword())) {
            header("Location: /index.php?controller=sign&action=in&error=invalid_username_password");
        } else if (!($user->getStatus())){
            header("Location: /index.php?controller=sign&action=in&error=user_locked");
        }
        else {
            session_start();
            $_SESSION["user_id"] = $user->getId();
            header("Location: /index.php?success=sign_in_message");
        }
    }
?>
