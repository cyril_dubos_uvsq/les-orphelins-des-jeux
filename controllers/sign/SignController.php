<?php
    class SignController extends Controller {
        function render() {
            if (!$this->id) {
                if ($this->action) {
                    switch ($this->action) {
                        case "up":
                            $this->up();
                            break;

                        case "in":
                            $this->in();
                            break;

                        case "out":
                            $this->out();
                            break;

                        default:
                            header("Location: /index.php?error=invalid_action");
                            break;
                    }
                } else {
                    header("Location: /index.php?error=undefined_action");
                }
            } else {
                header("Location: /index.php?error=invalid_id");
            }
        }

        function up() {
            $signUpForm = new SignUpForm();

            $card = new Card($GLOBALS["locale"]["sign_up"], $signUpForm);
            $card->render();
        }

        function in() {
            $signInForm = new SignInForm();

            $card = new Card($GLOBALS["locale"]["sign_in"], $signInForm);
            $card->render();
        }

        function out() {
            $signOutForm = new SignOutForm();

            $card = new Card($GLOBALS["locale"]["sign_out"], $signOutForm);
            $card->render();
        }

    }
?>
