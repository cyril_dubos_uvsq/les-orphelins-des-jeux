<?php
    $root = realpath($_SERVER["DOCUMENT_ROOT"]);

    require("$root/models/Database.php");
    require("$root/models/User.php");
    require("$root/config.php");

    if (!isset($_POST['firstname'])) {
        header("Location: /index.php?controller=sign&action=up&error=firstname_undefined");
    } else if (!isset($_POST['lastname'])) {
        header("Location: /index.php?controller=sign&action=up&error=lastname_undefined");
    } else if (!isset($_POST['username'])) {
        header("Location: /index.php?controller=sign&action=up&error=username_undefined");
    } else if (!isset($_POST['email'])) {
        header("Location: /index.php?controller=sign&action=up&error=email_undefined");
    } else if (!isset($_POST['password'])) {
        header("Location: /index.php?controller=sign&action=up&error=password_undefined");
    } else if (!isset($_POST['c_password'])) {
        header("Location: /index.php?controller=sign&action=up&error=password_unconfirmed");
    } else if (!isset($_POST['province_id'])) {
        header("Location: /index.php?controller=sign&action=up&error=province_id_undefined");
    } else {
        $firstname = htmlspecialchars($_POST['firstname']);
        $lastname = htmlspecialchars($_POST['lastname']);
        $username = htmlspecialchar($_POST['username']);
        $email = htmlspecialchars($_POST['email']);
        $password = password_hash($_POST['password'], PASSWORD_DEFAULT);
        $c_password = password_hash($_POST['c_password'], PASSWORD_DEFAULT);
        $province_id = $_POST['province_id'];

        if($_POST['password'] != $_POST['c_password']) {
            header("Location: /index.php?controller=sign&action=up&error=passwords_unmatched");
        } else if($user = getUserByUsername($username)) {
            header("Location: /index.php?controller=sign&action=up&error=username_used");
        } else if($user = getUserByEmail($email)) {
            header("Location: /index.php?controller=sign&action=up&error=email_used");
        } else {


            $user = new User(null,$firstname, $lastname, $username, $email, $password, 0, 0, 0, $province_id, null, 1);

            if(!$db_result = $user->add()) {
                header("Location: /index.php?controller=sign&action=up&error=signup_error");
            } else {
                if(isset($_FILES["avatar"])) {
                    $avatar = $user->getId();
                    $avatar_file = $_FILES['avatar']['tmp_name'];

                    $image_name = explode( ".",$_FILES['avatar']['name']);

                    list($width,$height)=getimagesize("$avatar_file");

                    $newwidth=$GLOBALS["config"]["user_avatar_width"];
                    $newheight=$GLOBALS["config"]["user_avatar_height"];
                    $tmp= imagecreatetruecolor($newwidth,$newheight);

                    if (strtolower($image_name[1]) == "png") {
                      $src= imagecreatefrompng("$avatar_file");
                      imagecopyresampled($tmp,$src,0,0,0,0,$newwidth,$newheight,$width,$height);

                      imagepng($tmp, "../../models/images/avatars/$avatar.png");
                    }
                    else if (strtolower($image_name[1]) == "jpg") {
                      $src= imagecreatefromjpeg("$avatar_file");
                      imagecopyresampled($tmp,$src,0,0,0,0,$newwidth,$newheight,$width,$height);

                      imagejpeg($tmp, "../../models/images/avatars/$avatar.png",250);
                    }
                    else {
                      header("Location: /index.php?controller=sign&action=up&error=type");
                    }

                    imagedestroy($src);
                    imagedestroy($tmp);
                }

                // Send activation email

                $token = md5(microtime(TRUE) * 100000);

                $sujet = "Les Orphelins des Jeux - Activer votre compte" ;
                $header = "From: noreply@lesorphelinsdesjeux.com" ;

                $message = '
                    Bienvenue sur Les Orphelins des Jeux,

                    Pour activer votre compte, veuillez cliquer sur le lien ci-dessous
                    ou copier/coller dans votre navigateur Internet.

                    http://votresite.com/index.php?controller=user&action=activate&id=' . urlencode($token) . '

                    ---------------

                    Ceci est un mail automatique, Merci de ne pas y répondre.
                ';

                mail($email, $subject, $message, $header) ;

                header("Location: /index.php?success=signup");
            }
        }
    }
?>
