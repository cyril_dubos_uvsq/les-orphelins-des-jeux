<?php
    $root = realpath($_SERVER["DOCUMENT_ROOT"]);

    if (isset($_GET["language"])) {
        $language = $_GET["language"];

        setcookie("language", $language, 0, "/");
    }

    header("Location: /index.php");
?>