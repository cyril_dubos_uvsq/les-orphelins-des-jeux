<?php require("requires.php"); ?>

<!DOCTYPE html>
<html lang="fr">
    <head>
        <meta charset="UTF-8">
        <title><?= $GLOBALS["locale"]["website_title"] ?></title>
        <link rel="stylesheet" href="libraries/css/bootstrap.css" />
        <link rel="stylesheet" href="libraries/css/lightbox.css" />
    </head>
    <body>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
        

        <?php
            $navbar = new Navbar();
            $navbar->render();
        ?>

        <div class="container">
            <?php
                $controller = get("controller");
                $action = get("action");
                $id = get("id");

                $router = new Router($controller, $action, $id);
                $router->render();
            ?>
        </div>

        <?php
            $error = get("error");

            if($error) {
                echo("
                    <div class='fixed-bottom'>
                        <div class='text-center alert alert-danger m-0' role='alert'>
                            {$GLOBALS['locale'][$error]}
                        </div>
                    </div>
                ");
            }
        ?>

        <?php
            $success = get("success");

            if($success) {
                echo("
                    <div class='fixed-bottom'>
                        <div class='text-center alert alert-success m-0' role='alert'>
                            {$GLOBALS['locale'][$success]}
                        </div>
                    </div>
                ");
            }
        ?>

        <script src="/libraries/js/jquery.js"></script>
        <script src="/libraries/js/lightbox.js"></script>
        <script src="/libraries/js/bootstrap.js"></script>
        
        <script>
            $('[data-toggle="popover"]').popover({ html: true, trigger: 'focus' });
        </script>
    </body>
</html>
