<?php
    $locale = array(
        "en_US" => "English (US)",
        "fr_FR" => "Français",

        "website_title" => "Les Orphelins des Jeux",
        "home" => "Home",

        // Error
        "error" => "Error",
        "unauthorized_action" => "Unauthorized action",

        "undefined_controller" => "Undefined controller",
        "undefined_action" => "Andefined action",
        "undefined_id" => "Undefined ID",

        "invalid_controller" => "Invalid controller",
        "invalid_action" => "Invalid action",
        "invalid_id" => "Invalid ID",

        // Success
        "success" => "Success",
        "sign_in_message" => "You are connected!",
        "sign_out_message" => "You are disconnected!",

        // Database
        "database" => "Games library",
        "database_message" => "Cette ludothèque répertorie tous les jeux pourvant être recherchés ou proposés sur le site. Ils sont classés en fonction de leur platforme, ainsi que de leur région.",


        // Platform
        "platform" => "Platform",
        "platform_add" => "Add platform",
        "platform_list" => "List of platforms",
        "platform_edit" => "Edit platform",
        "platform_delete" => "Delete platform",

        // Region
        "region" => "Region",
        "region_add" => "Add region",
        "region_list" => "List of regions",
        "region_edit" => "Edit region",
        "region_delete" => "Delete region",

        // Game
        "game" => "Game",
        "game_add" => "Add game",
        "game_list" => "List of games",
        "game_edit" => "Edit game",
        "game_delete" => "Delete game",
        "active" =>"Active",
        "disabled" =>"Disabled",

        //Users
        "research_users" => "Search Users :",
        "list_users" => "User List",

        //Item
        "items" => "Items",
        "research_items" => "Search for items :",
        "research" => "Research",
        "list_items" => "List of Items Owned by Users",
        "i_own" => "I own...",
        "title" => "Title",
        "my_items" => "My Items",
        "my_matches"=> "My Matches",
        "i_search" => "I search...",
        "game_name"=> "Name of the game",
        "game_type"=> "Type of the Jeu",
        "date_item"=> "Offer creation date",
        "item_delete_question" => "Do you really want to delete this item?",
        "owned_items" => "My owned items",
        "searched_items" => "My search items",
        "confirm" => "Confirm",
        "return" => "Return",
        "stat" => "Stat",

        // Notifications
        "add_notification"=>"Add a notification",
        "list_notifications"=>"Notifications List",
        "content" => "Content",

        "id" => "ID",
        "name" => "Name",
        "type" => "Type",
        "state" => "State",
        "pictures" => "Pictures",

        "sign_in" => "Sign in",
        "sign_up" => "Sign up",
        "sign_out" => "Sign out",
        "sign_out_question" => "Do you really want to disconnect ?",
        "return" => "Return",
        "welcome_message" => "Welcome on Les Orphelins des Jeux",
        "sign_out_message" => "Do you really want to sign out ?",
        "lock_message" => "Do you really want to lock this user ?",
        "unlock_message" => "Do you really want to unlock this user ?",
        "hello" => "Hello",
        "sumbit" => "Sumbit",
        "view" => "View",
        "edit" => "Edit",
        "delete" => "Delete",
        "add" => "Add",
        "lock" => "Lock",
        "unlock" => "Unlock",
        "username" => "Username",
        "password" => "Password",
        "confirm_password" => "Confirm your password",
        "select_option" => "Select an option",
        "country" => "Country",
        "province" => "Province",
        "avatar" => "Avatar",
        "conditions_check" => "I accept the conditions of use",
        "notifications_check" => "I would like to receive notifications",
        "email" => "Email",
        "firstname" => "Firstname",
        "lastname" => "Lastname",
        "default" => "Default : ",
        "myitems" => "My Items",
        "mymatches" => "My matches",
        "photos"=> "No pictures",
        "read_more" => "Read More",
        "created_at" => "Create at",
        "status" =>"Status",

        //Match
        "match_accept_question" => "Do you really want to accept this match ?",
        "match_refus_question" => "Do you really want to refuse this match ?",
        "match_vote_question" => "How would you rate the match ?",
        "accept" => "Accept",
        "match_cancel_question" => "Do you really want to cancel this match ?",
        "cancel" => "Cancel",
        "refus" => "Refuse"
    )
?>
