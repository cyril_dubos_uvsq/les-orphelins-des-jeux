<?php
    $locale = array(
        "en_US" => "English (US)",
        "fr_FR" => "Français",

        "hello" => "Bonjour",
        "website_title" => "Les Orphelins des Jeux",
        "welcome_message" => "Bienvenue sur Les Orphelins des Jeux",

        // Error
        "error" => "Erreur",
        "unauthorized_action" => "Action non authorisée",

        "undefined_controller" => "Controlleur non défnie",
        "undefined_action" => "Action non défnie",
        "undefined_id" => "ID non défnie",

        "invalid_controller" => "Controlleur invalide",
        "invalid_action" => "Action invalide",
        "invalid_id" => "ID invalide",

        // Global
        "user" => "Utilisateur",
        "id" => "ID",
        "name" => "Nom",
        "type" => "Type",
        "state" => "Etat",
        "title" => "Titre",
        "pictures" => "Photos",
        "language" => "Langue",
        "status" => "Status",
        "year" => "Année de sortie",
        "description" => "Description",
        "created_at" => "Ajouté le",

        "home" => "Accueil",
        "return" => "Retour",
        "read_more" => "En savoir plus",
        "sumbit" => "Envoyer",
        "view" => "Voir",
        "edit" => "Modifier",
        "delete" => "Supprimer",
        "search" => "Rechercher",
        "add" => "Ajouter",
        "lock" => "Bloquer",
        "unlock" => "Débloquer",
        "select_option" => "Sélectionnez une option",

        // Success
        "success" => "Succès",
        "sign_in_message" => "Vous êtes connecté !",
        "sign_out_message" => "Vous êtes déconnecté !",
        "sign_out_question" => "Voulez-vous vraiment vous déconnecter ?",

        // Database
        "database" => "Ludothèque",
        "database_message" => "Cette ludothèque répertorie tous les jeux pouvant être recherchés ou proposés sur le site. Ils sont classés en fonction de leur platforme, ainsi que de leur région.",

        // Platform
        "platform" => "Platforme",
        "platform_add" => "Ajouter une platforme",
        "platform_list" => "Liste des platformes",
        "platform_edit" => "Editer une platforme",
        "platform_delete" => "Supprimer une platforme",

        // Region
        "region" => "Région",
        "region_add" => "Ajouter une région",
        "region_list" => "Liste des régions",
        "region_edit" => "Editer une région",
        "region_delete" => "Supprimer une région",

        // Game
        "game" => "Jeu",
        "game_list" => "Liste des jeux",
        "game_add" => "Ajouter un jeu",
        "game_edit" => "Editer un jeu",
        "game_delete" => "Supprimer un jeu",
        "game_added" => "Jeu ajouté",
        "game_not_added" => "Jeu non ajouté",
        "game_edited" => "Jeu modifié",
        "game_not_edited" => "Jeu non modifié",
        "game_deleted" => "Jeu supprimé",
        "game_not_deleted" => "Jeu non supprimé",

        //Users
        "users" => "Utilisateurs",
        "user_locked" => "Utilisateur bloqué",
        "search" => "Rechercher",
        "list_users" => "Liste des Utilisateurs",

        // Item
        "items" => "Objets",
        "research_items" => "Recherche d'items",
        "research" => "Rechercher",
        "list_items" => "Liste des Items Possédés par les utilisateurs",
        "my_items" => "Mes objets",
        "photos"=> "Aucune photos",
        "game_name"=> "Nom du Jeu",
        "game_type"=> "Type de Jeu",
        "date_item"=> "Date création de l'offre",
        "active" =>"Actif",
        "disabled" =>"Désactivé",
        "owned_items" => "Mes items possédés",
        "searched_items" => "Mes items recherchés",
        "confirm" => "Confirmer",
        "return" => "Retour",
        "stat" => "Etat",

        "i_own" => "Je possède...",
        "i_search" => "Je recherche...",
        "item_delete_question" => "Voulez-vous vraiment supprimer cet objet ?",

        // Notifications
        "add_notification"=>"Ajouter une notification",
        "list_notifications"=>"Liste des notifications",
        "content" => "Contenu",

        // Matches
        "my_matches" => "Mes matchs",

        // Sign
        "sign_in" => "Se connecter",
        "sign_up" => "S'inscrire",
        "sign_out" => "Se déconnecter",

        "invalid_username_password" => "Nom d'utilisateur ou mot de passe invalide",

        "country" => "Pays",
        "province" => "Région",
        "avatar" => "Avatar",
        "email" => "Email",
        "firstname" => "Prénom",
        "username" => "Nom d'utilisateur",
        "lastname" => "Nom de famille",
        "password" => "Mot de passe",
        "confirm_password" => "Confirmer mot de passe",

        "conditions_check" => "J'accepte les conditions d'utilisations",
        "notifications_check" => "Je souhaite recevoir des notifications",

        "user_lock_question" => "Voulez-vous vraiment bloquer cet utilisateur ?",
        "user_unlock_question" => "Voulez-vous vraiment débloquer cet utilisateur ?",

        //Match
        "match_accept_question" => "Voulez-vous vraiment accepter ce match ?",
        "match_refus_question" => "Voulez-vous vraiment refuser ce match ?",
        "match_vote_question" => "Comment notez-vous le match ?",
        "accept" => "Accepter",
        "match_cancel_question" => "Voulez-vous vraiment annuler ce match ?",
        "cancel" => "Annuler",
        "refus" => "Refuser"
    )
?>
