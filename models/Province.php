<?php
    class Province {
        private $id;
        private $country_id;
        private $province;

        function __construct($id, $country_id, $province) {
            $this->id = $id;
            $this->country_id = $country_id;
            $this->province = $province;
        }

        function getId() {
            return ($this->id);
        }

        function getCountryId() {
            return ($this->country_id);
        }
        function getProvince() {
            return ($this->province);
        }

        function __toString() {
            return ("$this->province");
        }
    }

    function getProvinces($country_id) {
        $db_query = "SELECT * FROM provinces WHERE country_id = $country_id";

        if (!$db = getDatabase()) {
            return ($db);
        }

        if (!$db_result = $db->executeQuery($db_query)) {
            return ($db_result);
        }

        if(!$db_array = $db_result->fetch_all()) {
            return ($db_array);
        }

        foreach ($db_array as $db_row) {
            $provinces[] = new Province($db_row["0"], $db_row["1"], $db_row["2"]);
        }

        return ($provinces);
    }

    function getProvinceById($id) {
        $db_query = "SELECT * FROM provinces WHERE id = $id";

        if (!$db = getDatabase()) {
            return ($db);
        }

        if (!$db_result = $db->executeQuery($db_query)) {
            return ($db_result);
        }

        if(!$db_array = $db_result->fetch_array()) {
            return ($db_array);
        }

        return (new Province($db_array["id"], $db_array["country_id"], $db_array["province"]));
    }

    function getProvincesByCountryId($country_id) {
        $db_query = "SELECT * FROM provinces WHERE country_id = $country_id";

        if (!$db = getDatabase()) {
            return ($db);
        }

        if (!$db_result = $db->executeQuery($db_query)) {
            return ($db_result);
        }

        if(!$db_array = $db_result->fetch_all()) {
            return ($db_array);
        }

        foreach ($db_array as $db_row) {
            $provinces[] = new Province($db_row["0"], $db_row["1"], $db_row["2"]);
        }

        return ($provinces);
    }
?>
