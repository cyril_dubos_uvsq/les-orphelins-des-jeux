<?php
    class Country {
        private $id;
        private $country;

        function __construct($id, $country) {
            $this->id = $id;
            $this->country = $country;
        }

        function getId() {
            return ($this->id);
        }

        function getCountry() {
            return ($this->country);
        }

        function __toString() {
            return ("$this->country");
        }

    }

    function getCountries() {
        $db_query = "SELECT * FROM countries";

        if (!$db = getDatabase()) {
            return ($db);
        }
        
        if (!$db_result = $db->executeQuery($db_query)) {
            return ($db_result);
        }

        if(!$db_array = $db_result->fetch_all()) {
            return ($db_array);
        }

        foreach ($db_array as $db_row) {
            $countries[] = new Country($db_row["0"], $db_row["1"]);
        }

        return ($countries);
    }

    function getCountryById($id) {
        $db_query = "SELECT * FROM countries WHERE id = $id";

        if (!$db = getDatabase()) {
            return ($db);
        }

        if (!$db_result = $db->executeQuery($db_query)) {
            return ($db_result);
        }

        if(!$db_array = $db_result->fetch_array()) {
            return ($db_array);
        }

        return (new Country($db_array["id"], $db_array["country"]));
    }
?>
