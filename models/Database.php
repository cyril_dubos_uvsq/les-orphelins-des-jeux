<?php
    require("$root/config.php");

    class Database {
        private $host;
        private $username;
        private $password;
        private $name;

        private $connection;

        function __construct($host, $username, $password, $name)  {
            $this->host = $host;
            $this->username = $username;
            $this->password = $password;
            $this->name = $name;
        }

        function openConnection() {
            $this->connection = new mysqli($this->host, $this->username, $this->password, $this->name);

            // Check connection
            if ($this->connection->connect_error) {
                die("Connection failed: " . $this->connection->connect_error);
            }

            return ($this->connection);
        }

        function executeQuery($query) {
            $this->openConnection();

            $result = $this->connection->query($query);

            $this->closeConnection();

            return ($result);
        }

        function executePreparedStatement($statement, $types, ...$params) {
            $this->connection = $this->openConnection();

            $result = $this->connection->prepare($statement);
            $result->bind_param($types, ...$params);
            $result->execute();

            $this->closeConnection();

            return ($result);
        }

        function closeConnection() {
            return ($this->connection->close());
        }
    }

    function getDatabase() {
        return (new Database(
            $GLOBALS["config"]["database_host"],
            $GLOBALS["config"]["database_username"],
            $GLOBALS["config"]["database_password"],
            $GLOBALS["config"]["database_name"]
        ));
    }
?>
