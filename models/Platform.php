<?php
    class Platform {
        private $id;
        private $name;
        private $description;

        function __construct($id, $name, $description) {
            $this->id = $id;
            $this->name = $name;
            $this->description = $description;
        }

        function getId() {
            return ($this->id);
        }

        function getName() {
            return ($this->name);
        }

        function getDescription() {
            return ($this->description);
        }

        function __toString() {
            return ("ID: $this->id \t Name: $this->name \t Description: $this->description");
        }

        function add() {
            $name = $this->name;
            $description = $this->description;
    
            $db_statement = "INSERT INTO platforms (name, description) VALUES (?, ?)";
    
            if (!$db = getDatabase()) {
                return ($db);
            }
            
            $db_result = $db->executePreparedStatement($db_statement, "ss", $name, $description);
    
            return ($db_result);
        }

        function edit() {
            $id = $this->id;
            $name = $this->name;
            $description = $this->description;

            $db_query = "UPDATE platforms SET name = '$name', description = '$description' WHERE id = $id";
    
            if (!$db = getDatabase()) {
                return ($db);
            }
            
            $db_result = $db->executeQuery($db_query);
    
            return ($db_result);
        }

        function delete() {
            $id = $this->id;

            $db_query = "DELETE FROM platforms WHERE id = $id";
    
            if (!$db = getDatabase()) {
                return ($db);
            }
            
            $db_result = $db->executeQuery($db_query);
    
            return ($db_result);
        }
    }
    
    function getPlatforms() {
        $db_query = "SELECT * FROM platforms";

        if (!$db = getDatabase()) {
            return ($db);
        }
        
        if (!$db_result = $db->executeQuery($db_query)) {
            return ($db_result);
        }

        if(!$db_array = $db_result->fetch_all()) {
            return ($db_array);
        }

        foreach ($db_array as $db_row) {
            $platforms[] = new Platform($db_row["0"], $db_row["1"], $db_row["2"]);
        }

        return ($platforms);
    }

    function getPlatformById($id) {
        $db_query = "SELECT * FROM platforms WHERE id = $id";

        if (!$db = getDatabase()) {
            return ($db);
        }
        
        if (!$db_result = $db->executeQuery($db_query)) {
            return ($db_result);
        }

        if(!$db_array = $db_result->fetch_array()) {
            return ($db_array);
        }

        return (new Platform($db_array["id"], $db_array["name"], $db_array["description"]));
    }

    function getPlatformByRegionId($region_id) {
        $region = getRegionById($region_id);

        $platform_id = $region->getPlatformId($region_id);

        $db_query = "SELECT * FROM platforms WHERE id = $platform_id";

        if (!$db = getDatabase()) {
            return ($db);
        }
        
        if (!$db_result = $db->executeQuery($db_query)) {
            return ($db_result);
        }

        if(!$db_array = $db_result->fetch_array()) {
            return ($db_array);
        }

        return (new Platform($db_array["id"], $db_array["name"], $db_array["description"]));
    }
?>