<?php
    class Item {
        private $id;
        private $user_id;
        private $game_id;
        private $type_id;
        private $stat_id;
        private $created_at;
        private $status;

        function __construct($id, $user_id, $game_id, $type_id,$stat_id, $created_at, $status) {
            $this->id = $id;
            $this->user_id = $user_id;
            $this->game_id = $game_id;
            $this->type_id = $type_id;
            $this->stat_id = $stat_id;
            $this->created_at = $created_at;
            $this->status = $status;
        }

        function getId() {
            return ($this->id);
        }

        function getUserId() {
            return ($this->user_id);
        }

        function getGameId() {
            return ($this->game_id);
        }

        function getTypeId() {
            return ($this->type_id);
        }

        function getStatId() {
            return ($this->stat_id);
        }

        function getCreatedAt() {
            return ($this->created_at);
        }

        function getStatus() {
            return ($this->status);
        }

        function __toString() {
            return ("ID: $this->id \t UserId: $this->user_id \t GameId: $this->game_id \t TypeId: $this->type_id \t StatId: $this->stat_id \t CreatedAt: $this->created_at \t Status: $this->status");
        }

        function updateStat($table,$new_stat_id) {
            $this->stat_id = $new_stat_id;
            $id = $this->id;
            $db_query = "UPDATE $table SET stat_id = $new_stat_id WHERE id = $id";

            if (!$db = getDatabase()) {
                return ($db);
            }

            $db_result = $db->executeQuery($db_query);

            return ($db_result);
        }

        function add($table) {
            $user_id = $this->user_id;
            $game_id = $this->game_id;
            $type_id = $this->type_id;
            $stat_id = $this->stat_id;
            $status = $this->status;

            $db_query = "INSERT INTO $table (user_id, game_id, type_id,stat_id, status) VALUES ('$user_id', '$game_id',  '$type_id','$stat_id', '$status')";

            if (!$db = getDatabase()) {
                return ($db);
            }

            if(!$db_result = $db->executeQuery($db_query)){
                return($db_result);
            }


            $db_query = "SELECT AUTO_INCREMENT FROM  INFORMATION_SCHEMA.TABLES WHERE TABLE_SCHEMA = 'retrogaming' AND TABLE_NAME = '$table'";

            if (!$db = getDatabase()) {
                return ($db);
            }

            if (!$db_result = $db->executeQuery($db_query)) {
                return ($db_result);
            }

            if(!$db_array = $db_result->fetch_all()) {
                return ($db_array);
            }


            $ai =  $db_array["0"]["0"] - 1;
            $this->id = $ai;

            $table_ck = ($table == "searched_items") ? "owned_items" : "searched_items";
            $plusmoins = ($table == "searched_items") ? "<=" : ">=";

            $db_query = "SELECT * FROM $table_ck WHERE game_id = $game_id AND user_id != $user_id AND type_id = $type_id AND stat_id $plusmoins $stat_id AND status = 1";

            //echo $db_query;

            if (!$db = getDatabase()) {
                return ($db);
            }
            //
            if (!$db_result = $db->executeQuery($db_query)) {
                return ($db_result);
            }

            $db_array = $db_result->fetch_all();

            $root = realpath($_SERVER["DOCUMENT_ROOT"]);
            require("$root/models/Match.php");
            foreach ($db_array as $db_row) {
                if($table == "searched_items"){
                    $item_id_own = $db_row["0"];
                    $item_id_search = $ai;
                }
                else{
                    $item_id_search = $db_row["0"];
                    $item_id_own = $ai;
                }

                $match = new Match(NULL, $item_id_own, $item_id_search, 0, 0, 0, 0, NULL, 1);
                if(!$res = $match->add()){
                    return ($res);
                }
            }
            return ($db_result);
        }

        function delete($table) {
            $id = $this->id;

            $db_query = "DELETE FROM $table WHERE id = $id";

            if (!$db = getDatabase()) {
                return ($db);
            }

            if(!$db_result = $db->executeQuery($db_query)){
                return ($db_result);
            }

            $db_query = "DELETE FROM matches WHERE ";
            $db_query .= ($table == "owned_items") ? "owned_item_id=$id" : "searched_item_id=$id";

            if (!$db = getDatabase()) {
                return ($db);
            }

            $db_result = $db->executeQuery($db_query);

            return ($db_result);
        }
    }

    function lockAllItemsByUserId($user_id,$user_status) {
      $db_query = "UPDATE owned_items SET status=$user_status WHERE user_id=$user_id";
      if (!$db = getDatabase()) {
          return ($db);
      }

      if (!$db_result = $db->executeQuery($db_query)) {
          return ($db_result);
      }
    }

    function lockAnItemByUserId($item_id,$user_id,$user_status) {
      $db_query = "UPDATE owned_items SET status=$user_status WHERE user_id=$user_id and id=$item_id";
      if (!$db = getDatabase()) {
          return ($db);
      }

      if (!$db_result = $db->executeQuery($db_query)) {
          return ($db_result);
      }
    }

    function getItemsOwned() {
        $db_query = 'SELECT * FROM owned_items';

        if (!$db = getDatabase()) {
            return ($db);
        }

        if (!$db_result = $db->executeQuery($db_query)) {
            return ($db_result);
        }

        if(!$db_array = $db_result->fetch_all()) {
            return ($db_array);
        }

        foreach ($db_array as $db_row) {
            $owned_item[] = new Item($db_row["0"], $db_row["1"], $db_row["2"], $db_row["3"], $db_row["6"], $db_row["4"],$db_row["5"],);
        }

        return ($owned_item);
    }

    function getLastItemsOwned($number) {
        $db_query = 'SELECT * FROM owned_items ORDER BY created_at';

        if (!$db = getDatabase()) {
            return ($db);
        }

        if (!$db_result = $db->executeQuery($db_query)) {
            return ($db_result);
        }

        if(!$db_array = $db_result->fetch_all()) {
            return ($db_array);
        }

        $count = 0;

        foreach ($db_array as $db_row) {
            $owned_item[] = new Item($db_row["0"], $db_row["1"], $db_row["2"], $db_row["3"], $db_row["6"], $db_row["4"],$db_row["5"],);

            if (++$count >= $number) {
                break;
            }
        }

        return ($owned_item);
    }

    function getItemsSearched() {
        $db_query = 'SELECT * FROM searched_items';

        if (!$db = getDatabase()) {
            return ($db);
        }

        if (!$db_result = $db->executeQuery($db_query)) {
            return ($db_result);
        }

        if(!$db_array = $db_result->fetch_all()) {
            return ($db_array);
        }

        foreach ($db_array as $db_row) {
            $searched_item[] = new Item($db_row["0"], $db_row["1"], $db_row["2"], $db_row["3"], $db_row["6"], $db_row["4"], $db_row["5"]);
        }

        return ($searched_item);
    }

    function getItemsByUserId($table, $user_id, $ck_enabled) {
        $enabled = ($ck_enabled) ? "AND status = '1'" : "" ;

        $db_query = "SELECT * FROM $table WHERE user_id = $user_id ".$enabled;

        if (!$db = getDatabase()) {
            return ($db);
        }

        if (!$db_result = $db->executeQuery($db_query)) {
            return ($db_result);
        }

        if(!$db_array = $db_result->fetch_all()) {
            return ($db_array);
        }

        foreach ($db_array as $db_row) {
            $items[] = new Item($db_row["0"], $db_row["1"], $db_row["2"], $db_row["3"], $db_row["6"], $db_row["4"],$db_row["5"]);
        }

        return ($items);
    }

    function getItemById($table,$id) {
        $db_query = "SELECT * FROM $table WHERE id = $id";

        if (!$db = getDatabase()) {
            return ($db);
        }

        if (!$db_result = $db->executeQuery($db_query)) {
            return ($db_result);
        }

        if(!$db_array = $db_result->fetch_array()) {
            return ($db_array);
        }

        return (new Item($db_array["id"], $db_array["user_id"], $db_array["game_id"], $db_array["type_id"],$db_array["stat_id"], $db_array["created_at"], $db_array["status"]));
    }
?>
