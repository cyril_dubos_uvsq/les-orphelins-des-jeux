<?php
    class Game {
        private $id;
        private $title;
        private $year;
        private $region_id;
        private $description;
        private $language;

        function __construct($id, $title, $year, $region_id, $description, $language) {
            $this->id = $id;
            $this->title = $title;
            $this->year = $year;
            $this->region_id = $region_id;
            $this->description = $description;
            $this->language = $language;
        }

        function getId() {
            return ($this->id);
        }

        function getTitle() {
            return ($this->title);
        }

        function getYear() {
            return ($this->year);
        }

        function getRegionId() {
            return ($this->region_id);
        }

        function getDescription() {
            return ($this->description);
        }

        function getLanguage() {
            return ($this->language);
        }

        function __toString() {
            return ("ID: $this->id \t Title: $this->title \t Year: $this->year \t Region ID: $this->region_id \t Description: $this->description \t Langugage: $this->language");
        }

        function add() {
            $title = $this->title;
            $year = $this->year;
            $region_id = $this->region_id;
            $description = $this->description;
            $language = $this->language;
    
            $db_query = "INSERT INTO games (title, year, region_id, description, language) VALUES ('$title', '$year', '$region_id', '$description', '$language')";
    
            if (!$db = getDatabase()) {
                return ($db);
            }
            
            $db_result = $db->executeQuery($db_query);
    
            return ($db_result);
        }

        function edit() {
            $id = $this->id;
            $title = $this->title;
            $year = $this->year;
            $region_id = $this->region_id;
            $description = $this->description;
            $language = $this->language;

            $db_query = "UPDATE games SET title = '$title', year = '$year', region_id = '$region_id', description = '$description', language = '$language' WHERE id = $id";
    
            if (!$db = getDatabase()) {
                return ($db);
            }
            
            $db_result = $db->executeQuery($db_query);
    
            return ($db_result);
        }

        function delete() {
            $id = $this->id;

            $db_query = "DELETE FROM games WHERE id = $id";
    
            if (!$db = getDatabase()) {
                return ($db);
            }
            
            $db_result = $db->executeQuery($db_query);
    
            return ($db_result);
        }
    }

    function getGamesByRegionId($region_id) {
        $db_query = "SELECT * FROM games WHERE region_id = $region_id";

        if (!$db = getDatabase()) {
            return ($db);
        }
        
        if (!$db_result = $db->executeQuery($db_query)) {
            return ($db_result);
        }

        if(!$db_array = $db_result->fetch_all()) {
            return ($db_array);
        }

        foreach ($db_array as $db_row) {
            $games[] = new Game($db_row["0"], $db_row["1"], $db_row["2"], $db_row["3"], $db_row["4"], $db_row["5"]);
        }

        return ($games);
    }

    function getGameById($id) {
        $db_query = "SELECT * FROM games WHERE id = $id";

        if (!$db = getDatabase()) {
            return ($db);
        }

        if (!$db_result = $db->executeQuery($db_query)) {
            return ($db_result);
        }

        if(!$db_array = $db_result->fetch_array()) {
            return ($db_array);
        }

        return (new Game($db_array["id"], $db_array["title"], $db_array["year"], $db_array["region_id"], $db_array["description"], $db_array["language"]));
    }
?>