<?php
    class Notification {
        private $id;
        private $content;
        private $date;

        function __construct($id, $content, $date) {
            $this->id = $id;
            $this->content = $content;
            $this->date = $date;
        }

        function getId() {
            return ($this->id);
        }

        function getContent() {
            return ($this->content);
        }

        function getDate() {
            return ($this->date);
        }

        function __toString() {
            return ("ID: $this->id \t Content : $this->content \t Date : $this->date");
        }

        function add() {
            $content = $this->content;

            $db_query = "INSERT INTO notifications (content) VALUES ('$content')";

            if (!$db = getDatabase()) {
                return ($db);
            }

            if(!$db_result = $db->executeQuery($db_query)){
                return($db_result);
            }

            return ($db_result);
        }

        /*function edit() {
            $id = $this->id;
            $name = $this->name;
            $description = $this->description;

            $db_query = "UPDATE platforms SET name = '$name', description = '$description' WHERE id = $id";

            if (!$db = getDatabase()) {
                return ($db);
            }

            $db_result = $db->executeQuery($db_query);

            return ($db_result);
        }*/

        function delete() {
            $id = $this->id;

            $db_query = "DELETE FROM notifications WHERE id = $id";

            if (!$db = getDatabase()) {
                return ($db);
            }

            $db_result = $db->executeQuery($db_query);

            return ($db_result);
        }
    }

    function getNotifications() {
        $db_query = "SELECT * FROM notifications";

        if (!$db = getDatabase()) {
            return ($db);
        }

        if (!$db_result = $db->executeQuery($db_query)) {
            return ($db_result);
        }

        if(!$db_array = $db_result->fetch_all()) {
            return ($db_array);
        }

        foreach ($db_array as $db_row) {
            $notifications[] = new Notification($db_row["0"], $db_row["1"], $db_row["2"]);
        }

        return ($notifications);
    }

    function getNotificationById($id) {
        $db_query = "SELECT * FROM notifications WHERE id = $id";

        if (!$db = getDatabase()) {
            return ($db);
        }

        if (!$db_result = $db->executeQuery($db_query)) {
            return ($db_result);
        }

        if(!$db_array = $db_result->fetch_array()) {
            return ($db_array);
        }

        return (new Notification($db_array["id"], $db_array["content"], $db_array["date"]));
    }

    function getNotificationsOfToday() {
        $db_query = "SELECT * FROM notifications WHERE date >= cast((now()) as date) and date < cast((now() + interval 1 day) as date)";

        if (!$db = getDatabase()) {
            return ($db);
        }

        if (!$db_result = $db->executeQuery($db_query)) {
            return ($db_result);
        }

        if(!$db_array = $db_result->fetch_all()) {
            return ($db_array);
        }

        foreach ($db_array as $db_row) {
            $notifications[] = new Notification($db_row["0"], $db_row["1"], $db_row["2"]);
        }

        return ($notifications);
    }
?>
