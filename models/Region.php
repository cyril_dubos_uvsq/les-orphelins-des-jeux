<?php
    class Region {
        private $id;
        private $name;
        private $platform_id;
        private $description;

        function __construct($id, $name, $platform_id, $description) {
            $this->id = $id;
            $this->name = $name;
            $this->platform_id = $platform_id;
            $this->description = $description;
        }

        function getId() {
            return ($this->id);
        }

        function getName() {
            return ($this->name);
        }

        function getPlatformId() {
            return ($this->platform_id);
        }

        function getDescription() {
            return ($this->description);
        }

        function __toString() {
            return ("ID: $this->id \t Name: $this->name \t Platform ID: $this->platform_id \t Description: $this->description");
        }

        function add() {
            $name = $this->name;
            $platform_id = $this->platform_id;
            $description = $this->description;

            $db_query = "INSERT INTO regions (name, platform_id, description) VALUES ('$name', '$platform_id', '$description')";

            if (!$db = getDatabase()) {
                return ($db);
            }

            $db_result = $db->executeQuery($db_query);

            return ($db_result);
        }

        function edit() {
            $id = $this->id;
            $name = $this->name;
            $platform_id = $this->platform_id;
            $description = $this->description;

            $db_query = "UPDATE regions SET name = '$name', platform_id = '$platform_id', description = '$description' WHERE id = $id";

            if (!$db = getDatabase()) {
                return ($db);
            }

            $db_result = $db->executeQuery($db_query);

            return ($db_result);
        }

        function delete() {
            $id = $this->id;

            $db_query = "DELETE FROM regions WHERE id = $id";

            if (!$db = getDatabase()) {
                return ($db);
            }

            $db_result = $db->executeQuery($db_query);

            return ($db_result);
        }
    }

    function getRegions() {
        $db_query = "SELECT * FROM regions";

        if (!$db = getDatabase()) {
            return ($db);
        }
        
        if (!$db_result = $db->executeQuery($db_query)) {
            return ($db_result);
        }

        if(!$db_array = $db_result->fetch_all()) {
            return ($db_array);
        }

        foreach ($db_array as $db_row) {
            $regions[] = new Region($db_row["0"], $db_row["1"], $db_row["2"], $db_row["3"]);
        }

        return ($regions);
    }

    function getRegionsByPlatformId($platform_id) {
        $db_query = "SELECT * FROM regions WHERE platform_id = $platform_id";

        if (!$db = getDatabase()) {
            return ($db);
        }

        if (!$db_result = $db->executeQuery($db_query)) {
            return ($db_result);
        }

        if(!$db_array = $db_result->fetch_all()) {
            return ($db_array);
        }

        foreach ($db_array as $db_row) {
            $regions[] = new Region($db_row["0"], $db_row["2"], $db_row["1"], $db_row["3"]);
        }

        return ($regions);
    }

    function getRegionById($id) {
        $db_query = "SELECT * FROM regions WHERE id = $id";

        if (!$db = getDatabase()) {
            return ($db);
        }

        if (!$db_result = $db->executeQuery($db_query)) {
            return ($db_result);
        }

        if(!$db_array = $db_result->fetch_array()) {
            return ($db_array);
        }

        return (new Region($db_array["id"], $db_array["name"], $db_array["platform_id"], $db_array["description"]));
    }
?>
