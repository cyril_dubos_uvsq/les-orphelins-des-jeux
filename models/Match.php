<?php
    class Match {
        private $id;
        private $owned_item_id;
        private $searched_item_id;
        private $owner_state;
        private $searcher_state;
        private $owner_rating;
        private $searcher_rating;
        private $created_at;
        private $status;

        function __construct($id, $owned_item_id, $searched_item_id, $owner_state , $searcher_state, $owner_rating, $searcher_rating, $created_at, $status) {
            $this->id = $id;
            $this->owned_item_id = $owned_item_id;
            $this->searched_item_id = $searched_item_id;
            $this->owner_state = $owner_state;
            $this->searcher_state = $searcher_state;
            $this->owner_rating = $owner_rating;
            $this->searcher_rating = $searcher_rating;
            $this->created_at = $created_at;
            $this->status = $status;
        }

        function getId() {
            return ($this->id);
        }

        function getOwnedItemId() {
            return ($this->owned_item_id);
        }

        function getSearchedItemId() {
            return ($this->searched_item_id);
        }

        function getOwnerState() {
            return ($this->owner_state);
        }

        function getSearcherState() {
            return ($this->searcher_state);
        }

        function getOwnerRating() {
            return ($this->owner_rating);
        }

        function getSearcherRating() {
            return ($this->searcher_rating);
        }

        function getCreatedAt() {
            return ($this->created_at);
        }

        function getStatus() {
            return ($this->status);
        }

        function __toString() {
            return ("ID: $this->id \t OwnedItemId: $this->owned_item_id \t SearchedItemId: $this->searched_item_id \t OwnerState: $this->owner_state \t SearcherState: $this->searcher_state \t OwnerRating: $this->owner_rating \t SearcherRating: $this->searcher_rating \t CreatedAt: $this->created_at \t Status: $this->status");
        }

        function confirm(){
            $id_src = $this->searched_item_id;
            $id_own = $this->owned_item_id;

            $db_query = "UPDATE matches SET status = 0 WHERE id = $this->id";
            if (!$db = getDatabase()) {
                return ($db);
            }

            $db_result = $db->executeQuery($db_query);
            
            $db_query = "UPDATE searched_items SET status = 0 WHERE id = $id_src";
            if (!$db = getDatabase()) {
                return ($db);
            }

            $db_result = $db->executeQuery($db_query);
            
            $db_query = "UPDATE owned_items SET status = 0 WHERE id = $id_own";
            if (!$db = getDatabase()) {
                return ($db);
            }

            $db_result = $db->executeQuery($db_query);

            $root = realpath($_SERVER["DOCUMENT_ROOT"]);
            require("$root/models/Item.php");
        
            $id_owner = getItemById("owned_items",$id_own)->getUserId();
            $db_query = ($this->searcher_rating == 1) ? "UPDATE users SET positive_ranking = positive_ranking+1 " : "UPDATE users SET negative_ranking = negative_ranking+1 ";
            $db_query .= "WHERE id=$id_owner";
            if (!$db = getDatabase()) {
                return ($db);
            }

            $db_result = $db->executeQuery($db_query);
        
            $id_searcher = getItemById("searched_items",$id_src)->getUserId();
            $db_query = ($this->owner_rating == 1) ? "UPDATE users SET positive_ranking = positive_ranking+1 ":"UPDATE users SET negative_ranking = negative_ranking+1 ";
            $db_query .= "WHERE id=$id_searcher";
            if (!$db = getDatabase()) {
                return ($db);
            }

            $db_result = $db->executeQuery($db_query);

            $db_query = "DELETE FROM matches WHERE (searched_item_id = '$id_src' OR owned_item_id = '$id_own') AND status = '1'";

            if (!$db = getDatabase()) {
                return ($db);
            }

            $db_result = $db->executeQuery($db_query);

            return ($db_result);
        }

        function add() {
            $owned_item_id = $this->owned_item_id;
            $searched_item_id = $this->searched_item_id;
            $owner_state = $this->owner_state;
            $searcher_state = $this->searcher_state;
            $owner_rating = $this->owner_rating;
            $searcher_rating = $this->searcher_rating;
            $status = $this->status;

            $db_query = "INSERT INTO matches (owned_item_id, searched_item_id, owner_state, searcher_state, owner_rating, searcher_rating, status) VALUES ('$owned_item_id', '$searched_item_id', '$owner_state', '$searcher_state', '$owner_rating', '$searcher_rating', '$status')";

            if (!$db = getDatabase()) {
                return ($db);
            }

            $db_result = $db->executeQuery($db_query);

            return ($db_result);
        }

        function edit() {
            $id = $this->id;
            $owned_item_id = $this->owned_item_id;
            $searched_item_id = $this->searched_item_id;
            $owner_state = $this->owner_state;
            $searcher_state = $this->searcher_state;
            $owner_rating = $this->owner_rating;
            $searcher_rating = $this->searcher_rating;
            $created_at = $this->created_at;
            $status = $this->status;

            $db_query = "UPDATE matches SET owned_item_id = '$owned_item_id', searched_item_id = '$searched_item_id', owner_state = '$owner_state', searcher_state = '$searcher_state', owner_rating = '$owner_rating', searcher_rating = '$searcher_rating', created_at = '$created_at', status = '$status' WHERE id = $id";

            if (!$db = getDatabase()) {
                return ($db);
            }

            $db_result = $db->executeQuery($db_query);

            return ($db_result);
        }

        function delete() {
            $id = $this->id;

            $db_query = "DELETE FROM matches WHERE id = $id";

            if (!$db = getDatabase()) {
                return ($db);
            }

            $db_result = $db->executeQuery($db_query);

            return ($db_result);
        }
    }

    function getMatchById($id) {
        $db_query = "SELECT * FROM matches WHERE id = '$id'";

        if (!$db = getDatabase()) {
            return ($db);
        }

        if (!$db_result = $db->executeQuery($db_query)) {
            return ($db_result);
        }

        if(!$db_array = $db_result->fetch_array()) {
            return ($db_array);
        }

        return (new Match($db_array["id"], $db_array["owned_item_id"], $db_array["searched_item_id"], $db_array["owner_state"], $db_array["searcher_state"], $db_array["owner_rating"], $db_array["searcher_rating"], $db_array["created_at"], $db_array["status"]));
    }

    function getMatchsByUserId($table, $user_id, $ck_enabled) {
        $enabled = ($ck_enabled) ? "AND m.status = '1'" : "" ;
        if($table == "owned"){
            $db_query = "SELECT m.id, m.owned_item_id, m.searched_item_id, m.owner_state, m.searcher_state, m.owner_rating, m.searcher_rating, m.created_at, m.status FROM ".$table."_items t, matches m WHERE m.".$table."_item_id = t.id AND t.user_id = $user_id ".$enabled." GROUP BY owned_item_id";
        }
        else{
            $db_query = "SELECT m.id, m.owned_item_id, m.searched_item_id, m.owner_state, m.searcher_state, m.owner_rating, m.searcher_rating, m.created_at, m.status FROM ".$table."_items t, matches m WHERE m.".$table."_item_id = t.id AND t.user_id = $user_id ".$enabled." GROUP BY searched_item_id";
        }

        if (!$db = getDatabase()) {
            return ($db);
        }

        if (!$db_result = $db->executeQuery($db_query)) {
            return ($db_result);
        }

        if(!$db_array = $db_result->fetch_all()) {
            return ($db_array);
        }

        foreach ($db_array as $db_row) {
            $matchs[] = new Match($db_row["0"], $db_row["1"], $db_row["2"], $db_row["3"], $db_row["4"], $db_row["5"], $db_row["6"], $db_row["7"], $db_row["8"]);
        }

        return ($matchs);
    }

    function getMatchesByOwnedItemId($owned_item_id) {
        $db_query = "SELECT * FROM matches WHERE owned_item_id = $owned_item_id ORDER BY created_at ASC";

        if (!$db = getDatabase()) {
            return ($db);
        }

        if (!$db_result = $db->executeQuery($db_query)) {
            return ($db_result);
        }

        if(!$db_array = $db_result->fetch_all()) {
            return ($db_array);
        }

        foreach ($db_array as $db_row) {
            $matches[] = new Match($db_row["0"], $db_row["1"], $db_row["2"], $db_row["3"], $db_row["4"], $db_row["5"], $db_row["6"], $db_row["7"], $db_row["8"]);
        }

        return ($matches);
    }

    function getMatchesBySearchedItemId($searched_item_id) {
        $db_query = "SELECT * FROM matches WHERE searched_item_id = $searched_item_id ORDER BY created_at ASC";

        if (!$db = getDatabase()) {
            return ($db);
        }

        if (!$db_result = $db->executeQuery($db_query)) {
            return ($db_result);
        }

        if(!$db_array = $db_result->fetch_all()) {
            return ($db_array);
        }

        foreach ($db_array as $db_row) {
            $matches[] = new Match($db_row["0"], $db_row["1"], $db_row["2"], $db_row["3"], $db_row["4"], $db_row["5"], $db_row["6"], $db_row["7"], $db_row["8"]);
        }

        return ($matches);
    }

    function deleteAllMatchByIdItem($table, $id_item) {
        $db_query = "DELETE FROM matches WHERE $table = $id_item";

        if (!$db = getDatabase()) {
            return ($db);
        }

        if (!$db_result = $db->executeQuery($db_query)) {
            return ($db_result);
        }
    }

?>
