<?php
    class User {
        private $id;
        private $firstname;
        private $lastname;
        private $username;
        private $email;
        private $password;
        private $rank_id;
        private $positive_ranking;
        private $negative_ranking;
        private $province_id;
        private $created_at;
        private $status;

        function __construct($id, $firstname, $lastname, $username, $email, $password, $rank_id, $positive_ranking, $negative_ranking, $province_id, $created_at, $status) {
            $this->id = $id;
            $this->firstname = $firstname;
            $this->lastname = $lastname;
            $this->username = $username;
            $this->email = $email;
            $this->password = $password;
            $this->rank_id = $rank_id;
            $this->positive_ranking = $positive_ranking;
            $this->negative_ranking = $negative_ranking;
            $this->province_id = $province_id;
            $this->created_at = $created_at;
            $this->status = $status;
        }

        function getId(){
            return $this->id;
        }

        function getAvatar() {
            $root = realpath($_SERVER["DOCUMENT_ROOT"]);

            $default = "/models/images/avatars/default.png";
            $avatar = "/models/images/avatars/$this->id.png";

            if (file_exists($root . $avatar)) {
                return ($avatar);
            } else {
                return ($default);
            }
        }

        function getFirstname(){
            return $this->firstname;
        }

        function setFirstname($firstname){
            $this->firstname = $firstname;
        }

        function getLastname(){
            return $this->lastname;
        }

        function setLastname($lastname){
            $this->lastname = $lastname;
        }

        function getUsername(){
            return $this->username;
        }

        function setUsername($username){
            $this->username = $username;
        }

        function getEmail(){
            return $this->email;
        }

        function setEmail($email){
            $this->email = $email;
        }

        function getPassword(){
            return $this->password;
        }

        function getRankId(){
            return $this->rank_id;
        }

        function getPositiveRanking(){
            return $this->positive_ranking;
        }

        function getNegativeRanking(){
            return $this->negative_ranking;
        }

        function getProvinceId(){
            return $this->province_id;
        }

        function setProvinceId($province_id){
            $this->province_id = $province_id;
        }

        function getCreatedAt(){
            return $this->created_at;
        }

        function getStatus(){
            return $this->status;
        }

        function add() {
            $firstname = $this->firstname;
            $lastname = $this->lastname;
            $username = $this->username;
            $email = $this->email;
            $password = $this->password;
            $rank_id = $this->rank_id;
            $positive_ranking = $this->positive_ranking;
            $negative_ranking = $this->negative_ranking;
            $province_id = $this->province_id;
            $created_at = $this->created_at;
            $status = $this->status;


            $db_query = "INSERT INTO users (username, firstname, lastname, email, password, rank_id, positive_ranking, negative_ranking, province_id, status) VALUES ('$username', '$firstname', '$lastname', '$email', '$password', '$rank_id', '$positive_ranking', '$negative_ranking', '$province_id', '$status')";

            if (!$db = getDatabase()) {
                return ($db);
            }

            $db_result = $db->executeQuery($db_query);

            $db_query = "SELECT * FROM users WHERE username='$username' ";
            $db_result = $db->executeQuery($db_query);
            $db_array = $db_result->fetch_array();
            $this->id = $db_array["id"];
            return ($db_result);
        }

        function edit() {
            $id = $this->id;
            $firstname = $this->firstname;
            $lastname = $this->lastname;
            $username = $this->username;
            $email = $this->email;
            $province_id = $this->province_id;

            $db_query = "UPDATE users SET firstname = '$firstname', lastname = '$lastname', username = '$username', email = '$email', province_id = $province_id WHERE id = $id";

            if (!$db = getDatabase()) {
                return ($db);
            }

            $db_result = $db->executeQuery($db_query);

            return ($db_result);
        }

        function isAdministrator() {
            return ($this->rank_id == 2);
        }

        function lock(){
          $id=$this->id;
          $status = $this->status;

          $status = $status == 1 ? 0 : 1;

          $db_query = "UPDATE users SET status='$status' WHERE id='$id'";

          if (!$db = getDatabase()) {
              return ($db);
          }

          $db_result = $db->executeQuery($db_query);

          return ($status);
        }
    }

    function getUsers() {
        $db_query = 'SELECT * FROM users WHERE rank_id=0';

        if (!$db = getDatabase()) {
            return ($db);
        }

        if (!$db_result = $db->executeQuery($db_query)) {
            return ($db_result);
        }

        if(!$db_array = $db_result->fetch_all()) {
            return ($db_array);
        }

        foreach ($db_array as $db_row) {
            $users[] = new User($db_row["0"], $db_row["2"], $db_row["3"], $db_row["1"], $db_row["4"], $db_row["5"],$db_row["6"],$db_row["7"],$db_row["8"],$db_row["9"],$db_row["10"],$db_row["11"]);
        }

        return ($users);
    }

    function getUserById($id) {
        $db_query = "SELECT * FROM users WHERE id = '$id'";

        if (!$db = getDatabase()) {
            return ($db);
        }

        if (!$db_result = $db->executeQuery($db_query)) {
            return ($db_result);
        }

        if(!$db_array = $db_result->fetch_array()) {
            return ($db_array);
        }

        return (new User($db_array["id"], $db_array["firstname"], $db_array["lastname"], $db_array["username"], $db_array["email"], $db_array["password"], $db_array["rank_id"], $db_array["positive_ranking"], $db_array["negative_ranking"], $db_array["province_id"], $db_array["created_at"], $db_array["status"]));
    }

    function getUserByUsername($username) {
        $db_query = "SELECT * FROM users WHERE username = '$username'";

        if (!$db = getDatabase()) {
            return ($db);
        }

        if (!$db_result = $db->executeQuery($db_query)) {
            return ($db_result);
        }

        if(!$db_array = $db_result->fetch_array()) {
            return ($db_array);
        }

        return (new User($db_array["id"], $db_array["firstname"], $db_array["lastname"], $db_array["username"], $db_array["email"], $db_array["password"], $db_array["rank_id"], $db_array["positive_ranking"], $db_array["negative_ranking"], $db_array["province_id"], $db_array["created_at"], $db_array["status"]));
    }

    function getUserByEmail($email) {
        $db_query = "SELECT * FROM users WHERE email = '$email'";

        if (!$db = getDatabase()) {
            return ($db);
        }

        if (!$db_result = $db->executeQuery($db_query)) {
            return ($db_result);
        }

        if(!$db_array = $db_result->fetch_array()) {
            return ($db_array);
        }

        return (new User($db_array["id"], $db_array["firstname"], $db_array["lastname"], $db_array["username"], $db_array["email"], $db_array["password"], $db_array["rank_id"], $db_array["positive_ranking"], $db_array["negative_ranking"], $db_array["province_id"], $db_array["created_at"], $db_array["status"]));
    }
?>
